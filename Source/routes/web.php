<?php

/* [START] Web Route(s) */
Route::group(['middleware' => ['web'], 'namespace' => 'Site'], function() {
    
    Auth::routes();
    
    Route::get('/', 'HomeController@index')->name('home');
    
    Route::get('expert', 'PageController@getExpert');
    Route::get('catalogue', 'PageController@getCatalogue');
    Route::get('catalogue-details', 'PageController@getCatalogueDetails');
    Route::get('client', 'PageController@getClient');
    Route::get('course-details', 'PageController@getCourseDetails');
    Route::get('featured-content', 'PageController@getFeaturedContent');
    
    Route::get('thanks', 'PageController@getThanks');
    
    Route::get('payment', 'PageController@getPayment');
    Route::get('upcoming-events', 'PageController@getUpcomingEvents');
    Route::get('quick-lesson', 'PageController@getQuickLesson');
    Route::get('author-details', 'PageController@getAuthorDetails');
    Route::get('course-learner', 'PageController@getCourseLearner');
    Route::get('course-learner-test', 'PageController@getCourseLearnerTest');
    
    //Members pages
    Route::get('members-profile', 'PageController@getMembersProfile');
    Route::get('members-dashboard', 'PageController@getMemberDashboard');
    Route::get('members-categories', 'PageController@getMemberCategories');
    Route::get('members-course-details', 'PageController@getMemberCourseDetails');
    Route::get('members-courses', 'PageController@getMemberCourses');
    Route::get('member-courses-result', 'PageController@getMemberCoursesResult');
    Route::get('member-notifications', 'PageController@getMemberNotifications');
    Route::get('member-messages', 'PageController@getMemberMessages');
    Route::get('member-compose-message', 'PageController@getMemberComposeMessage');
    Route::get('member-message-chat', 'PageController@getMemberMessageChat');
    Route::get('member-certifications', 'PageController@getMemberCertifications');
    Route::get('member-create-certificate', 'PageController@getMemberCreateCertificate');
    Route::get('member-create-certificate-thanks', 'PageController@getMemberCreateCertificateThanks');
    Route::get('member-points', 'PageController@getMemberPoints');
    Route::get('member-info-help', 'PageController@getMemberInfoHelp');
    Route::get('member-sales-management', 'PageController@getMemberSalesManagement');
    Route::get('member-event-management', 'PageController@getMemberEventManagement');
    Route::get('member-quick-lesson', 'PageController@getMemberQuickLesson');
    Route::get('member-create-quick-lesson', 'PageController@getMemberCreateQuickLesson');
    Route::get('member-create-quick-lesson-thanks', 'PageController@getMemberCreateQuickLessonThanks');
    
    Route::get('member-online-course-management', 'PageController@getMemberCourseManagement');
    Route::get('member-ocm-classroom', 'PageController@getMemberOCMClassroom');
    Route::get('member-ocm-create-coupon', 'PageController@getMemberOCMCreateCoupon');
    Route::get('member-ocm-step1', 'PageController@getMemberOCMStep1');
    Route::get('member-ocm-step2', 'PageController@getMemberOCMStep2');
    Route::get('member-ocm-step3', 'PageController@getMemberOCMStep3');
    Route::get('member-ocm-step4', 'PageController@getMemberOCMStep4');
    Route::get('member-ocm-step-thanks', 'PageController@getMemberOCMStepThanks');
    
    //Employee pages
    Route::get('employee-billings', 'PageController@getEmployeeBillings');
    Route::get('employee-courses', 'PageController@getEmployeeCourses');
    Route::get('employee-database', 'PageController@getEmployeeDatabase');
    Route::get('employee-details', 'PageController@getEmployeeDetails');
    Route::get('employee-settings', 'PageController@getEmployeeSettings');
    
});
/* [END] Web Route(s) */
