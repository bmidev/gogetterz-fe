/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(4);


/***/ }),
/* 4 */
/***/ (function(module, exports) {

$(document).ready(function (e) {

    $('.modal-close').on('click', function () {
        $.colorbox.close();
    });

    var banner_height = document.querySelector('.featured-banner');
    var pseudoAfterHeight = window.getComputedStyle(banner_height, ':after').height;

    // set second shape top equal to height of 1st shape
    $('<style>.featured-data:before{top:' + pseudoAfterHeight + '}</style>').appendTo('head');

    // set height to before and after shapes based on content
    var numItems = $('.course').length;
    if (numItems > 4) {
        if (numItems > 6) {
            var shape_height = $('.data-container').height() / 2.2 - $('article').height();
        }
        var shape_height = $('.data-container').height() / 1.5 - $('article').height();
        shape_height = shape_height + 'px';
        $('<style>.featured-data:after{height:' + shape_height + '}</style>').appendTo('head');
    } else {
        var shape_height = $('article').height();
        shape_height = shape_height + 'px';
    }
    $('<style>.featured-data:before{height:' + shape_height + '}</style>').appendTo('head');

    // set third shape top equal to height of 2nd shape
    var video_height = document.querySelector('.featured-data');
    var pseudoBeforeHeight = window.getComputedStyle(video_height, ':before').height;

    // convert value to pixels
    var pseudoHeightNum = parseInt(pseudoAfterHeight) + parseInt(pseudoBeforeHeight) + 'px';
    $('<style>.featured-data:after{top:' + pseudoHeightNum + '}</style>').appendTo('head');
});

/***/ })
/******/ ]);