siteApp.controller('RegisterController', ['$scope', '$http', '$window', 'Register', function($scope, $http, $window, Register) {
    /* Default Variables */
    var user = {};
    /* Default Variables */
    
    /* [START] Validate User Registration */
    $scope.signUp = function() {
        user = $scope.users;

        Register.store(user).$promise.then(
            function(response) {
                console.log(response);
                // $window.location.href = 'thanks'
            }, function(response) {
                console.log('ERR', response)
            }
        );
    }, function() {
        console.log('ERR');
    };
    /* [END] Validate User Registration */
}]);
