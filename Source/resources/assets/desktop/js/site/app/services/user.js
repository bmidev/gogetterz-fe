siteApp.factory('User', ['$resource', function($resource) {
    return $resource(APP.baseUrl + 'user/:type', null, {
        'registerUser': {
            method: 'POST',
            params: {
                type: 'register'
            }
        },
        'loginUser': {
            method: 'POST',
            params: {
                type: 'login'
            }
        }
    });
}]);
