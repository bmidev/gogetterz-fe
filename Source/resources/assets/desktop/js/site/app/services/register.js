siteApp.factory('Register', ['$resource', function($resource) {
    return $resource(APP.baseUrl + 'register', null, {
        'store': {method: 'POST'}
    });
}]);
