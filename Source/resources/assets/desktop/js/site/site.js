$(function(e) {
	$('.top-search a.icon-search').on('click', function(e) {
		e.preventDefault();		
		$('.top-search .quich-search').toggle();
	});	
	
	// slider
	$('.welcome-slider').slick({
        infinite: true,
		autoplay: true,
        speed: 500,
        arrows: false,
		dots: true
    });
	
	$('.quick-lessons-slider').slick({
		arrows: true,
		centerMode: true,
		centerPadding: '32%',
		infinite: true,
		slidesToShow: 1,
		speed: 500,
		dots: true,
		responsive: [			
			{
			  breakpoint: 640,
			  settings: {
				centerPadding: '0'
			  }
			}
		]
    });
	
	$('.courses-slider').slick({
		infinite: true,
		autoplay: true,
		slidesToShow: 4,
		slidesToScroll: 4,
        speed: 500,
        arrows: false,
		dots: true,
		responsive: [
			{
			  breakpoint: 960,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 640,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			}
		]
    });
	
	$('.featured-content-slider').slick({
		infinite: true,
		autoplay: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		adaptiveHeight: true,
        speed: 500,
        arrows: true,
		dots: false,
		responsive: [
			{
			  breakpoint: 960,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			  }
			},
			{
			  breakpoint: 640,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			}
		]
    });

    $('.slider-container').slick({
        dots: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		centerMode: true,
		initialSlide: 0
    });
	
	$('.featured-content-promo-slider').slick({
        infinite: true,
		autoplay: true,
        speed: 500,
        arrows: false,
		dots: true
    });
	
	$('.more-videos-slider').slick({
		infinite: true,
		autoplay: true,
		slidesToShow: 3,
		slidesToScroll: 3,
        speed: 500,
        arrows: true,
		dots: false,
		responsive: [
			{
			  breakpoint: 960,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 640,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
		]
    });
	
	//Favorite button script
	$('.footer-container .favorite').on('click', function(){
		$(this).toggleClass('active');	
	});
	
	//Top multilevel menu
	$('#top-menu').dlmenu({
		animationClasses : { classin : 'dl-animate-in-1', classout : 'dl-animate-out-1' }
	});
	
	
	//Dropdown menu
	$('.dropdown-toggle').click(function(){		
		$(this).next('.dropdown').slideToggle(20);
		$(this).toggleClass('dropdown-open');
	});
	
	//accordion content
	$('.accordion-group').ariaAccordion({
		contentRole: ['document', 'application', 'document'],
		expandOnPageLoad: true,
		expandOnlyOne: true,
		slideSpeed: 400
	});
	
	//Enroll now fixed button
	$(document).scroll(function () {
		if($('.enroll-now-button').length){
			var ht = $(this).scrollTop();
			var navWrap = $('.course-info').offset().top;
			if (ht > navWrap) {
				$('.enroll-now-button').addClass('sticky');
			} else {
				$('.enroll-now-button').removeClass('sticky');
			}
		}
	});
	
	//Colorbox
	$('.modal-link').colorbox({
        inline: true, width: '100%', opacity: '0.10', closeButton:false
    });
	$('.modal-link80').colorbox({
        inline: true, width: '80%', opacity: '0.10', closeButton:false
    });

    $('.modal-link-episode').colorbox({
        inline: true, width: '100%', opacity: '0.10', closeButton:false, onComplete:function(){
            $('.all-episodes-slider').slick({
                infinite: true,
                autoplay: false,
                speed: 500,
                arrows: true,
                dots: false
            });
        }
    });

    $('.quick-lesson-video').colorbox({
        inline: true, width: '100%', opacity: '0.10', closeButton:false, 
		onComplete:function(){
			$('.quick-lesson-video-slider').slick({
				infinite: true,
				autoplay: false,
				speed: 500,
				arrows: true,
				dots: false
			});
		}
    });
    $('.select-author-content').change(function(){
    	$('.author-article').hide();
        $('#' + $(this).val()).show();
    });
	$('.modal-close').on('click', function(){
		$.colorbox.close();	
	});
	
	//Member page menu
	$('#main-menu').smartmenus({
		subMenusSubOffsetX: 1,
		subMenusSubOffsetY: -8
	});
	
	//Multi select menu
	$('.select-multi-course').fSelect({
		placeholder: 'Select Course(s)',
		searchText: 'Search Course(Upto 3)'	
	});
	
	//Tab + Accordion
	$('.resTabs').easyResponsiveTabs({
		type: 'default', //Types: default, vertical, accordion
		width: 'auto', 
		fit: true, 
		tabidentify: 'defaultTabs' 
	});
	
	//Top menu
	$('#nav-icon4').click(function() {
		$(this).toggleClass('open');
		$('nav').toggleClass('show_nav');
		$('#cssmenu').toggleClass('hide');
	});
	
	//Multi Select
	if($('.multiselect-listbox').length){
		$('.multiselect-listbox').multiSelect();
	}
	
	//add-material-icons script
	$('.add-material-icons a').on('click', function(){
		$(this).toggleClass('active');	
	});
	
	// Mobile sub menu
	if($(window).width() < 961){
		$('.members-links li:nth-child(4)').after('<li><a href="#" class="open-mobile-menu"><span class="icon menu-close"></span></a></li>');
		mobileSubMenu();
		
		//Copy paste heder menu to header bottom
		$('.member-left-links .user-info').insertBefore('#cssmenu > ul');
	}
});

function mobileSubMenu(){		
	var subMenuHt = $('.members-links').height();
	var firstMenuHt = $('.members-links li:nth-child(1)').height();
	$('.member-left-links').css('bottom', -(subMenuHt - firstMenuHt) );
	
	$('.open-mobile-menu').on('click', function(){
		$('.member-left-links').toggleClass('menuOpen');
		$('.icon.menu-close').closest('li').toggleClass('active');
		checkStatus();
	});
	
	function checkStatus(){
		if($('.member-left-links').hasClass('menuOpen')){
			$('.member-left-links').css('bottom', 0);
		} else {			
			$('.member-left-links').css('bottom', -(subMenuHt - firstMenuHt) );
		}
	}	
}

$(document).on('click', function (e) {
	if(!$(".dropdown-toggle").is(e.target) && !$(".dropdown-toggle").has(e.target).length){
		$('.dropdown').slideUp(20);
		$('.dropdown-toggle').removeClass('dropdown-open');
	}                       
});

