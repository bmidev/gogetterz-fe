@extends('desktop.layouts.master')
@section('content')

<section class="login-registration">
    <div class="m-page-container">
    	<div class="copy-container">
        	<h2 class="heading__h2">Enter Your Details</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
        <div class="process-container">
        	<div class="form-container step1">
            	<form>
                    <div class="form-element">
                        <input name="txtUsername" type="text" id="txtUsername" placeholder="Username/Email ID">
                    </div>
                    <div class="form-element">
                        <input name="txtPassword" type="password" id="txtPassword" placeholder="Password">
                    </div>
                    <div class="form-element button-container">
                        <input name="btnLogin" type="submit" id="btnLogin" value="Log In" class="button lightpink midsize full-width">
                    </div>
                    <div class="note">
                    	<a href="#">Forgot Password?</a>
                    </div>
                    <div class="social-links top-margin clear">
                        <div class="form-element float-left">
                            <a href="#" title="Log In using Fb" class="button grey-fill no-hover tinysize full-width">Log In using <span class="icon-fb"></span></a>
                        </div>
                        <div class="form-element float-right">
                            <a href="#" title="Log In using Tw" class="button grey-fill no-hover tinysize full-width">Log In using <span class="icon-tw"></span></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@stop

