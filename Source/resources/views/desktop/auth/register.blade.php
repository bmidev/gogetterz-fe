@extends('desktop.layouts.master')
@section('content')
<section class="login-registration">
    <div class="m-page-container">
    	<div class="copy-container">
        	<h2 class="heading__h2">Join GoGetterz</h2>
            <p>Sign Up for a free account and get free access to <strong>500 Courses!</strong> </p>
        </div>
        <div class="process-container" ng-controller="RegisterController" ng-cloak>
        	<div class="form-container step1">
                <form method="POST" name="register" ng-submit="signUp()" autocomplete="off">
                	<div class="social-links clear">
                        <div class="form-element float-left">
                            <a href="#" title="Log In using Fb" class="button grey-fill no-hover tinysize full-width">Sign Up using <span class="icon-fb"></span></a>
                        </div>
                        <div class="form-element float-right">
                            <a href="#" title="Log In using Tw" class="button grey-fill no-hover tinysize full-width">Sign Up using <span class="icon-tw"></span></a>
                        </div>
                    </div>
                    <div class="or">OR</div>
                    <div class="note center">
                    	Sign Up with GoGetterz
                    </div>
                    <div class="form-element" ng-class="{error: errors.userName}">
                        {!! Form::text('userName', old('userName'), ['id' => 'userName', 'placeholder' => 'Username', 'ng-class' => '{error: errors.userName}', 'ng-change' => "errors.userName = ''", 'ng-model' => 'users.userName']) !!}
                        <span class="error-message" ng-show="errors.userName">[[ errors.userName[0] ]]</span>
                    </div>
                    <div class="form-element" ng-class="{error: errors.email}">
                        {!! Form::text('email', old('email'), ['id' => 'email', 'placeholder' => 'Email ID', 'ng-class' => '{error: errors.email}', 'ng-change' => "errors.email = ''", 'ng-model' => 'users.email']) !!}
                        <span class="error-message" ng-show="errors.email">[[ errors.email[0] ]]</span>
                    </div>
                    <div class="form-element" ng-class="{error: errors.password}">
                        {!! Form::password('password', ['id' => 'password', 'placeholder' => 'Password', 'ng-class' => '{error: errors.password}', 'ng-change' => "errors.password = ''", 'ng-model' => 'users.password']) !!}
                        <span class="error-message" ng-show="errors.password">[[ errors.password[0] ]]</span>
                    </div>
                    <div class="form-element" ng-class="{error: errors.password_confirmation}">
                        {!! Form::password('password_confirmation', ['id' => 'password_confirmation', 'placeholder' => 'Confirm Password', 'ng-class' => '{error: errors.password_confirmation}', 'ng-change' => "errors.password_confirmation = ''", 'ng-model' => 'users.password_confirmation']) !!}
                        <span class="error-message" ng-show="errors.password_confirmation">[[ errors.password_confirmation[0] ]]</span>
                    </div>
                    <div class="error-message" id="registerValidate"></div>
                    <div class="form-element">
                        <input name="btnNext" type="submit" id="btnNext" value="Next" class="button lightpink midsize full-width">
                    </div>                    
                </form>
            </div>
            <div class="form-container step2" style="display:none;">
                <form method="POST" name="add-interest" ng-submit="addInterest()" autocomplete="off">
                    
                    <div class="checkbox">
                        <input id="chk1" type="checkbox" name="" class="css-checkbox" >
                        <label for="chk1" class="css-label">Creative &amp; Design</label>
                    </div>
                    
                    <div class="error-message"></div>
                    <div class="form-element">
                        <input name="btnNext" type="submit" id="btnNext" value="Next" class="button lightpink midsize full-width">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@stop

