<!DOCTYPE html>
<html lang="en" ng-app="siteApp">
<head>
    <!-- Google Tag Manager -->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});
        var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })
        (window,document,'script','dataLayer','GTM-5QSGZB8');
    </script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>GoGetterz</title>

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

    <!-- FACEBOOK METAS -->
    <meta property="fb:app_id" content="{{ FACEBOOK_APP_ID }}" />
    <meta property="og:title" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content="{{ BASE_URL }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ BASE_URL . 'desktop/images/share-image.jpg' }}" />    
    
    <link href="{{ asset(BASE_URL . 'desktop/css/vendor.css') }}" rel="stylesheet">
    <link href="{{ asset(BASE_URL . 'desktop/css/app.css') }}" rel="stylesheet">
    
    <script src="{{ asset(BASE_URL . 'desktop/js/modernizr.js') }}"></script>

    {{-- [START] Prevent IFrame Attack --}}
    <style>html{visibility:hidden;}</style>
    <script>if(self==top){document.documentElement.style.visibility='visible';}else{top.location=self.location;}</script>
    {{-- [END] Prevent IFrame Attack --}}
</head>
<body>

<header class="main-menu">
	<div class="left-links">
    	<ul>
        	<li id="top-menu" class="">
            	{{--<a href="#" class="icon-menu" title="Menu"></a>--}}
                <div id="nav-icon4" class="">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div id="cssmenu" class="hide">
                    <ul>
                        <li><a href='#'><span>Home</span></a></li>
                        <li class="has-sub"><a><span>Catalogue</span></a>
                            <ul>
                                <li><a href='#'><span>All Courses</span></a></li>
                                <li><a href='#'><span>Languages</span></a></li>
                                <li><a href='#'><span>Creative</span></a></li>
                                <li><a href='#'><span>Business</span></a></li>
                                <li><a href='#'><span>Health & beauty</span></a></li>
                                <li><a href='#'><span>It & technology</span></a></li>
                                <li><a href='#'><span>Hobbies & culture</span></a></li>
                                <li><a href='#'><span>Money</span></a></li>
                                <li><a href='#'><span>Certifications</span></a></li>
                                <li><a href='#'><span>Japanese culture & tourism</span></a></li>
                                <li><a href='#'><span>Cooking</span></a></li>
                            </ul>
                        </li>
                        <li><a href='#'><span>About Us</span></a></li>
                        <li><a href='#'><span>Contact Us</span></a></li>
                        <li class="has-sub"><a><span>Currency</span></a>
                            <ul>
                                <li><a href='#'><span>US Dollar</span></a></li>
                                <li><a href='#'><span>Japanese Yen</span></a></li>
                                <li><a href='#'><span>Indian Rupees</span></a></li>
                            </ul>
                        </li>
                        <li class="last"><a href='#'><span>Our Voice</span></a></li>
                        <hr class="mobile"/>
                        <li class="mobile imp"><a href="#"><span>Enterprise</span></a></li>
                        <li class="mobile imp"><a href="#"><span>Be an expert</span></a></li>
                        <hr class="mobile"/>
                        <li class="mobile login-li">
                            <a href="{{ BASE_URL }}login" class="li-btn"><span>Log In</span></a>
                            <a href="{{ BASE_URL }}register" class="li-btn"><span>Sign Up</span></a>
                        </li>
                        <li class="mobile inline"></li>

                    </ul>
                </div>
            </li>
            <li class="logo-app"><a href="{!! BASE_URL !!}"><img src="{{ BASE_URL . 'desktop/images/mobile-logo-gogetterz.svg' }}" alt=""/></a></li>
            <li class="top-search">
            	<a href="#" class="icon-search" title="Search"></a>
            	<div class="quich-search">
                	<input type="text" placeholder="| Search Courses">
                    <input type="submit" title="Submit" value="">
                </div>
            </li>
            <li class="language"><a href="#" class="switch-lang" title="Language">EN</a></li>
        </ul>
    </div>
	<nav class="center-links">
   	    <ul>
   	        <li><a href="#">Enterprise</a></li>
            <li class="logo"><a href="{!! BASE_URL !!}"><img src="{{ BASE_URL . 'desktop/images/logo-gogetterz.svg' }}" alt=""/></a></li>
            <li><a href="#">Be an expert</a></li>
        </ul>
   	</nav>    
    <div class="right-menu">
    	<nav class="registration-links nonmobile">
    	    <ul>
    	        <li><a href="{{ BASE_URL }}login">Log In</a></li>
                <li><a href="{{ BASE_URL }}register">Sign Up</a></li>
	        </ul>
    	</nav>
        {{--<nav class="account-dropdown">
        	<a href="#" class="notification-no">3</a>
            <a class="dropdown-toggle" href="#" title="Menu">Hello. johndoe121</a>
            <ul class="dropdown">
                <li><a href="#" class="dashboard"><span></span> Dashboard</a></li>
                <li><a href="#" class="signout"><span></span> Sign Out</a></li>
            </ul>
        </nav>--}}
    </div>
</header>
<div class="main-container">
	@yield('content')
</div>
<footer>
    <div class="page-container">
        <div class="footer-container row">
            <div class="col s12 m4">
                <ul class="footer-links">
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                </ul>
            </div>
            <div class="col s12 m4">
            	<ul class="social-links">
                    <li><a href="#" class="tw"></a></li>
                    <li><a href="#" class="fb"></a></li>                    
                    <li><a href="#" class="ig"></a></li>
                </ul>
            </div>
            <div class="col s12 m4">
                <ul class="footer-links">
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                </ul>
            </div>
        </div>
        <div class="copyrights">&copy; 2018 | Powered by Brandmovers India</div>
    </div>
</footer>

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId            : "{!! FACEBOOK_APP_ID !!}",
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v2.12'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
     

@if (app()->environment() === 'production')
    GA code -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-116706502-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!--GA code end
@endif

<!-- Scripts -->
<script>
    var APP = APP || {};
    APP.baseUrl = "{!! BASE_URL !!}";
    APP.FACEBOOK_APP_ID = "{!! FACEBOOK_APP_ID !!}";
</script>

<script src="{{ asset(BASE_URL . 'desktop/js/app.js') }}"></script>
<script src="{{ asset(BASE_URL . 'desktop/js/vendor.js') }}"></script>
<script src="{{ asset(BASE_URL . 'desktop/js/site.js') }}"></script>
@yield('script')
</body>
</html>
