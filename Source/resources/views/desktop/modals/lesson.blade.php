    <div id="view-lesson" class="modal-box">
        <div class="modal-container modal-lesson">
        	<div class="close-button">
            	<a href="#" class="modal-close"></a>
            </div>
        	<h3 class="heading__h3">Chapter 1 Lesson 1</h3>
            <div class="video-container">
            	<iframe width="560" height="315" src="https://www.youtube.com/embed/WDgy1p6dC2Q?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="lesson-info">
            	Vel turpis nunc eget lorem dolor sed viverra. Facilisi morbi tempus iaculis urna id volutpat lacus laoreet non. Tincidunt ornare massa eget egestas purus. Mattis aliquam faucibus purus in massa tempor nec. Ipsum consequat nisl vel pretium lectus. Vulputate dignissim suspendisse in est ante. Eu ultrices vitae auctor eu augue. Sed adipiscing diam donec adipiscing. Id semper risus in hendrerit gravida rutrum quisque non tellus. Nullam vehicula ipsum a arcu cursus vitae. Faucibus purus in massa tempor nec. 
            </div>
            <div class="more-videos">
            	<h4 class="heading__h4">More Videos</h4>
                <div class="more-videos-slider">
                    <div>
                        <article class="course">
                            <figure> <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course04.jpg') }})"></span></a> </figure>
                            <div class="article-info">
                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>                                
                            </div>
                        </article>
                    </div>
                    <div>
                        <article class="course">
                            <figure> <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a> </figure>
                            <div class="article-info">
                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                            </div>
                        </article>
                    </div>
                    <div>
                        <article class="course disabled">
                            <figure><span class="overlay">$500</span><a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a> </figure>
                            <div class="article-info">
                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                            </div>
                        </article>
                    </div>
                    <div>
                        <article class="course">
                            <figure> <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a> </figure>
                            <div class="article-info">
                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
