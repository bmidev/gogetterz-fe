<div id="featured-episodes" class="modal-box">
    <div class="modal-container modal-lesson">
        <div class="close-button">
            <a href="#" class="modal-close"></a>
        </div>
        <div class="all-episodes-slider more-videos">
            <div>
                <h3 class="heading__h3">Episode 1 : Name of the episode comes here</h3>
                <div class="video-container">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/WDgy1p6dC2Q?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div>
                <h3 class="heading__h3">Episode 2 : Name of the episode comes here</h3>
                <div class="video-container">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/WDgy1p6dC2Q?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>

        <div class="lesson-info">
            <div class="button-container">
                <a href="#" class="button pink midsize small">Pay ¥100 to watch Episode 1</a> <span class="or">or</span>
                <a href="#" class="button pink midsize small ">Pay ¥800 to watch all episodes</a>
            </div>
        </div>
    </div>
</div>
