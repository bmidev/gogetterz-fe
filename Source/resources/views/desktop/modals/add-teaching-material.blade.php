    <div id="add-teaching-material" class="modal-box">
        <div class="modal-container black teaching-material-modal">
        	<div class="close-button">
            	<a href="#" class="modal-close white"></a>
            </div>
        	<h4 class="heading__h4 text-center">Add Teaching Material</h4>
            
            <div class="add-material-icons">
                <ul class="row">
                    <li class="col s2">
                        <a href="javascript:;" class="icon-placeholder">
                            <figure>
                            	<span class="icon icon-video"></span>
                                <figcaption>Video</figcaption>
                            </figure>
                        </a>
                    </li>
                    <li class="col s2">
                        <a href="javascript:;" class="icon-placeholder">
                            <figure>
                            	<span class="icon icon-audio"></span>
                                <figcaption>Audio</figcaption>
                            </figure>
                        </a>
                    </li>
                    <li class="col s2">
                        <a href="javascript:;" class="icon-placeholder">
                            <figure>
                            	<span class="icon icon-pdf"></span>
                                <figcaption>PDF</figcaption>
                            </figure>
                        </a>
                    </li>
                    <li class="col s2">
                        <a href="javascript:;" class="icon-placeholder">
                            <figure>
                            	<span class="icon icon-text"></span>
                                <figcaption>Text</figcaption>
                            </figure>
                        </a>
                    </li>
                    <li class="col s2">
                        <a href="javascript:;" class="icon-placeholder">
                            <figure>
                            	<span class="icon icon-ms-office"></span>
                                <figcaption>MS Office</figcaption>
                            </figure>
                        </a>
                    </li>
                    <li class="col s2">
                        <a href="javascript:;" class="icon-placeholder">
                            <figure>
                            	<span class="icon icon-test"></span>
                                <figcaption>Test</figcaption>
                            </figure>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="form-container row">
            	<div class="form-element col s4">
                	<div class="file-upload whithe-btn">
                        <input type="file" name="receipt" id="file-browse" class="inputfile" />
                        <label for="file-browse"><span class="input-text">Upload File</span> <strong>&nbsp;</strong></label>
                    </div>
                </div>
                <div class="col s3">
               		<a href="#" class="button lightpink smallsize mid mLeft20">+ Add more</a> 
                </div>
            </div>
            <div class="listing">
            	<h6 class="heading__h6">Note :</h6>
            	<ol>
            	    <li>Supported format for uploading the file : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </li>
            	    <li>Supported format for uploading the file : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea </li>
        	    </ol>
            </div>
    </div>
    </div>
