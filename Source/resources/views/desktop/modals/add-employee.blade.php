    <div id="add-employee-data" class="modal-box modal-forms">
        <div class="modal-container ">
        	<div class="close-button">
            	<a href="#" class="modal-close"></a>
            </div>
        	<h4 class="heading__h4">Add Employee</h4>            
            
            <div class="form-container">
            	<div class="form-element">
                	<label for="" class="title">Full Name</label>
                    <input type="text" placeholder="Employee Name">
                </div>
                <div class="form-element">
                	<label for="" class="title">Email ID</label>
                    <input type="text" placeholder="Employee Email ID">
                </div>
                <div class="form-element">
                	<label for="" class="title">Department</label>
                    <div class="right-container">
                        <select class="custom-select small-size small-padding full-width grey">
                            <option>Select Department</option>
                        </select>
                    </div>
                </div>
                <div class="form-element">
                	<div class="left-container">&nbsp;</div>
                    <div class="right-container">
                        <div class="custom-checkbox">
                            <input id="chkAppointModerator" type="checkbox" name="chkAppointModerator" class="styled-checkbox" >
                            <label for="chkAppointModerator" class="">Appoint Moderator</label>
                        </div>
                    </div>
                </div>
                <div class="button-container text-right">
               		<a href="#" class="button grey-fill smallsize mid">Save</a> 
                </div>
            </div>
    	</div>
    </div>
