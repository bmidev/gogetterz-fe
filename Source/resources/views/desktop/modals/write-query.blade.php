    <div id="write-your-query" class="modal-box modal-forms">
        <div class="modal-container ">
        	<div class="close-button">
            	<a href="#" class="modal-close"></a>
            </div>
        	<h4 class="heading__h4 text-center">WRITE YOUR QUERY</h4>            
            
            <div class="form-container">
            	<div class="form-element">
                	<label for="" class="title">Course Name</label>
                    <input type="text" placeholder="">
                </div>                
                <div class="form-element">
                	<label for="" class="title">Chapter</label>
                    <input type="text" placeholder="">
                </div>
                <div class="form-element">
                	<label for="" class="title">Lesson</label>
                    <input type="text" placeholder="">
                </div>
                <div class="form-element">
                	<label for="" class="title">Query</label>
                    <textarea rows="5"></textarea>
                </div>
                <div class="form-element button-container">
                	<div class="left-container">&nbsp;</div>
                    <div class="right-container">
                		<input type="submit" class="button grey-fill tinysize mid" value="Submit">
                    </div>
                </div>
            </div>
    	</div>
    </div>
