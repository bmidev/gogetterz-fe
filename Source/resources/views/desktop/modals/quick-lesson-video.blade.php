<div id="quick-lesson-modal" class="modal-box">
    <div class="modal-container modal-lesson">
        <div class="close-button">
            <a href="#" class="modal-close"></a>
        </div>
        <div class="quick-lesson-video-slider more-videos">
            <div>
                <h3 class="heading__h3">Trust Yourself</h3>
                <div class="video-container">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/WDgy1p6dC2Q?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="lesson-info quick-lesson-info">
                    Vel turpis nunc eget lorem dolor sed viverra. Facilisi morbi tempus iaculis urna id volutpat lacus laoreet non. Tincidunt ornare massa eget egestas purus. Mattis aliquam faucibus purus in massa tempor nec. Ipsum consequat nisl vel pretium lectus. Vulputate dignissim suspendisse in est ante. Eu ultrices vitae auctor eu augue. Sed adipiscing diam donec adipiscing. Id semper risus in hendrerit gravida rutrum quisque non tellus. Nullam vehicula ipsum a arcu cursus vitae. Faucibus purus in massa tempor nec.
                </div>
            </div>
            <div>
                <h3 class="heading__h3">Trust Yourself</h3>
                <div class="video-container">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/WDgy1p6dC2Q?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="lesson-info quick-lesson-info">
                    Vel turpis nunc eget lorem dolor sed viverra. Facilisi morbi tempus iaculis urna id volutpat lacus laoreet non. Tincidunt ornare massa eget egestas purus. Mattis aliquam faucibus purus in massa tempor nec. Ipsum consequat nisl vel pretium lectus. Vulputate dignissim suspendisse in est ante. Eu ultrices vitae auctor eu augue. Sed adipiscing diam donec adipiscing. Id semper risus in hendrerit gravida rutrum quisque non tellus. Nullam vehicula ipsum a arcu cursus vitae. Faucibus purus in massa tempor nec.
                </div>
            </div>
        </div>

        <div class="lesson-info quick-lesson-info">
            <div class="button-container text-center">
                <a href="#" class="button grey-fill midsize small">View More</a>

            </div>
        </div>
    </div>
</div>
