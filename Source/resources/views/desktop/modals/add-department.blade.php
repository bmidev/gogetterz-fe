    <div id="add-department" class="modal-box modal-forms">
        <div class="modal-container ">
        	<div class="close-button">
            	<a href="#" class="modal-close"></a>
            </div>
        	<h4 class="heading__h4">Add Department</h4>            
            
            <div class="form-container">
            	<div class="form-element">
                	<label for="" class="title">Name</label>
                    <input type="text" placeholder="Department Name">
                </div>                
                <div class="form-element">
                	<label for="" class="title">Add Employees</label>
                    <div class="right-container">
                        <select class="custom-select small-size small-padding full-width grey">
                            <option>Type Employee Name</option>
                        </select>
                    </div>
                </div>
                
                <div class="button-container text-right">
               		<a href="#" class="button grey-fill smallsize mid">Save</a> 
                </div>
            </div>
    	</div>
    </div>
