<div id="expert-video-modal" class="modal-box">
    <div class="modal-container modal-lesson">
        <div class="close-button">
            <a href="#" class="modal-close"></a>
        </div>
        <div class="more-videos">
            <div>
                <h3 class="heading__h3">Trust Yourself</h3>
                <div class="video-container">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/WDgy1p6dC2Q?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
