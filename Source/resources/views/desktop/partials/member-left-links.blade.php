		<aside class="member-left-links">
            <div class="user-info">
            	<figure class="user-img" style="background-image:url({{ asset('desktop/images/user.svg') }})">
                	<a href="#" class="edit-icon"></a>
                </figure>
                <div class="user-data">
                	<h4 class="heading__h4">Mark Jones</h4>
                    <div class="points"><span>1200 Points</span></div>
                </div>
            </div>
            <nav class="members-links">
            	<ul>
            	    <li class="active">
                    	<a href="{{ BASE_URL }}members-profile" ><span class="icon setting-nav"></span> Profile settings</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}members-dashboard" class=""><span class="icon dashboard-nav"></span> Dashboard</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}members-categories" class=""><span class="icon categories-nav"></span> Categories</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}members-courses" class=""><span class="icon courses-nav"></span> Courses</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}member-notifications" class=""><span class="icon notifications-nav"></span> Notifications</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}member-messages" class=""><span class="icon messages-nav"></span> Messages</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}member-certifications" class=""><span class="icon certifications-nav"></span> Certifications</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}member-points" class=""><span class="icon points-nav"></span> Points</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}member-info-help" class=""><span class="icon help-nav"></span> Help</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}member-quick-lesson" class=""><span class="icon quick-lesson"></span> Quick Lesson</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}member-online-course-management" class=""><span class="icon course-management-nav"></span> Online course management</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}member-sales-management" class=""><span class="icon sales-management-nav"></span> Sales management</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}member-event-management" class=""><span class="icon event-management-nav"></span> Event management</a>
                    </li>
        	    </ul>
            </nav>
        </aside>