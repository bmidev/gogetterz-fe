		<aside class="member-left-links">
            <div class="user-info">
            	<figure class="user-img" style="background-image:url({{ asset('desktop/images/user.svg') }})">
                	<a href="#" class="edit-icon"></a>
                </figure>
                <div class="user-data">
                	<h4 class="heading__h4">Mark Jones</h4>
                    <div class="points">Admin | ABC Corp</div>
                </div>
            </div>
            <nav class="members-links">
            	<ul>
                	<li>
                    	<a href="{{ BASE_URL }}members-dashboard" class=""><span class="icon dashboard-nav"></span> Dashboard</a>
                    </li>
            	    <li>
                    	<a href="{{ BASE_URL }}employee-database" class="active"><span class="icon setting-nav"></span> Employees</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}employee-courses" class=""><span class="icon courses-nav"></span> Courses</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}employee-billings" class=""><span class="icon billings"></span> Billings</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}member-notifications" class=""><span class="icon notifications-nav"></span> Notifications</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}member-messages" class=""><span class="icon messages-nav"></span> Messages</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}employee-settings" class=""><span class="icon settings"></span> Settings</a>
                    </li>
                    <li>
                    	<a href="{{ BASE_URL }}member-info-help" class=""><span class="icon help-nav"></span> Help</a>
                    </li>                    
        	    </ul>
            </nav>
        </aside>