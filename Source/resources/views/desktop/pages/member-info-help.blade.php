@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<div class="left">
                    <h3 class="heading__h3">Information &amp; Help</h3>
                </div>
                <div class="right">
                	<section class="search-courses"><!--Css in _course.scss-->
                        <div class="form-container red">
                            <input type="text" placeholder="Search Messages">
                            <input type="submit" title="Submit" value="">
                        </div>
                    </section>
                </div>
            </div>
            <section class="content-grey-box no-top-border">            	
				<div class="content-white-box">
                	<h3 class="heading__h3">For any information, write to us!</h3>
                    <div class="info-help-container">
                    	<div class="data-container">
                        	<h4 class="heading__h4">Write a Query!</h4>
                            <div class="form-container theme-white no-padding">
                            	<div class="form-element">
                                	<input name="" type="text" id="txtFullName" placeholder="Full Name" class="full-width">
                                </div>
                                <div class="form-element">
                                	<input name="" type="text" id="txtEmailID" placeholder="Email ID" class="full-width">
                                </div>
                                <div class="form-element">
                                	<input name="" type="text" id="txtCompanyName" placeholder="Company Name" class="full-width">
                                </div>
                                <div class="form-element">
                                	<textarea rows="4" class="full-width" id="txtMessage" placeholder="Message"></textarea>
                                </div>
                                <div class="button-container text-right">
                                	<a href="#" class="button lightpink tinysize small">Submit</a>
                                </div>
                            </div>
                        </div>
                    	<div class="img-container">
                        	<img src="{{ asset('desktop/images/img-info-help.svg') }}" alt="" />
                        </div>
                    </div>
                </div>
            </section>
            
        </div>
    </div>
</div>
@stop

