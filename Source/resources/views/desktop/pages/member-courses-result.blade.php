@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading clear">
            	<div class="left">
            	<nav class="member-heading-nav"> 
                    <!-- Sample menu definition -->
                    <ul id="main-menu" class="sm sm-simple">
                        <li><a href="#">Courses &amp; Catalogue</a>
                            <ul>
                                <li><a href="#">Languages</a></li>
                                <li><a href="#">Health &amp; wellbeing</a></li>
                                <li><a href="#">Technology &amp; design</a></li>
                                <li><a href="#">Cool japan</a>
                                    <ul>
                                        <li><a href="#">Sub-Category of the Course</a></li>
                                        <li><a href="#">Sub-Category of the Course</a></li>
                                        <li><a href="#">Sub-Category of the Course</a></li>
                                        <li><a href="#">Sub-Category of the Course</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>                                                
                    </ul>
                </nav>
                </div>
                <div class="right">
                	<section class="search-courses"><!--Css in _course.scss-->
                        <div class="form-container red">
                            <input type="text" placeholder="Search Courses/Subjects">
                            <input type="submit" title="Submit" value="">
                        </div>
                    </section>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="back-link">
                    <a href="#">< Go Back</a> 
                </div>
                <h4 class="heading__h4 caps">Search Results</h4>
                <div class="search-results">        			 
                    <article class="course bg-light-pink">
                        <figure>
                            <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                        </figure>
                        <div class="article-info">
                            <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                            <div class="author"><a href="#">Arighna Sarkar</a></div>
                            <div class="footer-container">
                                <button class="favorite"></button>
                                <div class="rating">
                                    New
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="course bg-light-pink">
                        <figure>
                            <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                        </figure>
                        <div class="article-info">
                            <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                            <div class="author"><a href="#">Arighna Sarkar</a></div>
                            <div class="footer-container">
                                <button class="favorite"></button>
                                <div class="rating">
                                    <span class="star"></span> 4.5
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="course bg-light-pink">
                        <figure>
                            <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                        </figure>
                        <div class="article-info">
                            <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                            <div class="author"><a href="#">Arighna Sarkar</a></div>
                            <div class="footer-container">
                                <button class="favorite"></button>
                                <div class="rating">
                                    New
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="course bg-light-pink">
                        <figure>
                            <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                        </figure>
                        <div class="article-info">
                            <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                            <div class="author"><a href="#">Arighna Sarkar</a></div>
                            <div class="footer-container">
                                <button class="favorite"></button>
                                <div class="rating">
                                    <span class="star"></span> 4.5
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                
                <div class="tab-data">
                    <div class="resTabs">
                        <ul class="resp-tabs-list defaultTabs">
                            <li>Active courses</li>
                            <li>Suggested courses</li>
                            <li><span class="icon-favourite"></span>Favourites</li>
                        </ul>
                        <div class="resp-tabs-container defaultTabs">
                            <div> 
                            	<article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                New
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                <span class="star"></span> 4.5
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                New
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                <span class="star"></span> 4.5
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                New
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                <span class="star"></span> 4.5
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                New
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                <span class="star"></span> 4.5
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div>                             	
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                <span class="star"></span> 4.5
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                New
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                <span class="star"></span> 4.5
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                New
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                <span class="star"></span> 4.5
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                New
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                <span class="star"></span> 4.5
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                New
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div> 
                            	<article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                New
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                <span class="star"></span> 4.5
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                New
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                <span class="star"></span> 4.5
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                New
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                <span class="star"></span> 4.5
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                New
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                        <div class="footer-container">
                                            <button class="favorite"></button>
                                            <div class="rating">
                                                <span class="star"></span> 4.5
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
        </div>
    </div>
</div>

@stop

