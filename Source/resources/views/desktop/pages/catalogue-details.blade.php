@extends('desktop.layouts.master')
@section('content')

<section class="category-promo">
    <div class="m-page-container">
        <div class="category-promo-image">
        	<img src="{{ asset('desktop/images/jp/category-promo-language.jpg') }}" alt=""/>
        </div>
        <div class="welcome-copy">
            <h2 class="heading__h2">Language Courses</h2>
            <p>Ornare quam viverra orci sagittis eu volutpat. Sed sed risus pretium quam vulputate dignissim suspendisse in est. Sed id semper risus in hendrerit gravida rutrum quisque non. Bibendum neque egestas congue quisque egestas diam in arcu cursus. </p>
            <p>Dignissim suspendisse in est ante in nibh. Facilisi morbi tempus iaculis urna id volutpat lacus laoreet. Eget mauris pharetra et ultrices neque ornare aenean euismod. Arcu odio ut sem nulla pharetra diam. </p>
            
        </div>
    </div>
</section>    
<section class="course-detais-view">
    <div class="m-page-container">
        <div class="course-header-links">
        	<div class="sort-by">
            	<select class="custom-select">
            	    <option>SORT BY</option>
            	    <option>Alphabetically A-Z</option>
            	    <option>Free Courses</option>
            	    <option>Most Popular</option>
            	    <option>Most Relevant</option>
            	</select>
            </div>
            <div class="select-lang">
            	<select class="custom-select">
            	    <option>ENGLISH LANGUAGE</option>
            	    <option>All Languages</option>
            	    <option>Japanese Language</option>
            	    <option>Japanese Language</option>
            	</select>
            </div>
            {{--<div class="filter">
            	<a href="#" class="button grey-fill midsize mid caps">filter</a>
            </div>--}}
        </div>
        <div class="course-articles">
        	<div class="article-wrapper">
                <article class="course">
                    <figure>
                    	<span class="overlay">Free</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">English For Beginners</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	New
                            </div>
                        </div>
                    </div>
                </article>
        	</div>
            <div class="article-wrapper">
                <article class="course">
                    <figure>
                    	<span class="overlay">Free</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
        	</div>
            <div class="article-wrapper">
                <article class="course">
                    <figure>
                    	<span class="overlay">Free</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	New
                            </div>
                        </div>
                    </div>
                </article>
        	</div>
            <div class="article-wrapper">
                <article class="course">
                    <figure>
                    	<span class="overlay">¥500</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">English For Beginners</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	New
                            </div>
                        </div>
                    </div>
                </article>
        	</div>
            <div class="article-wrapper">
                <article class="course">
                    <figure>
                    	<span class="overlay">¥500</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
        	</div>
            <div class="article-wrapper">
                <article class="course">
                    <figure>
                    	<span class="overlay">¥500</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	New
                            </div>
                        </div>
                    </div>
                </article>
        	</div>
            <div class="article-wrapper">
                <article class="course">
                    <figure>
                    	<span class="overlay">¥500</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">English For Beginners</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	New
                            </div>
                        </div>
                    </div>
                </article>
        	</div>
            <div class="article-wrapper">
                <article class="course">
                    <figure>
                    	<span class="overlay">¥500</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
        	</div>
            <div class="article-wrapper">
                <article class="course">
                    <figure>
                    	<span class="overlay">¥500</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	New
                            </div>
                        </div>
                    </div>
                </article>
        	</div>
        </div>
    </div>
</section> 
@stop

