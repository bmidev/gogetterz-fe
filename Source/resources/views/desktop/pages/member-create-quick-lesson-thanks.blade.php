@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<h3 class="heading__h3">Create Quick Lesson</h3>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="member-breadcrumb">
               		<a href="{{ BASE_URL }}member-quick-lesson">Quick Lesson</a> 
                    <span class="divider"> > </span> 
                    Create Quick Lesson
                </div>
				<div class="content-white-box">
                    <div class="tranks-certificate">
                        <h3 class="heading__h3">Thanks for uploading your Quick Lesson!</h3>
                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                        <h3 class="heading__h3">Quick Lesson ID : XXXXXXX</h3>
                        <div class="button-container">
                            <a href="#" class="button tinysize small lightpink">Go to Main Page</a>
                        </div>
                    </div>
                </div>                
            </section>
            
        </div>
    </div>
</div>
@stop

