@extends('desktop.layouts.master')
@section('content')

<div class="page-container">
    <div class="learner-container">
    	<aside class="learner-left-links">
        	<div class="link-dashboard">
            	<a href="#">< GO BACK TO DASHBOARD</a>
            </div>
            <div class="chapter-accordion">
            	<div class="accordion-group theme-pink-grey">
                    <section class="accordion-group__accordion">
                        <div class="accordion-group__accordion-head">
                            <h3 class="accordion-group__accordion-heading">
                                <button type="button" class="accordion-group__accordion-btn">Chapter 1</button>
                            </h3>
                        </div>
                        <div class="accordion-group__accordion-panel">
                            <div class="accordion-group__accordion-content">
                                <div class="lessons">
                                    <ul>
                                        <li>
                                            <h5>Lesson 1</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 2</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li class="active">
                                            <h5>Lesson 3</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 4</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 5</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="accordion-group__accordion">
                        <div class="accordion-group__accordion-head">
                            <h3 class="accordion-group__accordion-heading">
                                <button type="button" class="accordion-group__accordion-btn">Chapter 2</button>
                            </h3>
                        </div>
                        <div class="accordion-group__accordion-panel">
                            <div class="accordion-group__accordion-content">
                                <div class="lessons">
                                    <ul>
                                        <li>
                                            <h5>Lesson 1</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 2</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li class="active">
                                            <h5>Lesson 3</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 4</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 5</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="accordion-group__accordion">
                        <div class="accordion-group__accordion-head">
                            <h3 class="accordion-group__accordion-heading">
                                <button type="button" class="accordion-group__accordion-btn">Chapter 3</button>
                            </h3>
                        </div>
                        <div class="accordion-group__accordion-panel">
                            <div class="accordion-group__accordion-content">
                                <div class="lessons">
                                    <ul>
                                        <li>
                                            <h5>Lesson 1</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 2</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li class="active">
                                            <h5>Lesson 3</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 4</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 5</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="accordion-group__accordion">
                        <div class="accordion-group__accordion-head">
                            <h3 class="accordion-group__accordion-heading">
                                <button type="button" class="accordion-group__accordion-btn">Chapter 4</button>
                            </h3>
                        </div>
                        <div class="accordion-group__accordion-panel">
                            <div class="accordion-group__accordion-content">
                                <div class="lessons">
                                    <ul>
                                        <li>
                                            <h5>Lesson 1</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 2</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li class="active">
                                            <h5>Lesson 3</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 4</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 5</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="accordion-group__accordion">
                        <div class="accordion-group__accordion-head">
                            <h3 class="accordion-group__accordion-heading">
                                <button type="button" class="accordion-group__accordion-btn">Chapter 5</button>
                            </h3>
                        </div>
                        <div class="accordion-group__accordion-panel">
                            <div class="accordion-group__accordion-content">
                                <div class="lessons">
                                    <ul>
                                        <li>
                                            <h5>Lesson 1</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 2</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li class="active">
                                            <h5>Lesson 3</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 4</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 5</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </aside>
        <section class="learner-content-area">
        	<div class="learner-heading">
                <div class="left">
                    <h3 class="heading__h3">English Email Tips</h3>
                </div>
                <div class="right">
                	<div class="status-calculator">
                    	<div class="status-percentage">50% Progress</div>
                        <div class="status-bar">
                            <span class="status-bar__value" style="width:50%"></span>                                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="couse-lesson">
                <div class="next-prev-buttons">
                	<div class="next-button"><a href="#">Next lesson</a></div>
                	<div class="prev-button"><a href="#">Previous lesson</a></div>
                </div>
                <div class="course-lesson-copy">
                    <h4 class="heading__h4">Lesson 3 : Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod</h4>
                    <div class="video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/WDgy1p6dC2Q?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                    <div class="lesson-info">
                        <p>Aliquam sem fringilla ut morbi tincidunt augue interdum. Tristique nulla aliquet enim tortor at auctor urna nunc id. Nunc non blandit massa enim nec dui nunc mattis enim. Sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque eu. Sem nulla pharetra diam sit amet nisl. Eget arcu dictum varius duis at consectetur lorem donec. Vitae et leo duis ut diam quam nulla porttitor massa. Fames ac turpis egestas maecenas pharetra convallis. Lacus vel facilisis volutpat est velit egestas. Sodales ut eu sem integer vitae justo eget magna. Ac turpis egestas sed tempus urna et pharetra. Felis imperdiet proin fermentum leo vel orci porta non. Volutpat sed cras ornare arcu. Leo a diam sollicitudin tempor id. Porttitor massa id neque aliquam vestibulum morbi. Accumsan lacus vel facilisis volutpat est velit egestas. </p>
                        <p>Metus dictum at tempor commodo ullamcorper. Aliquam nulla facilisi cras fermentum. Scelerisque felis imperdiet proin fermentum leo. Eu volutpat odio facilisis mauris sit amet massa vitae. Et sollicitudin ac orci phasellus egestas. Facilisi nullam vehicula ipsum a arcu. Platea dictumst quisque sagittis purus sit amet. Pellentesque id nibh tortor id aliquet lectus proin. Odio morbi quis commodo odio aenean. Et ligula ullamcorper malesuada proin libero nunc.</p>
                    </div>
                    <div class="download-material">
                    	<h6 class="heading__h6">DOWNLOAD LESSON MATERIAL :</h6>
                        <ul>
                            <li>
                                <a href="javascript:;" class="icon-placeholder">
                                    <figure>
                                        <span class="icon icon-video"></span>
                                    </figure>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="icon-placeholder">
                                    <figure>
                                        <span class="icon icon-audio"></span>
                                    </figure>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="icon-placeholder">
                                    <figure>
                                        <span class="icon icon-pdf"></span>
                                    </figure>
                                </a>
                            </li>                            
                            <li>
                                <a href="javascript:;" class="icon-placeholder">
                                    <figure>
                                        <span class="icon icon-test"></span>
                                    </figure>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="button-container text-center">
                    	<a href="#write-your-query" class="button lightpink tinysize mid modal-link">Contact the Expert</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!--Modal code-->
<div class="hide">
	@include('desktop.modals.write-query')
</div>

@stop

