@extends('desktop.layouts.master')
@section('content')

<div class="page-container">
    <div class="learner-container">
    	<aside class="learner-left-links">
        	<div class="link-dashboard">
            	<a href="#">< GO BACK TO DASHBOARD</a>
            </div>
            <div class="chapter-accordion">
            	<div class="accordion-group theme-pink-grey">
                    <section class="accordion-group__accordion">
                        <div class="accordion-group__accordion-head">
                            <h3 class="accordion-group__accordion-heading">
                                <button type="button" class="accordion-group__accordion-btn">Chapter 1</button>
                            </h3>
                        </div>
                        <div class="accordion-group__accordion-panel">
                            <div class="accordion-group__accordion-content">
                                <div class="lessons">
                                    <ul>
                                        <li>
                                            <h5>Lesson 1</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 2</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li class="active">
                                            <h5>Lesson 3</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 4</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 5</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="accordion-group__accordion">
                        <div class="accordion-group__accordion-head">
                            <h3 class="accordion-group__accordion-heading">
                                <button type="button" class="accordion-group__accordion-btn">Chapter 2</button>
                            </h3>
                        </div>
                        <div class="accordion-group__accordion-panel">
                            <div class="accordion-group__accordion-content">
                                <div class="lessons">
                                    <ul>
                                        <li>
                                            <h5>Lesson 1</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 2</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li class="active">
                                            <h5>Lesson 3</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 4</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 5</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="accordion-group__accordion">
                        <div class="accordion-group__accordion-head">
                            <h3 class="accordion-group__accordion-heading">
                                <button type="button" class="accordion-group__accordion-btn">Chapter 3</button>
                            </h3>
                        </div>
                        <div class="accordion-group__accordion-panel">
                            <div class="accordion-group__accordion-content">
                                <div class="lessons">
                                    <ul>
                                        <li>
                                            <h5>Lesson 1</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 2</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li class="active">
                                            <h5>Lesson 3</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 4</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 5</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="accordion-group__accordion">
                        <div class="accordion-group__accordion-head">
                            <h3 class="accordion-group__accordion-heading">
                                <button type="button" class="accordion-group__accordion-btn">Chapter 4</button>
                            </h3>
                        </div>
                        <div class="accordion-group__accordion-panel">
                            <div class="accordion-group__accordion-content">
                                <div class="lessons">
                                    <ul>
                                        <li>
                                            <h5>Lesson 1</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 2</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li class="active">
                                            <h5>Lesson 3</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 4</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 5</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="accordion-group__accordion">
                        <div class="accordion-group__accordion-head">
                            <h3 class="accordion-group__accordion-heading">
                                <button type="button" class="accordion-group__accordion-btn">Chapter 5</button>
                            </h3>
                        </div>
                        <div class="accordion-group__accordion-panel">
                            <div class="accordion-group__accordion-content">
                                <div class="lessons">
                                    <ul>
                                        <li>
                                            <h5>Lesson 1</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 2</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li class="active">
                                            <h5>Lesson 3</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 4</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                        <li>
                                            <h5>Lesson 5</h5>
                                            <p>Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </aside>
        <section class="learner-content-area">
        	<div class="learner-heading">
                <div class="left">
                    <h3 class="heading__h3">English Email Tips</h3>
                </div>
                <div class="right">
                	<div class="status-calculator">
                    	<div class="status-percentage">50% Progress</div>
                        <div class="status-bar">
                            <span class="status-bar__value" style="width:50%"></span>                                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="couse-lesson">                
                <div class="course-lesson-copy">
                    <h4 class="heading__h4 text-center caps">Multiple Choice Question Test</h4>                    
                    <div class="question-set">
                    	<div class="question-container">
                        	<div class="question">
                            	<span class="no">Q1.</span>
                                Sem integer vitae justo eget magna fermentum iaculis eu. Proin nibh nisl condimentum id venenatis. Eu tincidunt tortor aliquam nulla facilisi cras fermentum?
                            </div>
                            <div class="question-options">
                            	<div class="radioGroup block">
                                    <div class="radio-item">
                                        <input name="questionOptions1" id="questionOptions11" type="radio" class="radio-btn">
                                        <label for="questionOptions11" class="radio-label">Lorem Ipsum</label>
                                    </div>
                                    <div class="radio-item">
                                        <input name="questionOptions1" id="questionOptions12" type="radio" class="radio-btn">
                                        <label for="questionOptions12" class="radio-label">Arcu dictum varius</label>
                                    </div>
                                    <div class="radio-item">
                                        <input name="questionOptions1" id="questionOptions13" type="radio" class="radio-btn">
                                        <label for="questionOptions13" class="radio-label">Velit euismod</label>
                                    </div>
                                    <div class="radio-item">
                                        <input name="questionOptions1" id="questionOptions14" type="radio" class="radio-btn">
                                        <label for="questionOptions14" class="radio-label">Bibendum est ultricies</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="question-container">
                        	<div class="question">
                            	<span class="no">Q2.</span>
                                Sem integer vitae justo eget magna fermentum iaculis eu. Proin nibh nisl condimentum id venenatis. Eu tincidunt tortor aliquam nulla facilisi cras fermentum?
                            </div>
                            <div class="question-options">
                            	<div class="radioGroup block">
                                    <div class="radio-item">
                                        <input name="questionOptions2" id="questionOptions21" type="radio" class="radio-btn">
                                        <label for="questionOptions21" class="radio-label">Lorem Ipsum</label>
                                    </div>
                                    <div class="radio-item">
                                        <input name="questionOptions2" id="questionOptions22" type="radio" class="radio-btn">
                                        <label for="questionOptions22" class="radio-label">Arcu dictum varius</label>
                                    </div>
                                    <div class="radio-item">
                                        <input name="questionOptions2" id="questionOptions23" type="radio" class="radio-btn">
                                        <label for="questionOptions23" class="radio-label">Velit euismod</label>
                                    </div>
                                    <div class="radio-item">
                                        <input name="questionOptions2" id="questionOptions24" type="radio" class="radio-btn">
                                        <label for="questionOptions24" class="radio-label">Bibendum est ultricies</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="question-container">
                        	<div class="question">
                            	<span class="no">Q3.</span>
                                Sem integer vitae justo eget magna fermentum iaculis eu. Proin nibh nisl condimentum id venenatis. Eu tincidunt tortor aliquam nulla facilisi cras fermentum?
                            </div>
                            <div class="question-options">
                            	<div class="radioGroup block">
                                    <div class="radio-item">
                                        <input name="questionOptions3" id="questionOptions31" type="radio" class="radio-btn">
                                        <label for="questionOptions31" class="radio-label">Lorem Ipsum</label>
                                    </div>
                                    <div class="radio-item">
                                        <input name="questionOptions3" id="questionOptions32" type="radio" class="radio-btn">
                                        <label for="questionOptions32" class="radio-label">Arcu dictum varius</label>
                                    </div>
                                    <div class="radio-item">
                                        <input name="questionOptions3" id="questionOptions33" type="radio" class="radio-btn">
                                        <label for="questionOptions33" class="radio-label">Velit euismod</label>
                                    </div>
                                    <div class="radio-item">
                                        <input name="questionOptions3" id="questionOptions34" type="radio" class="radio-btn">
                                        <label for="questionOptions34" class="radio-label">Bibendum est ultricies</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="question-container">
                        	<div class="question">
                            	<span class="no">Q4.</span>
                                Sem integer vitae justo eget magna fermentum iaculis eu. Proin nibh nisl condimentum id venenatis. Eu tincidunt tortor aliquam nulla facilisi cras fermentum?
                            </div>
                            <div class="question-options">
                            	<div class="radioGroup block">
                                    <div class="radio-item">
                                        <input name="questionOptions4" id="questionOptions41" type="radio" class="radio-btn">
                                        <label for="questionOptions41" class="radio-label">Lorem Ipsum</label>
                                    </div>
                                    <div class="radio-item">
                                        <input name="questionOptions4" id="questionOptions42" type="radio" class="radio-btn">
                                        <label for="questionOptions42" class="radio-label">Arcu dictum varius</label>
                                    </div>
                                    <div class="radio-item">
                                        <input name="questionOptions4" id="questionOptions43" type="radio" class="radio-btn">
                                        <label for="questionOptions43" class="radio-label">Velit euismod</label>
                                    </div>
                                    <div class="radio-item">
                                        <input name="questionOptions4" id="questionOptions44" type="radio" class="radio-btn">
                                        <label for="questionOptions44" class="radio-label">Bibendum est ultricies</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="button-container text-center">
                        	<input type="submit" value="Submit" class="button lightpink tinysize mid">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@stop

