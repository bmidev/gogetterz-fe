@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.employee-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<h3 class="heading__h3">Billing history</h3>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="date-range margin-bot ">
                    <div class="form-container">
                        <div class="form-element">
                            <label for="txtFromDate" class="title">From </label>
                            <input name="txtFromDate" type="date" id="txtFromDate">
                        </div>
                        <div class="form-element">
                            <label for="txtToDate" class="title">To </label>
                            <input name="txtToDate" type="date" id="txtToDate">
                        </div>
                    </div>
                </div>
				<div class="content-white-box">                                        
                    
                	<div class="notification-grid">
                            <div class="title small-size row">   
                                <div class="col m1 text-center">
                                    <div class="bg-container">S.No</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">Date</div>
                                </div>
                                <div class="col m4">
                                    <div class="bg-container">Invoice Number</div>
                                </div>
                                <div class="col m3 text-center">
                                    <div class="bg-container">Amount (in $)</div>
                                </div>
                                <div class="col m2 text-center">
                                    
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">01</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">10/02/2017</div>
                                </div>
                                <div class="col m4">
                                    <div class="bg-container">PQ1289N</div>
                                </div>
                                <div class="col m3 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2">
                                    <a href="#" class="button grey-fill tinysize mid">Download</a>
                                </div>
                            </div>
                        	<div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">02</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">10/02/2017</div>
                                </div>
                                <div class="col m4">
                                    <div class="bg-container">PQ1289N</div>
                                </div>
                                <div class="col m3 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2">
                                    <a href="#" class="button grey-fill tinysize mid">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">03</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">10/02/2017</div>
                                </div>
                                <div class="col m4">
                                    <div class="bg-container">PQ1289N</div>
                                </div>
                                <div class="col m3 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2">
                                    <a href="#" class="button grey-fill tinysize mid">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">04</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">10/02/2017</div>
                                </div>
                                <div class="col m4">
                                    <div class="bg-container">PQ1289N</div>
                                </div>
                                <div class="col m3 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2">
                                    <a href="#" class="button grey-fill tinysize mid">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">05</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">10/02/2017</div>
                                </div>
                                <div class="col m4">
                                    <div class="bg-container">PQ1289N</div>
                                </div>
                                <div class="col m3 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2">
                                    <a href="#" class="button grey-fill tinysize mid">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">06</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">10/02/2017</div>
                                </div>
                                <div class="col m4">
                                    <div class="bg-container">PQ1289N</div>
                                </div>
                                <div class="col m3 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2">
                                    <a href="#" class="button grey-fill tinysize mid">Download</a>
                                </div>
                            </div>
                        	<div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">07</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">10/02/2017</div>
                                </div>
                                <div class="col m4">
                                    <div class="bg-container">PQ1289N</div>
                                </div>
                                <div class="col m3 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2">
                                    <a href="#" class="button grey-fill tinysize mid">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">08</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">10/02/2017</div>
                                </div>
                                <div class="col m4">
                                    <div class="bg-container">PQ1289N</div>
                                </div>
                                <div class="col m3 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2">
                                    <a href="#" class="button grey-fill tinysize mid">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">09</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">10/02/2017</div>
                                </div>
                                <div class="col m4">
                                    <div class="bg-container">PQ1289N</div>
                                </div>
                                <div class="col m3 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2">
                                    <a href="#" class="button grey-fill tinysize mid">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">10</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">10/02/2017</div>
                                </div>
                                <div class="col m4">
                                    <div class="bg-container">PQ1289N</div>
                                </div>
                                <div class="col m3 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2">
                                    <a href="#" class="button grey-fill tinysize mid">Download</a>
                                </div>
                            </div>
                    	</div>
                </div>
            </section>
            
        </div>
    </div>
</div>
@stop

