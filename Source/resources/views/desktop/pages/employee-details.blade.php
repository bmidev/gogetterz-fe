@extends('desktop.layouts.master')
@section('content')
<script src="{{ asset('desktop/js/chart.js') }}"></script>
<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.employee-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Employee database</h3>
                </div>
                <div class="right">
                	<section class="search-courses"><!--Css in _course.scss-->
                        <div class="form-container red">
                            <input type="text" placeholder="Search Employees/Department">
                            <input type="submit" title="Submit" value="">
                        </div>
                    </section>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="back-link">
                	<a href="#" class="">< BACK</a>                
                </div>
                <div class="about-employee">
                	<h4 class="heading__h4">Jane Dawson
                    	<a href="#" class="button lightpink tinysize tiny mLeft10">Edit Profile</a>
                    </h4>
                    <div class="designation">Senior Marketing Manager | ABC Corporation Limited</div>
                    <div class="email">
                        <a href="mailto:janedawson@abccorp.com">janedawson@abccorp.com</a> 
                    </div>                    
                </div>
                <div class="resTabs hours-spent-tabs">
                    <ul class="resp-tabs-list defaultTabs">
                        <li>Daily</li>
                        <li>Weekly</li>
                        <li>Monthly</li>
                    </ul>
                    <div class="resp-tabs-container defaultTabs">
                        <div>
                            <div class="chart-container">
                                <div class="chart-header">
                                    <h4 class="heading__h4">Hours Spent Per Day</h4>
                                    <div class="chart-select-course">
                                        <select class="select-multi-course" multiple="multiple">
                                            <option value="1">All Categories</option>
                                            <option value="2">Languages</option>
                                            <option value="3">Health &amp; Wellbeing</option>
                                            <option value="5">Technology &amp; Design</option>
                                            <option value="6">Option 6</option>
                                            <option value="7">Option 7</option>
                                            <option value="8">Option 8</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="chart-canvas">
                                    <canvas id="multi-area-chart"></canvas>
                                    <script>
										var ctx = document.getElementById("multi-area-chart").getContext("2d");
			
										const colors = {
										  red01: {
											fill: '#fbd3d3',
											stroke: '#fbd3d3',
										  },
										  red02: {
											fill: '#d53838',
											stroke: '#d53838',
										  },
										  red03: {
											fill: '#ff7373',
											stroke: '#ff7373',
										  },
										  red04: {
											fill: '#fc4c4c',
											stroke: '#fc4c4c',
										  },
										};
										
										const courseName01 = [0, 1, 0.2, 0.1, 0.5, 0.2, 2, 1.5];
										const courseName02 = [0, 0.7, 0.2, 0.3, 1, 0.3, 2, 1.5];
										const courseName03 = [0, 1, 2, 0.3, 1.5, 0.8, 2, 1.5];
										const unavailable = [0, 0.3, 0.2, 0.5, 1, 0.8, 2, 1.5];
										const xData = [10, 11, 12, 13, 14, 15, 16, 17];
										
										const myChart = new Chart(ctx, {
										  type: 'line',
										  data: {
											labels: xData,
											datasets: [{
											  label: "Unavailable",
											  fill: true,
											  backgroundColor: colors.red04.fill,
											  pointBackgroundColor: colors.red04.stroke,
											  borderColor: colors.red04.stroke,
											  pointHighlightStroke: colors.red04.stroke,
											  borderCapStyle: 'butt',
											  data: unavailable,
										
											}, {
											  label: "Course Name 03",
											  fill: true,
											  backgroundColor: colors.red03.fill,
											  pointBackgroundColor: colors.red03.stroke,
											  borderColor: colors.red03.stroke,
											  pointHighlightStroke: colors.red03.stroke,
											  borderCapStyle: 'butt',
											  data: courseName03,
											}, {
											  label: "Course Name 02",
											  fill: true,
											  backgroundColor: colors.red02.fill,
											  pointBackgroundColor: colors.red02.stroke,
											  borderColor: colors.red02.stroke,
											  pointHighlightStroke: colors.red02.stroke,
											  borderCapStyle: 'butt',
											  data: courseName02,
											}, {
											  label: "Course Name 01",
											  fill: true,
											  backgroundColor: colors.red01.fill,
											  pointBackgroundColor: colors.red01.stroke,
											  borderColor: colors.red01.stroke,
											  pointHighlightStroke: colors.red01.stroke,
											  data: courseName01,
											}]
										  },
										  options: {
											responsive: true,
											// Can't just just `stacked: true` like the docs say
											scales: {
											  xAxes: [{
												gridLines: {
													display: false
												},
											  }],
											  yAxes: [{
												stacked: true,
												gridLines: {
													display: false
												},
											  }]
											},
											animation: {
											  duration: 750,
											},
											legend: {
												display: true,
												position: 'bottom'
											},
											
										  }
										});
									</script> 
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="chart-container">
                                <div class="chart-header">
                                    <h4 class="heading__h4">Hours Spent Per Week</h4>
                                    <div class="chart-select-course">
                                        <select class="select-multi-course" multiple="multiple">
                                            <option value="1">All Categories</option>
                                            <option value="2">Languages</option>
                                            <option value="3">Health &amp; Wellbeing</option>
                                            <option value="5">Technology &amp; Design</option>
                                            <option value="6">Option 6</option>
                                            <option value="7">Option 7</option>
                                            <option value="8">Option 8</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="chart-canvas">
                                    <canvas id="multi-area-chart2"></canvas>
                                    <script>
										var ctx = document.getElementById("multi-area-chart2").getContext("2d");
			
										const colors2 = {
										  red01: {
											fill: '#fbd3d3',
											stroke: '#fbd3d3',
										  },
										  red02: {
											fill: '#d53838',
											stroke: '#d53838',
										  },
										  red03: {
											fill: '#ff7373',
											stroke: '#ff7373',
										  },
										  red04: {
											fill: '#fc4c4c',
											stroke: '#fc4c4c',
										  },
										};
										
										const courseName11 = [2.2, 1, 0.2, 0.1, 0.5, 0.2, 2, 1.1];
										const courseName12 = [0, 0.7, 0.2, 0.8, 1, 0.3, 2, 1.5];
										const courseName13 = [2, 1, 2, 0.3, 1.5, 0.8, 2, 0.8];
										const unavailable2 = [0, 0.3, 0.2, 0.5, 1, 0.8, 2, 0.5];
										const xData2 = [10, 11, 12, 13, 14, 15, 16, 17];
										
										const myChart2 = new Chart(ctx, {
										  type: 'line',
										  data: {
											labels: xData2,
											datasets: [{
											  label: "unavailable2",
											  fill: true,
											  backgroundColor: colors2.red04.fill,
											  pointBackgroundColor: colors2.red04.stroke,
											  borderColor: colors2.red04.stroke,
											  pointHighlightStroke: colors2.red04.stroke,
											  borderCapStyle: 'butt',
											  data: unavailable2,
										
											}, {
											  label: "Course Name 03",
											  fill: true,
											  backgroundColor: colors2.red03.fill,
											  pointBackgroundColor: colors2.red03.stroke,
											  borderColor: colors2.red03.stroke,
											  pointHighlightStroke: colors2.red03.stroke,
											  borderCapStyle: 'butt',
											  data: courseName13,
											}, {
											  label: "Course Name 02",
											  fill: true,
											  backgroundColor: colors2.red02.fill,
											  pointBackgroundColor: colors2.red02.stroke,
											  borderColor: colors2.red02.stroke,
											  pointHighlightStroke: colors2.red02.stroke,
											  borderCapStyle: 'butt',
											  data: courseName12,
											}, {
											  label: "Course Name 01",
											  fill: true,
											  backgroundColor: colors2.red01.fill,
											  pointBackgroundColor: colors2.red01.stroke,
											  borderColor: colors2.red01.stroke,
											  pointHighlightStroke: colors2.red01.stroke,
											  data: courseName11,
											}]
										  },
										  options: {
											responsive: true,
											// Can't just just `stacked: true` like the docs say
											scales: {
											  xAxes: [{
												gridLines: {
													display: false
												},
											  }],
											  yAxes: [{
												stacked: true,
												gridLines: {
													display: false
												},
											  }]
											},
											animation: {
											  duration: 750,
											},
											legend: {
												display: true,
												position: 'bottom'
											},
											
										  }
										});
									</script> 
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="chart-container">
                                <div class="chart-header">
                                    <h4 class="heading__h4">Hours Spent Per Month</h4>
                                    <div class="chart-select-course">
                                        <select class="select-multi-course" multiple="multiple">
                                            <option value="1">All Categories</option>
                                            <option value="2">Languages</option>
                                            <option value="3">Health &amp; Wellbeing</option>
                                            <option value="5">Technology &amp; Design</option>
                                            <option value="6">Option 6</option>
                                            <option value="7">Option 7</option>
                                            <option value="8">Option 8</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="chart-canvas">
                                    <canvas id="multi-area-chart3"></canvas>
                                    <script>
										var ctx = document.getElementById("multi-area-chart3").getContext("2d");
			
										const colors3 = {
										  red01: {
											fill: '#fbd3d3',
											stroke: '#fbd3d3',
										  },
										  red02: {
											fill: '#d53838',
											stroke: '#d53838',
										  },
										  red03: {
											fill: '#ff7373',
											stroke: '#ff7373',
										  },
										  red04: {
											fill: '#fc4c4c',
											stroke: '#fc4c4c',
										  },
										};
										
										const courseName21 = [1.4, 1, 0.2, 0.4, 0.9, 0.2, 1.3, 1.1];
										const courseName22 = [0, 0.7, 0.2, 0.8, 1, 0.3, 0.4, 1.5];
										const courseName23 = [1, 1, 2, 0.6, 1.2, 0.8, 0.2, 0.8];
										const unavailable3 = [0.6, 0.2, 0.2, 0.5, 1, 0.3, 1.4, 0.5];
										const xData3 = [10, 11, 12, 13, 14, 15, 16, 17];
										
										const myChart3 = new Chart(ctx, {
										  type: 'line',
										  data: {
											labels: xData3,
											datasets: [{
											  label: "unavailable3",
											  fill: true,
											  backgroundColor: colors3.red04.fill,
											  pointBackgroundColor: colors3.red04.stroke,
											  borderColor: colors3.red04.stroke,
											  pointHighlightStroke: colors3.red04.stroke,
											  borderCapStyle: 'butt',
											  data: unavailable3,
										
											}, {
											  label: "Course Name 03",
											  fill: true,
											  backgroundColor: colors3.red03.fill,
											  pointBackgroundColor: colors3.red03.stroke,
											  borderColor: colors3.red03.stroke,
											  pointHighlightStroke: colors3.red03.stroke,
											  borderCapStyle: 'butt',
											  data: courseName23,
											}, {
											  label: "Course Name 02",
											  fill: true,
											  backgroundColor: colors3.red02.fill,
											  pointBackgroundColor: colors3.red02.stroke,
											  borderColor: colors3.red02.stroke,
											  pointHighlightStroke: colors3.red02.stroke,
											  borderCapStyle: 'butt',
											  data: courseName22,
											}, {
											  label: "Course Name 01",
											  fill: true,
											  backgroundColor: colors3.red01.fill,
											  pointBackgroundColor: colors3.red01.stroke,
											  borderColor: colors3.red01.stroke,
											  pointHighlightStroke: colors3.red01.stroke,
											  data: courseName21,
											}]
										  },
										  options: {
											responsive: true,
											// Can't just just `stacked: true` like the docs say
											scales: {
											  xAxes: [{
												gridLines: {
													display: false
												},
											  }],
											  yAxes: [{
												stacked: true,
												gridLines: {
													display: false
												},
											  }]
											},
											animation: {
											  duration: 750,
											},
											legend: {
												display: true,
												position: 'bottom'
											},
											
										  }
										});
									</script> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="select-year">
                    	<select class="custom-select white tiny-padding small-size">
                    	    <option>Year 2018</option>
                    	</select>
                    </div>
                </div>
				<div class="content-white-box margin-bot">
                    <div class="clear">
                        <div class="left70">
                            <h4 class="heading__h4">Total Hours Spent	:	20</h4>
                            <div class="notification-grid">
                                <div class="title small-size row">                        	
                                    <div class="col m2 text-center">
                                        <div class="bg-container">Sr No</div>
                                    </div>                            
                                    <div class="col m7">
                                        <div class="bg-container">Name of the Course</div>
                                    </div>
                                    <div class="col m3 text-center">
                                        <div class="bg-container">Hours Spent</div>
                                    </div>
                                </div>
                                <div class="data small-size row">                        	
                                    <div class="col m2 text-center">
                                        <div class="bg-container small-padding">01</div>
                                    </div>
                                    <div class="col m7">
                                        <div class="bg-container">
                                            Duis aute irure dolor in reprehenderit in voluptate
                                        </div>
                                    </div>
                                    <div class="col m3 text-center">
                                        <div class="bg-container">
                                            05
                                        </div>
                                    </div>
                                </div>
                                <div class="data small-size row">                        	
                                    <div class="col m2 text-center">
                                        <div class="bg-container small-padding">02</div>
                                    </div>
                                    <div class="col m7">
                                        <div class="bg-container">
                                            Duis aute irure dolor in reprehenderit in voluptate
                                        </div>
                                    </div>
                                    <div class="col m3 text-center">
                                        <div class="bg-container">
                                            05
                                        </div>
                                    </div>
                                </div>
                                <div class="data small-size row">                        	
                                    <div class="col m2 text-center">
                                        <div class="bg-container small-padding">03</div>
                                    </div>
                                    <div class="col m7">
                                        <div class="bg-container">
                                            Duis aute irure dolor in reprehenderit in voluptate
                                        </div>
                                    </div>
                                    <div class="col m3 text-center">
                                        <div class="bg-container">
                                            05
                                        </div>
                                    </div>
                                </div>
                                <div class="data small-size row">                        	
                                    <div class="col m2 text-center">
                                        <div class="bg-container small-padding">04</div>
                                    </div>
                                    <div class="col m7">
                                        <div class="bg-container">
                                            Duis aute irure dolor in reprehenderit in voluptate
                                        </div>
                                    </div>
                                    <div class="col m3 text-center">
                                        <div class="bg-container">
                                            05
                                        </div>
                                    </div>
                                </div>
                                <div class="data small-size row">                        	
                                    <div class="col m2 text-center">
                                        <div class="bg-container small-padding">05</div>
                                    </div>
                                    <div class="col m7">
                                        <div class="bg-container">
                                            Duis aute irure dolor in reprehenderit in voluptate
                                        </div>
                                    </div>
                                    <div class="col m3 text-center">
                                        <div class="bg-container">
                                            05
                                        </div>
                                    </div>
                                </div>
                            </div>   
                        </div>   
                        <div class="right25">
                        	<div class="hours-spent-form">
                                <select class="select-multi-course" multiple="multiple">
                                    <option value="1">All Categories</option>
                                    <option value="2">Languages</option>
                                    <option value="3">Health &amp; Wellbeing</option>
                                    <option value="5">Technology &amp; Design</option>
                                    <option value="6">Option 6</option>
                                    <option value="7">Option 7</option>
                                    <option value="8">Option 8</option>
                                </select>
                            </div>
                            <div class="date-range margin-bot ">
                                <div class="form-container">
                                    <div class="form-element full-width">
                                        <label for="txtFromDate" class="title">From </label>
                                        <input name="txtFromDate" type="date" id="txtFromDate">
                                    </div>
                                    <div class="form-element full-width">
                                        <label for="txtToDate" class="title">To </label>
                                        <input name="txtToDate" type="date" id="txtToDate">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                                  
                </div>
                <div class="content-white-box">
                	<h4 class="heading__h4">Active Courses</h4>
                    <div class="active-courses">
                        <div class="course-container">
                            <article class="course bg-light-pink">
                                <div class="status-calculator">
                                    <div class="status-bar">
                                        <span class="status-bar__value" style="width:50%"></span>                                        
                                    </div>
                                    <div class="status-percentage">50</div>
                                </div>
                                <div class="article-info">
                                    <h4><a href="#">Lorem ipsum dolor</a></h4>
                                    <div class="footer-container">
                                        <a href="#" class="button lightpink tinysize small">Resume Course</a>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="course-container">
                            <article class="course bg-light-pink">
                                <div class="status-calculator">
                                    <div class="status-bar">
                                        <span class="status-bar__value" style="width:50%"></span>                                        
                                    </div>
                                    <div class="status-percentage">50</div>
                                </div>
                                <div class="article-info">
                                    <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                    <div class="footer-container">
                                        <a href="#" class="button lightpink tinysize small">Resume Course</a>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="course-container">
                            <article class="course bg-light-pink">
                                <div class="status-calculator">
                                    <div class="status-bar">
                                        <span class="status-bar__value" style="width:50%"></span>                                        
                                    </div>
                                    <div class="status-percentage">50</div>
                                </div>
                                <div class="article-info">
                                    <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                    <div class="footer-container">
                                        <a href="#" class="button lightpink tinysize small">Resume Course</a>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="course-container">
                            <article class="course bg-light-pink">
                                <div class="status-calculator">
                                    <div class="status-bar">
                                        <span class="status-bar__value" style="width:50%"></span>                                        
                                    </div>
                                    <div class="status-percentage">50</div>
                                </div>
                                <div class="article-info">
                                    <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit adipiscing elit</a></h4>
                                    <div class="footer-container">
                                        <a href="#" class="button lightpink tinysize small">Resume Course</a>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="course-container">
                            <article class="course bg-light-pink">
                                <div class="status-calculator">
                                    <div class="status-bar">
                                        <span class="status-bar__value" style="width:50%"></span>                                        
                                    </div>
                                    <div class="status-percentage">50</div>
                                </div>
                                <div class="article-info">
                                    <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                    <div class="footer-container">
                                        <a href="#" class="button lightpink tinysize small">Resume Course</a>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="course-container">
                            <article class="course bg-light-pink">
                                <div class="status-calculator">
                                    <div class="status-bar">
                                        <span class="status-bar__value" style="width:50%"></span>                                        
                                    </div>
                                    <div class="status-percentage">50</div>
                                </div>
                                <div class="article-info">
                                    <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                    <div class="footer-container">
                                        <a href="#" class="button lightpink tinysize small">Resume Course</a>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </section>
            
        </div>
    </div>
</div>
<!--Modal code-->
<div class="hide">
	@include('desktop.modals.add-employee')
    @include('desktop.modals.add-department')
</div>
@stop

