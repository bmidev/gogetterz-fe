@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Certifications</h3>
                </div>
                <div class="right">
                	<a href="{{ BASE_URL }}member-create-certificate" class="button tinysize small caps certificate-btn">
                    	<span class="icon-trophy"></span> Create new certificate
                    </a>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
				<div class="content-white-box">
                	<div class="certificate-grid">
                    	<article>
                        	<h5>Certificate of completion</h5>
                            <figure class="certificate-img">
                            	<img src="{{ asset('desktop/images/img-certificate01.svg') }}" alt=""/>
                            </figure>
                            <div class="course-name">Name of the Course</div>
                            <a href="#" class="button tinysize small lightpink">Download PDF</a>
                        </article>
                        <article>
                        	<h5>Certificate of completion</h5>
                            <figure class="certificate-img">
                            	<img src="{{ asset('desktop/images/img-certificate02.svg') }}" alt=""/>
                            </figure>
                            <div class="course-name">Name of the Course</div>
                            <a href="#" class="button tinysize small lightpink">Download PDF</a>
                        </article>
                        <article>
                        	<h5>Certificate of completion</h5>
                            <figure class="certificate-img">
                            	<img src="{{ asset('desktop/images/img-certificate03.svg') }}" alt=""/>
                            </figure>
                            <div class="course-name">Name of the Course</div>
                            <a href="#" class="button tinysize small lightpink">Download PDF</a>
                        </article>
                        <article>
                        	<h5>Certificate of completion</h5>
                            <figure class="certificate-img">
                            	<img src="{{ asset('desktop/images/img-certificate01.svg') }}" alt=""/>
                            </figure>
                            <div class="course-name">Name of the Course</div>
                            <a href="#" class="button tinysize small lightpink">Download PDF</a>
                        </article>
                        <article>
                        	<h5>Certificate of completion</h5>
                            <figure class="certificate-img">
                            	<img src="{{ asset('desktop/images/img-certificate01.svg') }}" alt=""/>
                            </figure>
                            <div class="course-name">Name of the Course</div>
                            <a href="#" class="button tinysize small lightpink">Download PDF</a>
                        </article>
                        <article>
                        	<h5>Certificate of completion</h5>
                            <figure class="certificate-img">
                            	<img src="{{ asset('desktop/images/img-certificate02.svg') }}" alt=""/>
                            </figure>
                            <div class="course-name">Name of the Course</div>
                            <a href="#" class="button tinysize small lightpink">Download PDF</a>
                        </article>
                        <article>
                        	<h5>Certificate of completion</h5>
                            <figure class="certificate-img">
                            	<img src="{{ asset('desktop/images/img-certificate03.svg') }}" alt=""/>
                            </figure>
                            <div class="course-name">Name of the Course</div>
                            <a href="#" class="button tinysize small lightpink">Download PDF</a>
                        </article>
                        <article>
                        	<h5>Certificate of completion</h5>
                            <figure class="certificate-img">
                            	<img src="{{ asset('desktop/images/img-certificate01.svg') }}" alt=""/>
                            </figure>
                            <div class="course-name">Name of the Course</div>
                            <a href="#" class="button tinysize small lightpink">Download PDF</a>
                        </article>
                    </div>
                </div>                
            </section>
            
        </div>
    </div>
</div>
@stop

