@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<h3 class="heading__h3">Categories &amp; Interests</h3>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="content-white-box select-categories">
                	<div class="copy-container">
                        <h3 class="heading__h3">Your Interests</h3>
                        <p>Selected categories help us recommend the best courses for you</p>
                    </div>
                    <div class="process-container">                        
                        <div class="form-container">
                            <form>
                                <div class="checkbox">
                                    <input id="chk1" type="checkbox" name="" class="css-checkbox" >
                                    <label for="chk1" class="css-label">Creative &amp; Design</label>
                                </div>
                                <div class="checkbox">
                                    <input id="chk2" type="checkbox" name="" class="css-checkbox" >
                                    <label for="chk2" class="css-label">Music</label>
                                </div>
                                <div class="checkbox">
                                    <input id="chk3" type="checkbox" name="" class="css-checkbox" >
                                    <label for="chk3" class="css-label">English &amp; Languages</label>
                                </div>
                                <div class="checkbox">
                                    <input id="chk4" type="checkbox" name="" class="css-checkbox" >
                                    <label for="chk4" class="css-label">IT Technology</label>
                                </div>
                                <div class="checkbox">
                                    <input id="chk5" type="checkbox" name="" class="css-checkbox" >
                                    <label for="chk5" class="css-label">Crafts &amp; Hobbies</label>
                                </div>
                                <div class="checkbox">
                                    <input id="chk6" type="checkbox" name="" class="css-checkbox" >
                                    <label for="chk6" class="css-label">Cooking</label>
                                </div>
                                <div class="checkbox">
                                    <input id="chk7" type="checkbox" name="" class="css-checkbox" >
                                    <label for="chk7" class="css-label">Sports &amp; Beauty</label>
                                </div>
                                <div class="checkbox">
                                    <input id="chk8" type="checkbox" name="" class="css-checkbox" >
                                    <label for="chk8" class="css-label">Business</label>
                                </div>
                                <div class="form-element text-right">
                                    <input name="btnNext" type="submit" id="btnNext" value="Save Changes" class="button lightpink smallsize mid">
                                </div>                    
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            
        </div>
    </div>
</div>
@stop

