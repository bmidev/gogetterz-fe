@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Messages</h3>
                </div>
                <div class="right">
                	<section class="search-courses"><!--Css in _course.scss-->
                        <div class="form-container red">
                            <input type="text" placeholder="Search Messages">
                            <input type="submit" title="Submit" value="">
                        </div>
                    </section>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="member-breadcrumb">
               		<a href="{{ BASE_URL }}member-messages">Messages</a> 
                    <span class="divider"> > </span> 
                    Compose Message
                </div>
				<div class="content-white-box">
                	<h4 class="heading__h4">Compose Message</h4>
                	<div class="form-container no-padding theme-grey">
                    	<div class="form-element">
                        	<label for="txtTo" class="title">To</label>
                            <input name="txtTo" type="text" id="txtTo" class="width30">
                            <div class="file-upload attached-img right">
                                <input type="file" name="receipt" id="file-browse" class="inputfile" />
                                <label for="file-browse"><span class="input-text"></span> <strong>&nbsp;</strong></label>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtSubject" class="title">Subject</label>
                            <input name="txtSubject" type="text" id="txtSubject" class="width80">
                        </div>
                    	<div class="form-element">
                        	<label for="txtMessage" class="title">Message</label>
                            <textarea name="txtMessage" rows="5" id="txtMessage" class="width80"></textarea>
                        </div>
                        <div class="form-element button-container">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="right-container">
                                <input type="submit" value="Send" class="button grey-fill tinysize mid">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@stop

