@extends('desktop.layouts.master')
@section('content')
    <div class="client-bg">
        <section class="top-banner">
            <div class="m-page-container">
                <div class="top-banner-image">
                    <img src="{{ asset('desktop/images/expert/expert-banner.jpg') }}" alt=""/>
                </div>
                <div class="top-banner-copy">
                    <h1 class="heading__h1">Our Vision</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <div class="button-container">
                        <a href="#" class="button black large small">Get Started</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="teach-with boxes-grid-4">
            <div class="m-page-container">
                <h2 class="heading__h2">Why Teach With GoGetterz</h2>
                <div class="box-container">
                    <div class="teach-with-boxes">
                        <div class="teach-with-box">
                            <div class="teach-with-box-image" style="background-image:url({{ asset('desktop/images/expert/free-registration.svg') }})"></div>
                            <div class="teach-with-box-copy">
                                <h5 class="heading__h5">Free registration</h5>
                                <p>There is no cost for registration, credit system, member progress, monitoring emails and accounting for anyone. This enables experts who wish to create free courses without paying any fee.</p>
                            </div>
                        </div>
                        <div class="teach-with-box">
                            <div class="teach-with-box-image" style="background-image:url({{ asset('desktop/images/expert/course-creation.svg') }})"></div>
                            <div class="teach-with-box-copy">
                                <h5 class="heading__h5">Ease of course creation</h5>
                                <p>It is very easy to create and manage online courses. All it takes is just a click to upload your documents and videos. Even individuals who are not familiar with computers can easily create a course.</p>
                            </div>
                        </div>
                    </div>
                    <div class="teach-with-boxes">
                        <div class="teach-with-box">
                            <div class="teach-with-box-image" style="background-image:url({{ asset('desktop/images/expert/customer-support.svg') }})"></div>
                            <div class="teach-with-box-copy">
                                <h5 class="heading__h5">Customer care support</h5>
                                <p>GoGetterz provides one to one support for all your queries. If you are an individual or a corporation all provide you all the assistance from online course creation to uploading, marketing, etc.</p>
                            </div>
                        </div>
                        <div class="teach-with-box">
                            <div class="teach-with-box-image" style="background-image:url({{ asset('desktop/images/expert/competitive-rates.svg') }})"></div>
                            <div class="teach-with-box-copy">
                                <h5 class="heading__h5">Competitive commission rates</h5>
                                <p>With no fee from course registration to publishing, experts gain high rates of compensation thus ensuring you make more money with your knowledge.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-container ">
                <div class="teach-with-slider center">
                    <div class="slider-image">
                        <img src="{{ asset('desktop/images/expert/teach-slider.png') }}" alt=""/>
                    </div>
                    <div class="slider-copy">
                        <h2 class="heading__h2">Who Can Teach Here</h2>
                        <h5 class="heading__h5">Health & Beauty Professionals</h5>
                        <p>Who wants to hold their own class but doesn't have to know how for setting up a classroom and attracting students.</p>
                        <p>Since it's online, the cost for setting up a classroom and getting students is 0. There is no registration or publication fee, so there is no risk in starting.</p>
                    </div>
                </div>
                <div class="teach-with-slider clear">
                    <div class="slider-image">
                        <img src="{{ asset('desktop/images/expert/teach-slider.png') }}" alt=""/>
                    </div>
                    <div class="slider-copy">
                        <h2 class="heading__h2">Who Can Teach Here</h2>
                        <h5 class="heading__h5">Health & Beauty Professionals</h5>
                        <p>Who wants to hold their own class but doesn't have to know how for setting up a classroom and attracting students.</p>
                        <p>Since it's online, the cost for setting up a classroom and getting students is 0. There is no registration or publication fee, so there is no risk in starting.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="expert-how-it-works">
            <div class="m-page-container">
                <div class="info-copy">
                    <h2 class="heading__h2">How it works</h2>
                    <p class="sub-copy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                    <div class="steps-video-container clear">
                        <div class="steps">
                            <ol>
                                <li>
                                    <h5 class="heading__h5">Lorem Ipsum</h5>
                                    <p>Vitae elementum curabitur vitae nunc sed. Elit ullamcorper dignissim cras tincidunt. Libero justo laoreet sit amet cursus sit. Et netus et malesuada fames ac turpis egestas integer.</p>
                                </li>
                                <li>
                                    <h5 class="heading__h5">Lorem Ipsum</h5>
                                    <p>Vitae elementum curabitur vitae nunc sed. Elit ullamcorper dignissim cras tincidunt. Libero justo laoreet sit amet cursus sit. Et netus et malesuada fames ac turpis egestas integer.</p>
                                </li>
                                <li>
                                    <h5 class="heading__h5">Lorem Ipsum</h5>
                                    <p>Vitae elementum curabitur vitae nunc sed. Elit ullamcorper dignissim cras tincidunt. Libero justo laoreet sit amet cursus sit. Et netus et malesuada fames ac turpis egestas integer.</p>
                                </li>
                            </ol>
                        </div>
                        <div class="expert-video-container">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/WDgy1p6dC2Q?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="know-more-container">
                    <div class="know-more clear">
                        <h5 class="heading__h5">To know more about the expert commission scheme</h5>
                        <div class="button-container">
                            <a href="#" class="button white midsize small">Click Here</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="expert-testimonial">
            <div class="m-page-container">
                <h1 class="heading__h1">EXPERT TESTIMONIALS</h1>
                <div class="testimonial-container">
                    <div class="testimonials">
                        <article class="course">
                            <figure>
                                <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/expert/testimonial-img-1.jpg') }})"></span></a>
                            </figure>
                            <div class="article-info article-info-black">
                                <h4><a href="#">Expert Name</a></h4>
                                <div class="author"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </a></div>
                            </div>
                        </article>
                        <article class="course">
                            <figure>
                                <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/expert/testimonial-img-2.jpg') }})"></span></a>
                            </figure>
                            <div class="article-info article-info-black">
                                <h4><a href="#">Expert Name</a></h4>
                                <div class="author"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </a></div>
                            </div>
                        </article>
                        <article class="course">
                            <figure>
                                <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/expert/testimonial-img-3.jpg') }})"></span></a>
                            </figure>
                            <div class="article-info article-info-black">
                                <h4><a href="#">Expert Name</a></h4>
                                <div class="author"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </a></div>
                            </div>
                        </article>
                    </div>
                    <div class="testimonial-view-all">
                        <a href="#">View all</a>
                    </div>
                </div>
                <div class="button-container">
                    <a href="#" class="button black midsize small">Get Started</a>
                </div>
            </div>
        </section>
    </div>
@stop