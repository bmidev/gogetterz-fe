@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Online course management</h3>
                </div>
                <div class="right">
                	<a href="#" class="button tinysize small caps certificate-btn">
                    	<span class="icon-delete white small"></span> Delete course
                    </a>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<h3 class="heading__h3 bg-grey">
                	<div class="steps-heading">
                    	<span>Step 01</span>
                        <span>Step 02</span>
                        <span class="active">Step 03</span>
                        <span>Step 04</span>
                    </div>
                </h3>
            	<div class="content-white-box no-top-border">
                	<div class="back-link">
                   		<a href="#">< Go Back</a> 
                    </div>
                	<h4 class="heading__h4">Tuition and Other Settings</h4>
                    <div class="form-container">
                    	<div class="form-element">
                        	<label for="lstSalesDeadline" class="title">Sales Deadline</label>
                            <select name="" class="custom-select width30" id="lstSalesDeadline" >
                                <option>Unspecified</option>
                            </select>
                        </div>
                        <div class="form-element">
                        	<div class="left-container">&nbsp;</div>
                            <div class="right-container">
                            	<div class="date-range pink">
                                    <div class="form-container">
                                        <div class="form-element">
                                            <label for="txtFromDate" class="title">From </label>
                                            <select class="custom-select paddingBig">
                                                <option>11:00 AM</option>
                                            </select>
                                        </div>
                                        <div class="form-element">
                                            <label for="txtToDate" class="title">To </label>
                                            <select class="custom-select paddingBig">
                                                <option>03:00 PM</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtCost" class="title">Cost</label>
                            <div class="right-container">
                                <input name="" type="text" class="width30" id="txtCost" placeholder="Enter Amount">
                                <select name="" class="custom-select width20 margin-left" id="">
                                    <option>$ US Dollar</option>
                                </select>
                                <span class="note full-width clear">Notet : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </span>
                        	</div>
                        </div>  
                    </div>
                    <h4 class="heading__h4 bg-grey no-margin">Add Coupon</h4>
                    <div class="content-grey-box small-padding no-top-border">
                    	<div class="form-container theme-white">
                        	<div class="form-element">
                                <label for="txtTitle" class="title">Title</label>
                                <input name="" type="text" class="width40" id="txtTitle" placeholder="">
                            </div>
                            <div class="form-element">
                                <label for="txtCouponCode" class="title">Coupon Code</label>
                                <input name="" type="text" class="width40" id="txtCouponCode" placeholder="">
                            </div>
                            <div class="form-element">
                                <label for="txtDiscount" class="title">Discount</label>
                                <input name="" type="text" class="width40" id="txtDiscount" placeholder="">
                            </div>
                            <div class="form-element">
                                <label for="txtExpiryDate" class="title">Expiry Date</label>
                                <input name="" type="date" class="width30" id="txtExpiryDate" placeholder="">
                            </div>
                            <div class="form-element">
                                <label for="txtDescription" class="title">Description</label>
                                <div class="right-container">
                                    <textarea name="" rows="4" id="txtDescription" class="full-width"></textarea>
                                    <span class="note full-width clear">Note : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-container">
                        <div class="form-element button-container">
                            <div class="left-container">&nbsp;</div>
                            <div class="right-container">
                                <input type="submit" value="Continue to Step 3" class="button lightpink tinysize small">
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>            
        </div>
    </div>
</div>
@stop

