@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<h3 class="heading__h3">Event management</h3>
            </div>
            <section class="content-grey-box no-top-border">
				<div class="content-white-box margin-bot">
                	<h4 class="heading__h4">Create an event</h4>
                    <div class="form-container">
                    	<div class="form-element">
                        	<label for="txtTitle" class="title">Title</label>
                            <input type="text" id="txtTitle" placeholder="Title of the event comes here">
                        </div>
                        <div class="form-element">
                        	<label for="txtSubtitle" class="title">Subtitle</label>
                            <input type="text" id="txtSubtitle" placeholder="Subtitle of the event comes here">
                        </div>
                        <div class="form-element">
                        	<label for="txtEventDate" class="title">Event Date</label>
                            <input type="date" id="txtEventDate" class="width30">
                        </div>
                        <div class="form-element">
                        	<label for="" class="title">Event Time</label>
                            <div class="right-container">
                            	<div class="date-range pink">
                                    <div class="form-container">
                                        <div class="form-element">
                                            <label for="txtFromDate" class="title">From </label>
                                            <select class="custom-select paddingBig">
                                                <option>11:00 AM</option>
                                            </select>
                                        </div>
                                        <div class="form-element">
                                            <label for="txtToDate" class="title">To </label>
                                            <select class="custom-select paddingBig">
                                                <option>03:00 PM</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtPromoVideos" class="title">Event Thumbnail</label>
                            <div class="file-upload upload-img">
                                <input type="file" name="receipt" id="file-browse" class="inputfile" />
                                <label for="file-browse"><span class="input-text">&nbsp;</span> <strong>+ Upload Image</strong></label>
                            </div>
                            <div class="left note">Course Image Format : Supported format for uploading the file : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtProfileOverview" class="title">Profile Overview</label>
                            <textarea id="txtProfileOverview" rows="8" >Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tortor posuere ac ut consequat. Nulla aliquet porttitor lacus luctus accumsan tortor. Est sit amet facilisis magna etiam tempor orci eu. Maecenas volutpat blandit aliquam etiam erat velit scelerisque in. Purus faucibus ornare suspendisse sed nisi lacus sed viverra tellus. Elementum facilisis leo vel fringilla est. Eget gravida cum sociis natoque penatibus et magnis dis. Venenatis lectus magna fringilla urna porttitor. Vitae tempus quam pellentesque nec nam.</textarea>
                        </div>
                        <div class="form-element">
                        	<label for="rdoCertificateContent" class="title">Certificate Content<br>
								<span>(Choose Any One)</span>
                            </label>
                            <div class="radioGroup block">
                            	<div class="radio-item">
                                	<input name="rdoCertificateContent" id="rdoCertificateContent1" type="radio" class="radio-btn">
                                    <label for="rdoCertificateContent1" class="radio-label">Suggested Venue :</label>
                                    <div class="event-location">
                                    	<div class="address">
                                        	<strong>GG Studio :</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                                        </div>
                                        <div class="map">
                                        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3768.7823554215324!2d72.82968831533647!3d19.16100198703927!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b65b5f15d3a3%3A0x2efe6695a52b1948!2sG+S+Studio!5e0!3m2!1sen!2sin!4v1531480168946" width="300" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-item">
                                	<input name="rdoCertificateContent" id="rdoCertificateContent2" type="radio" class="radio-btn">
                                    <label for="rdoCertificateContent2" class="radio-label">Enter Venue Address :</label>
                                    <div class="add-location">
                                    	<div class="address">
                                        	<textarea id="txtEnterVenueAddress" rows="4" >Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tortor posuere ac ut consequat. </textarea>
                                        </div>
                                        <div class="seating-capacity">
                                        	<div class="form-element">
                                                <label for="" class="title">Maximum Seating Capacity</label>
                                                <select class="custom-select">
                                                    <option>12</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtRegistrationDeadline" class="title">Registration Deadline</label>
                            <input type="date" id="txtRegistrationDeadline" class="width30">
                        </div>
                        <div class="form-element button-container">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="right-container">
                                <input type="submit" value="Add Event" class="button lightpink tinysize mid">
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="tab-data">
                    <div class="resTabs">
                        <ul class="resp-tabs-list defaultTabs">
                            <li>Upcoming events</li>
                            <li>Event history</li>
                        </ul>
                        <div class="resp-tabs-container defaultTabs">
                            <div> 
                            	<article class="course bg-light-pink">
                                    <div class="event-date">
                                        10 January’18
                                    </div>
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <div class="event-date">
                                        10 January’18
                                    </div>
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <div class="event-date">
                                        10 January’18
                                    </div>
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <div class="event-date">
                                        10 January’18
                                    </div>
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <div class="event-date">
                                        10 January’18
                                    </div>
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <div class="event-date">
                                        10 January’18
                                    </div>
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <div class="event-date">
                                        10 January’18
                                    </div>
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <div class="event-date">
                                        10 January’18
                                    </div>
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                    </div>
                                </article>
                            </div>
                            <div>
                            	<article class="course bg-light-pink">
                                    <div class="event-date">
                                        10 January’18
                                    </div>
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <div class="event-date">
                                        10 January’18
                                    </div>
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <div class="event-date">
                                        10 January’18
                                    </div>
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                    </div>
                                </article>
                                <article class="course bg-light-pink">
                                    <div class="event-date">
                                        10 January’18
                                    </div>
                                    <figure>
                                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                                    </figure>
                                    <div class="article-info">
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                                    </div>
                                </article>
                            </div>                        
                        </div>
                    </div>
                </div>               
            </section>
            
        </div>
    </div>
</div>
@stop

