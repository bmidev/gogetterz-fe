@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.employee-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<h3 class="heading__h3">Employee database</h3>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="course-top-buttons">
                	<a href="#add-employee-data" class="button grey-fill no-hover tinysize small modal-link">+ Add Employees</a>   
                    <a href="#add-department" class="button grey-fill no-hover tinysize small mLeft10 modal-link">+ Add department</a>                 
                </div>
				<div class="content-white-box">
                	<div class="inner-heading">
                    	<div class="left">
                        	<select class="custom-select full-width small-padding grey">
                        	    <option>MARKETING DEPARTMENT</option>
                        	</select>
                        </div>
                        <div class="right">
                        	<a href="#" class="button lightpink tinysize small">FILTER</a>
                        </div>
                    </div>
                	<div class="notification-grid">
                    	<div class="title small-size row">
                        	<div class="col m1 delete">
                            	<a href="#" title="Delete" class="icon-delete"></a>
                            </div>
                            <div class="col m1 text-center">
                                <div class="bg-container">Sr No</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">ID Number</div>
                            </div>
                            <div class="col m3">
                                <div class="bg-container">Name</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Department</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">Level/Position</div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk1" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk1" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m1 text-center">
                                <div class="bg-container small-padding">01 <span class="moderator">M</span></div>
                            </div>
                            <div class="col m2">
                                <div class="bg-container">
                                	<a href="{{ BASE_URL }}employee-details">PMQR12</a> 
                                </div>
                            </div>
                            <div class="col m3">
                                <div class="bg-container white">
                                    <a href="{{ BASE_URL }}employee-details">Arighna Sarkar</a>
                                </div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Marketing</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">Senior</div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk1" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk1" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m1 text-center">
                                <div class="bg-container small-padding">01 <span class="moderator">M</span></div>
                            </div>
                            <div class="col m2">
                                <div class="bg-container">
                                	<a href="{{ BASE_URL }}employee-details">PMQR12</a> 
                                </div>
                            </div>
                            <div class="col m3">
                                <div class="bg-container white">
                                    <a href="{{ BASE_URL }}employee-details">Arighna Sarkar</a>
                                </div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Marketing</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">Senior</div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk1" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk1" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m1 text-center">
                                <div class="bg-container small-padding">01 <span class="moderator">M</span></div>
                            </div>
                            <div class="col m2">
                                <div class="bg-container">
                                	<a href="{{ BASE_URL }}employee-details">PMQR12</a> 
                                </div>
                            </div>
                            <div class="col m3">
                                <div class="bg-container white">
                                    <a href="{{ BASE_URL }}employee-details">Arighna Sarkar</a>
                                </div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Marketing</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">Senior</div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk1" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk1" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m1 text-center">
                                <div class="bg-container small-padding">01 <span class="moderator">M</span></div>
                            </div>
                            <div class="col m2">
                                <div class="bg-container">
                                	<a href="{{ BASE_URL }}employee-details">PMQR12</a> 
                                </div>
                            </div>
                            <div class="col m3">
                                <div class="bg-container white">
                                    <a href="{{ BASE_URL }}employee-details">Arighna Sarkar</a>
                                </div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Marketing</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">Senior</div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk1" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk1" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m1 text-center">
                                <div class="bg-container small-padding">01 <span class="moderator">M</span></div>
                            </div>
                            <div class="col m2">
                                <div class="bg-container">
                                	<a href="{{ BASE_URL }}employee-details">PMQR12</a> 
                                </div>
                            </div>
                            <div class="col m3">
                                <div class="bg-container white">
                                    <a href="{{ BASE_URL }}employee-details">Arighna Sarkar</a>
                                </div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Marketing</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">Senior</div>
                            </div>
                        </div>
                    </div>
                    <div class="note add-padding">
                    	<span class="moderator">M</span> Department Moderator
                    </div>                    
                </div>
            </section>
            
        </div>
    </div>
</div>
<!--Modal code-->
<div class="hide">
	@include('desktop.modals.add-employee')
    @include('desktop.modals.add-department')
</div>
@stop

