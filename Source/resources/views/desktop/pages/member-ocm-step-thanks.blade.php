@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Online course management</h3>
                </div>
                <div class="right">
                	<a href="#" class="button tinysize small caps certificate-btn">
                    	<span class="icon-delete white small"></span> Delete course
                    </a>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<h3 class="heading__h3 bg-grey">
                	<div class="steps-heading">
                    	<span>Step 01</span>
                        <span>Step 02</span>
                        <span>Step 03</span>
                        <span class="active">Step 04</span>
                    </div>
                </h3>
            	<div class="content-white-box no-top-border">
                	<div class="tranks-certificate">
                        <h3 class="heading__h3">Thanks for uploading your course!</h3>
                        <p>Your course is under review and will be notified within 24-72 hours.</p>
                        <h3 class="heading__h3">Course ID : XXXXXXX</h3>
                        <div class="button-container">
                            <a href="#" class="button tinysize small lightpink">Go to Main Page</a>
                        </div>
                    </div>
                </div>
                
            </section>            
        </div>
    </div>
</div>
@stop

