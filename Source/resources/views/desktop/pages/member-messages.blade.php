@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Messages</h3>
                </div>
                <div class="right">
                	<section class="search-courses"><!--Css in _course.scss-->
                        <div class="form-container red">
                            <input type="text" placeholder="Search Messages">
                            <input type="submit" title="Submit" value="">
                        </div>
                    </section>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="notification-sound">
               		<a href="#"><span class="icon-sound-mute"></span>Mute Notifications</a> 
                </div>
				<div class="content-white-box">
                	<div class="">
                    	<a href="{{ BASE_URL }}member-compose-message" class="button lightpink tinysize small caps">Compose Message</a>
                    </div>
                	<div class="resTabs message-tabs">
                        <ul class="resp-tabs-list defaultTabs">
                            <li>Sent</li>
                            <li>Received</li>
                        </ul>
                        <div class="resp-tabs-container defaultTabs">
                            <div>
                            	<div class="notification-grid">
                    	<div class="title row">
                        	<div class="col m1 delete">
                            	<a href="#" title="Delete" class="icon-delete"></a>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">Date</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Sender</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">Subject</div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk1" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk1" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Ben French</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                    <div class="right">10 : 15 PM</div>
                                </div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk2" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk2" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Ben French</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                    <div class="right">10 : 15 PM</div>
                                </div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk3" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk3" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Ben French</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                    <div class="right">10 : 15 PM</div>
                                </div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk4" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk4" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Ben French</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                    <div class="right">10 : 15 PM</div>
                                </div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk5" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk5" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Ben French</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                    <div class="right">10 : 15 PM</div>
                                </div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk6" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk6" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Ben French</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                    <div class="right">10 : 15 PM</div>
                                </div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk7" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk7" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Ben French</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                    <div class="right">10 : 15 PM</div>
                                </div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk8" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk8" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Ben French</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                    <div class="right">10 : 15 PM</div>
                                </div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk9" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk9" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Ben French</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                    <div class="right">10 : 15 PM</div>
                                </div>
                            </div>
                        </div>
                        <div class="data row">
                        	<div class="col m1 delete">
                            	<div class="custom-checkbox">
                                    <input id="chk10" type="checkbox" name="" class="styled-checkbox" >
                                    <label for="chk10" class="css-label"></label>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Ben French</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                    <div class="right">10 : 15 PM</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    		</div>
                            <div>
                            	<div class="notification-grid">
                                    <div class="title row">
                                        <div class="col m1 delete"> <a href="#" title="Delete" class="icon-delete"></a> </div>
                                        <div class="col m2 text-center">
                                            <div class="bg-container">Date</div>
                                        </div>
                                        <div class="col m3 text-center">
                                            <div class="bg-container">Sender</div>
                                        </div>
                                        <div class="col m6">
                                            <div class="bg-container">Subject</div>
                                        </div>
                                    </div>
                                    <div class="data row">
                                        <div class="col m1 delete">
                                            <div class="custom-checkbox">
                                                <input id="chk20" type="checkbox" name="" class="styled-checkbox" >
                                                <label for="chk20" class="css-label"></label>
                                            </div>
                                        </div>
                                        <div class="col m2 text-center">
                                            <div class="bg-container">10/02/2017</div>
                                        </div>
                                        <div class="col m3 text-center">
                                            <div class="bg-container">Ben French</div>
                                        </div>
                                        <div class="col m6">
                                            <div class="bg-container">
                                                <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                                <div class="right">10 : 15 PM</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="data row">
                                        <div class="col m1 delete">
                                            <div class="custom-checkbox">
                                                <input id="chk21" type="checkbox" name="" class="styled-checkbox" >
                                                <label for="chk21" class="css-label"></label>
                                            </div>
                                        </div>
                                        <div class="col m2 text-center">
                                            <div class="bg-container">10/02/2017</div>
                                        </div>
                                        <div class="col m3 text-center">
                                            <div class="bg-container">Ben French</div>
                                        </div>
                                        <div class="col m6">
                                            <div class="bg-container">
                                                <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                                <div class="right">10 : 15 PM</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="data row">
                                        <div class="col m1 delete">
                                            <div class="custom-checkbox">
                                                <input id="chk22" type="checkbox" name="" class="styled-checkbox" >
                                                <label for="chk22" class="css-label"></label>
                                            </div>
                                        </div>
                                        <div class="col m2 text-center">
                                            <div class="bg-container">10/02/2017</div>
                                        </div>
                                        <div class="col m3 text-center">
                                            <div class="bg-container">Ben French</div>
                                        </div>
                                        <div class="col m6">
                                            <div class="bg-container">
                                                <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                                <div class="right">10 : 15 PM</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="data row">
                                        <div class="col m1 delete">
                                            <div class="custom-checkbox">
                                                <input id="chk23" type="checkbox" name="" class="styled-checkbox" >
                                                <label for="chk23" class="css-label"></label>
                                            </div>
                                        </div>
                                        <div class="col m2 text-center">
                                            <div class="bg-container">10/02/2017</div>
                                        </div>
                                        <div class="col m3 text-center">
                                            <div class="bg-container">Ben French</div>
                                        </div>
                                        <div class="col m6">
                                            <div class="bg-container">
                                                <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                                <div class="right">10 : 15 PM</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="data row">
                                        <div class="col m1 delete">
                                            <div class="custom-checkbox">
                                                <input id="chk24" type="checkbox" name="" class="styled-checkbox" >
                                                <label for="chk24" class="css-label"></label>
                                            </div>
                                        </div>
                                        <div class="col m2 text-center">
                                            <div class="bg-container">10/02/2017</div>
                                        </div>
                                        <div class="col m3 text-center">
                                            <div class="bg-container">Ben French</div>
                                        </div>
                                        <div class="col m6">
                                            <div class="bg-container">
                                                <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                                <div class="right">10 : 15 PM</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="data row">
                                        <div class="col m1 delete">
                                            <div class="custom-checkbox">
                                                <input id="chk25" type="checkbox" name="" class="styled-checkbox" >
                                                <label for="chk25" class="css-label"></label>
                                            </div>
                                        </div>
                                        <div class="col m2 text-center">
                                            <div class="bg-container">10/02/2017</div>
                                        </div>
                                        <div class="col m3 text-center">
                                            <div class="bg-container">Ben French</div>
                                        </div>
                                        <div class="col m6">
                                            <div class="bg-container">
                                                <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                                <div class="right">10 : 15 PM</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="data row">
                                        <div class="col m1 delete">
                                            <div class="custom-checkbox">
                                                <input id="chk26" type="checkbox" name="" class="styled-checkbox" >
                                                <label for="chk26" class="css-label"></label>
                                            </div>
                                        </div>
                                        <div class="col m2 text-center">
                                            <div class="bg-container">10/02/2017</div>
                                        </div>
                                        <div class="col m3 text-center">
                                            <div class="bg-container">Ben French</div>
                                        </div>
                                        <div class="col m6">
                                            <div class="bg-container">
                                                <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                                <div class="right">10 : 15 PM</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="data row">
                                        <div class="col m1 delete">
                                            <div class="custom-checkbox">
                                                <input id="chk27" type="checkbox" name="" class="styled-checkbox" >
                                                <label for="chk27" class="css-label"></label>
                                            </div>
                                        </div>
                                        <div class="col m2 text-center">
                                            <div class="bg-container">10/02/2017</div>
                                        </div>
                                        <div class="col m3 text-center">
                                            <div class="bg-container">Ben French</div>
                                        </div>
                                        <div class="col m6">
                                            <div class="bg-container">
                                                <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                                <div class="right">10 : 15 PM</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="data row">
                                        <div class="col m1 delete">
                                            <div class="custom-checkbox">
                                                <input id="chk28" type="checkbox" name="" class="styled-checkbox" >
                                                <label for="chk28" class="css-label"></label>
                                            </div>
                                        </div>
                                        <div class="col m2 text-center">
                                            <div class="bg-container">10/02/2017</div>
                                        </div>
                                        <div class="col m3 text-center">
                                            <div class="bg-container">Ben French</div>
                                        </div>
                                        <div class="col m6">
                                            <div class="bg-container">
                                                <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                                <div class="right">10 : 15 PM</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="data row">
                                        <div class="col m1 delete">
                                            <div class="custom-checkbox">
                                                <input id="chk29" type="checkbox" name="" class="styled-checkbox" >
                                                <label for="chk29" class="css-label"></label>
                                            </div>
                                        </div>
                                        <div class="col m2 text-center">
                                            <div class="bg-container">10/02/2017</div>
                                        </div>
                                        <div class="col m3 text-center">
                                            <div class="bg-container">Ben French</div>
                                        </div>
                                        <div class="col m6">
                                            <div class="bg-container">
                                                <div class="left">Lorem ipsum dolor sit amet, consectetur...</div>
                                                <div class="right">10 : 15 PM</div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                	
                </div>
                <div class="notification-email">
                    <div class="custom-checkbox">
                        <input id="chkNotification" type="checkbox" name="chkNotification" class="styled-checkbox" >
                        <label for="chkNotification" class="">Receive Notifications by Email</label>
                    </div>
                </div>
            </section>
            
        </div>
    </div>
</div>
@stop

