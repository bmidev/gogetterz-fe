@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<h3 class="heading__h3">Create Quick Lesson</h3>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="member-breadcrumb">
               		<a href="{{ BASE_URL }}member-quick-lesson">Quick Lesson</a> 
                    <span class="divider"> > </span> 
                    Create Quick Lesson
                </div>
            	<div class="content-white-box">
                	<h3 class="heading__h3">Create Quick Lesson</h3>
                    <div class="form-container">
                    	<div class="form-element">
                        	<label for="txtCouponTitle" class="title">Title</label>
                            <input name="" type="text" class="width40" id="txtCouponTitle" placeholder="Coupon Title">
                        </div>
                        <div class="form-element">
                        	<label for="txtDescription" class="title">Description</label>
                            <textarea name="" rows="4" id="txtDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</textarea>
                        </div>
                        <div class="form-element">
                        	<label for="txtCourseThumbnail" class="title">Course Thumbnail</label>
                            <div class="file-upload upload-img">
                                <input type="file" name="receipt" id="file-browse" class="inputfile" />
                                <label for="file-browse"><span class="input-text">&nbsp;</span> <strong>+ Upload Image</strong></label>
                            </div>
                            <div class="left note">Course Image Format : Supported format for uploading the file : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtCourseThumbnail" class="title">Course Thumbnail</label>
                            <div class="file-upload upload-video">
                                <input type="file" name="receipt" id="file-browse" class="inputfile" />
                                <label for="file-browse"><span class="input-text">&nbsp;</span> <strong>+ Upload Image</strong></label>
                            </div>
                            <div class="left note">Course Video Format : Supported format for uploading the file : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
                        </div>
                        <div class="form-element button-container">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="right-container">
                                <input type="submit" value="Cancel" class="button black tinysize mid">
                                <input type="submit" value="Submit" class="button lightpink tinysize mid mLeft20">
                            </div>
                        </div>
                    </div>
                </div>
            </section>            
        </div>
    </div>
</div>
@stop

