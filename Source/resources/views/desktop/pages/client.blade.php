@extends('desktop.layouts.master')
@section('content')
    <div class="client-bg">
        <section class="top-banner">
            <div class="m-page-container">
                <div class="top-banner-image">
                    <img src="{{ asset('desktop/images/client/client-banner.jpg') }}" alt=""/>
                </div>
                <div class="top-banner-copy">
                    <h1 class="heading__h1">INTRODUCTION</h1>
                    <p>GoGetterz is a platform for someone who wants to develop a new skill for their current career, attain a qualification for a new career and to sharpen their talent with help of the best experts in their respective fields. We develop a bespoke learning style for you to attain your goals. We transform beginners into masters on any skill they embark upon in the process of learning & developing.</p>
                    <div class="button-container">
                        <a href="#" class="button black large small">Contact Us</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="what-we-offer boxes-grid-4">
            <div class="m-page-container">
                <h2 class="heading__h2">What we offer</h2>
                <div class="box-container">
                    <div class="teach-with-boxes">
                        <div class="teach-with-box">
                            <div class="teach-with-box-image" style="background-image:url({{ asset('desktop/images/client/freedom-of-choice.svg') }})"></div>
                            <div class="teach-with-box-copy">
                                <h5 class="heading__h5">Freedom of choice</h5>
                                <p>We give you a learning experience with a freedom of choice that matches your lifestyle and goals as per your convenience.</p>
                            </div>
                        </div>
                        <div class="teach-with-box">
                            <div class="teach-with-box-image" style="background-image:url({{ asset('desktop/images/client/goal-oriented.svg') }})"></div>
                            <div class="teach-with-box-copy">
                                <h5 class="heading__h5">Goal oriented learning</h5>
                                <p>We believe that one should set a clear goal in their journey of learning. So, you get a unique learning platform that provides you with a distinct path to achieve your individual goals. </p>
                            </div>
                        </div>
                    </div>
                    <div class="teach-with-boxes">
                        <div class="teach-with-box">
                            <div class="teach-with-box-image" style="background-image:url({{ asset('desktop/images/client/composite-learning.svg') }})"></div>
                            <div class="teach-with-box-copy">
                                <h5 class="heading__h5">Composite learning platform</h5>
                                <p>GoGetterz provides one to one support for all your queries. If you are an individual or a corporation all provide you all the assistance from online course creation to uploading, marketing, etc.</p>
                            </div>
                        </div>
                        <div class="teach-with-box">
                            <div class="teach-with-box-image" style="background-image:url({{ asset('desktop/images/client/mentoring.svg') }})"></div>
                            <div class="teach-with-box-copy">
                                <h5 class="heading__h5">Mentoring function</h5>
                                <p>Every course has a ‘classroom function’ option which enables experts to check on the status of members. Experts can also support their learning process by personally messaging via email.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="choose-gogetterz">
            <div class="m-page-container">
                <h2 class="heading__h2">WHY CHOOSE GOGETTERZ</h2>
                <div class="choose-gogetterz-boxes">
                    <div class="choose-gogetterz-box">
                        <div class="box-container">
                            <h5 class="heading__h5 box-heading">Quis nostrud exerc ullamco laboris nisi </h5>
                            <p class="box-copy">We offer rich course variations for all 10 genres and 77 categories. You can take the best course according to your level and purpose.</p>
                            <h2 class="heading__h2 box-footer">01</h2>
                        </div>
                    </div>
                    <div class="choose-gogetterz-box">
                        <div class="box-container">
                            <h5 class="heading__h5 box-heading">Duis aute irure dolor in</h5>
                            <p class="box-copy">You can take full-fledged courses handled anywhere by top-notch experts who have dedicated their way, including active professors and best-selling writers.</p>
                            <h2 class="heading__h2 box-footer">02</h2>
                        </div>
                    </div>
                    <div class="choose-gogetterz-box">
                        <div class="box-container">
                            <h5 class="heading__h5 box-heading">Excepteur sint occaecat cupidatat non proident</h5>
                            <p class="box-copy">We offer all 500 courses handed by leading experts for ¥ 1,000 per month. It can be used not only for a continuous term contract but also for the necessary period at a necessary time.</p>
                            <h2 class="heading__h2 box-footer">03</h2>
                        </div>
                    </div>
                    <div class="choose-gogetterz-box">
                        <div class="box-container">
                            <h5 class="heading__h5 box-heading">dolor enim ad minim veniam quis nostrud</h5>
                            <p class="box-copy">In addition to offering videos, we also offer support and suggestions for experts in charge, such as confirming the attendance status and supporting attendance, dispatching lecturers for training and video production services.</p>
                            <h2 class="heading__h2 box-footer">04</h2>
                        </div>
                    </div>
                </div>
                <div class="button-container">
                    <a href="#" class="button black large small">Contact Us</a>
                </div>
            </div>
        </section>
        <section class="client-reviews">
            <div class="m-page-container">
                <h2 class="heading__h2">CLIENT REVIEWS</h2>
                <div class="review_container">
                    <div class="review clear">
                        <div class="review-img" style="background-image: url({{ asset('desktop/images/client/review-user.jpg') }})"></div>
                        <div class="review-copy">
                            <div class="star-container">
                                <ul>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                </ul>
                            </div>
                            <h3 class="reviewer_name">Username</h3>
                            <p class="review_comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                    <div class="review clear">
                        <div class="review-img" style="background-image: url({{ asset('desktop/images/client/review-user.jpg') }})"></div>
                        <div class="review-copy">
                            <div class="star-container">
                                <ul>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                </ul>
                            </div>
                            <h3 class="reviewer_name">Username</h3>
                            <p class="review_comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                    <div class="review clear">
                        <div class="review-img" style="background-image: url({{ asset('desktop/images/client/review-user.jpg') }})"></div>
                        <div class="review-copy">
                            <div class="star-container">
                                <ul>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                </ul>
                            </div>
                            <h3 class="reviewer_name">Username</h3>
                            <p class="review_comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                    <div class="review clear">
                        <div class="review-img" style="background-image: url({{ asset('desktop/images/client/review-user.jpg') }})"></div>
                        <div class="review-copy">
                            <div class="star-container">
                                <ul>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                </ul>
                            </div>
                            <h3 class="reviewer_name">Username</h3>
                            <p class="review_comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                    <div class="review clear">
                        <div class="review-img" style="background-image: url({{ asset('desktop/images/client/review-user.jpg') }})"></div>
                        <div class="review-copy">
                            <div class="star-container">
                                <ul>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                </ul>
                            </div>
                            <h3 class="reviewer_name">Username</h3>
                            <p class="review_comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                    <div class="review clear">
                        <div class="review-img" style="background-image: url({{ asset('desktop/images/client/review-user.jpg') }})"></div>
                        <div class="review-copy">
                            <div class="star-container">
                                <ul>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                    <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                </ul>
                            </div>
                            <h3 class="reviewer_name">Username</h3>
                            <p class="review_comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                </div>
                <div class="review-load-more">
                    <a href="#">Load More
                        <img src="{{ asset('desktop/images/client/down-arrow.svg') }}" />
                    </a>
                </div>
            </div>
        </section>
        <section class="brands">
            <div class="m-page-container">
                <h2 class="heading__h2">BRANDS USING GOGETTERZ</h2>
                <div class="brand-container">
                    <div class="brand-container-grid">
                        <div class="brand-logo">
                            <img src="{{ asset('desktop/images/client/brands/toyota-logo.png') }}" />
                        </div>
                        <div class="brand-logo">
                            <img src="{{ asset('desktop/images/client/brands/Sony_logo.png') }}" />
                        </div>
                        <div class="brand-logo">
                            <img src="{{ asset('desktop/images/client/brands/bmw-logo.png') }}" />
                        </div>
                        <div class="brand-logo">
                            <img src="{{ asset('desktop/images/client/brands/samsung_logo.png') }}"  />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="service-introduction">
            <div class="m-page-container">
                <h2 class="heading__h2">SERVICE INTRODUCTION FLOW</h2>
                <div class="introduction-steps">
                    <div class="service-step">
                        <h2 class="heading__h2 step-header">STEP 1</h2>
                        <h5 class="heading__h5 step-sub-header">Contact us</h5>
                        <p class="step-copy">After inquiry, we will contact you from our charge. Please tell us about usage and subjects etc.</p>
                    </div>
                    <div class="service-step">
                        <h2 class="heading__h2 step-header">STEP 2</h2>
                        <h5 class="heading__h5 step-sub-header">Proposal and estimate of the plan</h5>
                        <p class="step-copy">After inquiry, we will contact you from our charge. Please tell us about usage and subjects etc.</p>
                    </div>
                    <div class="service-step">
                        <h2 class="heading__h2 step-header">STEP 3</h2>
                        <h5 class="heading__h5 step-sub-header">Plan decision · contract</h5>
                        <p class="step-copy">After inquiry, we will contact you from our charge. Please tell us about usage and subjects etc.</p>
                    </div>
                    <div class="service-step">
                        <h2 class="heading__h2 step-header">STEP 4</h2>
                        <h5 class="heading__h5 step-sub-header">Start using</h5>
                        <p class="step-copy">After inquiry, we will contact you from our charge. Please tell us about usage and subjects etc.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="contact-us">
            <div class="m-page-container">
                <h2 class="heading__h2">CONTACT US</h2>
                <div class="form-container">
                    <div class="form-item">
                        <h5 class="heading__h5">Write a Query!</h5>
                        <form>
                            <div class="form-element">
                                <input type="text" name="company-name" placeholder="Company Name">
                            </div>
                            <div class="form-element">
                                <input type="text" name="kanji-name" placeholder="Name in Kanji">
                            </div>
                            <div class="form-element">
                                <input type="text" name="karntaka-name" placeholder="Name in Karnataka">
                            </div>
                            <div class="form-element">
                                <input type="text" name="email" placeholder="Email">
                            </div>
                            <div class="form-element">
                                <input type="text" name="telephone" placeholder="Telephone">
                            </div>
                            <div class="form-element">
                                <textarea name="message" placeholder="Message" ></textarea>
                            </div>
                            <p>Which services are you interested in?</p>
                            <div class="checkbox-container">
                                <div class="checkbox">
                                    <input id="chk1" type="checkbox" name="" class="css-checkbox" >
                                    <label for="chk1" class="css-label">Freedom of choice</label>
                                </div>
                                <div class="checkbox">
                                    <input id="chk2" type="checkbox" name="" class="css-checkbox" >
                                    <label for="chk2" class="css-label">Composite learning platform</label>
                                </div>
                                <div class="checkbox">
                                    <input id="chk3" type="checkbox" name="" class="css-checkbox" >
                                    <label for="chk3" class="css-label">Goal oriented learning</label>
                                </div>
                                <div class="checkbox">
                                    <input id="chk4" type="checkbox" name="" class="css-checkbox" >
                                    <label for="chk4" class="css-label">Mentoring function</label>
                                </div>
                            </div>
                            <div class="button-container">
                                <a href="#" class="button pink large small">Submit</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="contact-us-image">
                    <img src="{{ asset('desktop/images/client/contact-us.svg') }}" />
                </div>
            </div>
        </section>
    </div>
@stop
