@extends('desktop.layouts.master')
@section('content')
    <section class="event-name">
        <div class="m-page-container">
            <h1 class="heading__h1">NAME OF THE EVENT</h1>
            <div class="event-info">
                <div class="event-date">
                    <img src="{{ asset('desktop/images/icon-calender.svg') }}" alt="" />
                    <h5 class="heading__h5">Date : 10th January 2018</h5>
                </div>
                <div class="event-venue">
                    <img src="{{ asset('desktop/images/icon-location.svg') }}" alt="" />
                    <h5 class="heading__h5">Venue : Nisi vitae suscipit tellus mauris</h5>
                </div>
            </div>
            <div class="event-content clear">
                <div class="event-copy">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat proident, sunt in culpa qui officia deserunt mollit.</p>
                    <p>Nunc congue nisi vitae suscipit tellus mauris a. Sit amet facilisis magna etiam tempor orci eu lobortis elementum. Lacus vel facilisis volutpat est velit egestas dui id. Pellentesque massa placerat duis ultricies lacus sed turpis. Molestie nunc non blandit massa enim nec dui nunc. Velit sed ullamcorper morbi tincidunt ornaresed lectus.</p>
                    <div class="button-container">
                        <a href="#" class="button grey midsize small">Enroll Now </a>
                    </div>
                </div>
                <div class="event-image">
                    <img src="{{ asset('desktop/images/upcoming-events/banner.jpg') }}" alt=""/>
                </div>
            </div>
        </div>
    </section>
    <section class="event-desc">
        <div class="m-page-container">
            <div class="event-desc-container">
                <div class="event-desc-content">
                    <h4 class="heading__h4">Lorem Ipsum</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eleifend donec pretium vulputate sapien. Dignissim cras tincidunt lobortis feugiat vivamus at. Amet consectetur adipiscing elit ut aliquam. Volutpat ac tincidunt vitae semper quis lectus nulla. Sed euismod nisi porta lorem mollis aliquam ut porttitor. Sollicitudin tempor id eu nisl nunc. Sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque eu. Diam quam nulla porttitor massa. Egestas integer eget aliquet nibh praesent tristique. Accumsan lacus vel facilisis volutpat est velit egestas. Eleifend quam adipiscing vitae proin sagittis. Nunc aliquet bibendum enim facilisis. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. At tempor commodo ullamcorper a lacus vestibulum. Leo in vitae turpis massa sed elementum tempus egestas. Ac auctor augue mauris augue neque. Sed egestas egestas fringilla phasellus faucibus. Placerat vestibulum lectus mauris ultrices eros in cursus turpis massa.</p>
                    <p>Enim diam vulputate ut pharetra sit amet aliquam id. Id velit ut tortor pretium viverra suspendisse potenti nullam ac. Mauris cursus mattis molestie a. Aliquet lectus proin nibh nisl condimentum id venenatis. Purus viverra accumsan in nisl nisi scelerisque eu ultrices. Augue ut lectus arcu bibendum at varius vel. Urna cursus eget nunc scelerisque viverra mauris. Mattis nunc sed blandit libero volutpat sed. Gravida rutrum quisque non tellus orci ac auctor augue. Imperdiet dui accumsan sit amet nulla facilisi morbi. Ultricies integer quis auctor elit sed. Aliquam ultrices sagittis orci a scelerisque purus. Purus gravida quis blandit turpis cursus. Lorem ipsum dolor sit amet consectetur adipiscing elit duis. Molestie nunc non blandit massa enim nec dui nunc. Sed turpis tincidunt id aliquet risus feugiat in ante. Amet consectetur adipiscing elit pellentesque.</p>
                </div>

            </div>
        </div>
    </section>
@stop