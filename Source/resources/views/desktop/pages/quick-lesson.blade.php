@extends('desktop.layouts.master')
@section('content')

<section class="category-promo">
    <div class="m-page-container">
        <div class="category-promo-image">
            <img src="{{ asset('desktop/images/jp/category-promo-language.jpg') }}" alt=""/>
        </div>
        <div class="welcome-copy">
            <h2 class="heading__h2">Trust yourself</h2>
            <p>Ornare quam viverra orci sagittis eu volutpat. Sed sed risus pretium quam vulputate dignissim suspendisse in est. Sed id semper risus in hendrerit gravida rutrum quisque non. Bibendum neque egestas congue quisque egestas diam in arcu cursus. </p>
            <p>Dignissim suspendisse in est ante in nibh. Facilisi morbi tempus iaculis urna id volutpat lacus laoreet. Eget mauris pharetra et ultrices neque ornare aenean euismod. Arcu odio ut sem nulla pharetra diam. </p>

        </div>
    </div>
</section>
<section class="course-detais-view">
    <div class="m-page-container">
        <div class="course-header-links">
            <div class="sort-by">
                <select class="custom-select">
                    <option>SORT BY</option>
                    <option>Alphabetically A-Z</option>
                    <option>Free Courses</option>
                    <option>Most Popular</option>
                    <option>Most Relevant</option>
                </select>
            </div>
            <div class="select-lang">
                <select class="custom-select">
                    <option>ENGLISH LANGUAGE</option>
                    <option>All Languages</option>
                    <option>Japanese Language</option>
                    <option>Japanese Language</option>
                </select>
            </div>
            {{--<div class="filter">--}}
                {{--<a href="#" class="button grey-fill midsize mid caps">filter</a>--}}
            {{--</div>--}}
        </div>
        <div class="course-articles">
            <div class="article-wrapper">
                <article class="course">
                    <a href="#quick-lesson-modal" class="quick-lesson-video">
                        <figure>
                          <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-01.jpg') }})"></span>
                        </figure>
                    </a>
                    <div class="article-info">
                        <h4><a href="#">English For Beginners</a></h4>
                    </div>
                </article>
            </div>
            <div class="article-wrapper">
                <article class="course">
                    <a href="#quick-lesson-modal" class="quick-lesson-video">
                        <figure>
                           <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-02.jpg') }})"></span>
                        </figure>
                    </a>
                    <div class="article-info">
                        <h4><a href="#">English Email Tips</a></h4>
                    </div>
                </article>
            </div>
            <div class="article-wrapper">
                <article class="course">
                    <a href="#quick-lesson-modal" class="quick-lesson-video">
                        <figure>
                            <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-03.jpg') }})"></span>
                        </figure>
                    </a>
                    <div class="article-info">
                        <h4><a href="#">Spoken English</a></h4>
                    </div>
                </article>
            </div>
            <div class="article-wrapper">
                <article class="course">
                    <a href="#quick-lesson-modal" class="quick-lesson-video">
                        <figure>
                            <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-01.jpg') }})"></span>
                        </figure>
                    </a>
                    <div class="article-info">
                        <h4><a href="#">English For Beginners</a></h4>
                    </div>
                </article>
            </div>
            <div class="article-wrapper">
                <article class="course">
                    <a href="#quick-lesson-modal" class="quick-lesson-video">
                        <figure>
                            <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-02.jpg') }})"></span>
                        </figure>
                    </a>
                    <div class="article-info">
                        <h4><a href="#">English Email Tips</a></h4>
                    </div>
                </article>
            </div>
            <div class="article-wrapper">
                <article class="course">
                    <a href="#quick-lesson-modal" class="quick-lesson-video">
                        <figure>
                            <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-03.jpg') }})"></span>
                        </figure>
                    </a>
                    <div class="article-info">
                        <h4><a href="#">Spoken English</a></h4>
                    </div>
                </article>
            </div>
            <div class="article-wrapper">
                <article class="course">
                    <a href="#quick-lesson-modal" class="quick-lesson-video">
                        <figure>
                            <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-01.jpg') }})"></span>
                        </figure>
                    </a>
                    <div class="article-info">
                        <h4><a href="#">English For Beginners</a></h4>
                    </div>
                </article>
            </div>
            <div class="article-wrapper">
                <article class="course">
                    <a href="#quick-lesson-modal" class="quick-lesson-video">
                        <figure>
                            <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-02.jpg') }})"></span>
                        </figure>
                    </a>
                    <div class="article-info">
                        <h4><a href="#">English Email Tips</a></h4>
                    </div>
                </article>
            </div>
            <div class="article-wrapper">
                <article class="course">
                    <a href="#quick-lesson-modal" class="quick-lesson-video">
                        <figure>
                            <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-03.jpg') }})"></span>
                        </figure>
                    </a>
                    <div class="article-info">
                        <h4><a href="#">Spoken English</a></h4>
                    </div>
                </article>
            </div>
        </div>
    </div>
    <!--Modal code-->
    <div class="hide">
        @include('desktop.modals.quick-lesson-video')
    </div>
</section>
@stop

