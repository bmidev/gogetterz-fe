@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<h3 class="heading__h3">Profile settings</h3>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="content-white-box">
                	<h3 class="heading__h3">Personal Settings</h3>
                    <div class="form-container">
                    	<div class="form-element">
                        	<label for="txtUsername" class="title">Username*</label>
                            <input name="txtUsername" type="text" id="txtUsername" class="width40">
                        </div>
                        <div class="form-element">
                        	<label for="txtName" class="title">Name(Optional)</label>
                            <input name="txtName" type="text" id="txtName" class="width40">
                        </div>
                        <div class="form-element">
                        	<label for="txtEmail" class="title">Email*</label>
                            <input name="txtEmail" type="text" id="txtEmail" class="width40">
                        </div>
                        <div class="form-element">
                        	<label for="txtBirthday" class="title">Birthday*</label>
                            <input name="txtBirthday" type="date" id="txtBirthday" class="width30">
                        </div>
                        <div class="form-element">
                        	<label for="txtCompanyName	" class="title">Gender*</label>
                            <div class="radioGroup">
                            	<div class="radio-item">
                                	<input name="rdoGender" id="rdoGender1" type="radio" class="radio-btn">
                                    <label for="rdoGender1" class="radio-label">Male</label>
                                </div>
                                <div class="radio-item">
                                	<input name="rdoGender" id="rdoGender2" type="radio" class="radio-btn">
                                    <label for="rdoGender2" class="radio-label">Female</label>
                                </div>
                                <div class="radio-item">
                                	<input name="rdoGender" id="rdoGender3" type="radio" class="radio-btn">
                                    <label for="rdoGender3" class="radio-label">Others</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtAddress1" class="title">Address</label>
                            <input name="txtAddress1" type="text" id="txtAddress1" placeholder="Address Line 1">
                        </div>
                        <div class="form-element">
                            <label for="txtAddress2" class="title"></label>
                            <input name="txtAddress2" type="text" id="txtAddress2" placeholder="Address Line 2">
                        </div>
                        <div class="form-element">
                        	<label for="lstCountry" class="title">Country*</label>
                            <select name="lstCountry" class="custom-select" id="lstCountry">
                                <option>Japan</option>
                            </select>
                        </div>
                        <div class="form-element">
                        	<label for="txtCompanyName" class="title">Company Name</label>
                            <input name="txtCompanyName" type="text" id="txtCompanyName" placeholder="">
                        </div>
                        <div class="form-element">
                        	<label for="txtPosition" class="title">Position</label>
                            <input name="txtPosition" type="text" id="txtPosition" placeholder="" class="width40">
                        </div>
                        <div class="form-element">
                        	<label for="txtLanguagePreference" class="title">Language Preference*</label>
                            <select name="txtLanguagePreference" class="custom-select" id="txtLanguagePreference">
                                <option>English</option>
                            </select>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-grey-box ">
            	<h3 class="heading__h3 bg-grey">Expert Settings</h3>
            	<div class="content-white-box no-top-border">  
                	<h4 class="heading__h4">Profile Details</h4>              	
                    <div class="form-container">
                    	<div class="form-element">
                        	<label for="txtOverview" class="title">Profile Overview<br>(Expert)</label>
                            <textarea name="txtOverview" rows="10" id="txtOverview">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tortor posuere ac ut consequat. Nulla aliquet porttitor lacus luctus accumsan tortor. Est sit amet facilisis magna etiam tempor orci eu. Maecenas volutpat blandit aliquam etiam erat velit scelerisque in. Purus faucibus ornare suspendisse sed nisi lacus sed viverra tellus. Elementum facilisis leo vel fringilla est. Eget gravida cum sociis natoque penatibus et magnis dis. Venenatis lectus magna fringilla urna porttitor. Vitae tempus quam pellentesque nec nam.</textarea>
                        </div>
                        <div class="form-element">
                        	<label for="txtPromoVideos" class="title">Promo Videos</label>
                            <div class="file-upload lightpink">
                                <input type="file" name="receipt" id="file-browse" class="inputfile" />
                                <label for="file-browse"><span class="input-text">Upload File</span> <strong>&nbsp;</strong></label>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtEmail" class="title">SNS Public Link</label>
                            <div class="right-container">                            	
                                <div class="social-links-container">
                                	<p>Link Your Expert Profile</p>
                                    <div class="social-row">
                                        <div class="left">
                                            <span class="icon-fb black"></span> Facebook
                                        </div>
                                        <div class="right">
                                        	<div class="account-info">
                                            	Mitchel Jones
                                                <button class="button grey-fill no-hover tinysize small">Remove Account</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="social-row">
                                        <div class="left">
                                            <span class="icon-ig black"></span> Instagram
                                        </div>
                                        <div class="right">
                                            <button class="button grey-fill tinysize small">Link Account</button>
                                        </div>
                                    </div>
                                    <div class="social-row">
                                        <div class="left">
                                            <span class="icon-tw black"></span> Twitter
                                        </div>
                                        <div class="right">
                                            <button class="button grey-fill tinysize small">Link Account</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="divider">&nbsp;</div>
                        <h4 class="heading__h4">Notation based on Specified Commercial Transactions Law <a href="#" class="icon-info small"></a></h4>
                        <div class="form-element">
                        	<label for="txtNameInCharge" class="title">Name/Person in charge</label>
                            <input name="txtNameInCharge" type="text" id="txtNameInCharge" class="width40">
                        </div>
                        <div class="form-element">
                        	<label for="txtAddressInCharge1" class="title">Address</label>
                            <input name="txtAddressInCharge1" type="text" id="txtAddressInCharge1" placeholder="Address Line 1">
                        </div>
                        <div class="form-element">
                            <label for="txtAddressInCharge2" class="title"></label>
                            <input name="txtAddressInCharge2" type="text" id="txtAddressInCharge2" placeholder="Address Line 2">
                        </div>
                        <div class="form-element">
                            <label for="txtWebsite" class="title">Website URL</label>
                            <div class="right-container">
                            	<button class="button grey-fill tinysize small">Link Account</button>
                            </div>
                        </div>
                        <div class="divider">&nbsp;</div>
                        <h4 class="heading__h4">Bank Transfer Information</h4>
                        <div class="form-element">
                        	<label for="lstReceiveSales" class="title">How to Receive Sales*</label>
                            <select name="lstReceiveSales" class="custom-select width40" id="lstReceiveSales">
                                <option>Bank Transfer</option>
                            </select>
                        </div>
                        <div class="form-element">
                        	<label for="lstBankName" class="title">Bank Name*</label>
                            <select name="lstBankName" class="custom-select width40" id="lstBankName">
                                <option>HSBC Bank</option>
                            </select>
                        </div>
                        <div class="form-element">
                        	<label for="lstBranchName" class="title">Branch Name*</label>
                            <select name="lstBranchName" class="custom-select width40" id="lstBranchName">
                                <option>Lorem Ipsum</option>
                            </select>
                        </div>
                        <div class="form-element">
                        	<label for="rdoDepositCategory" class="title">Deposit Category*</label>
                            <div class="radioGroup">
                            	<div class="radio-item">
                                	<input name="rdoDepositCategory" id="rdoDepositCategory1" type="radio" class="radio-btn">
                                    <label for="rdoDepositCategory1" class="radio-label">Usually</label>
                                </div>
                                <div class="radio-item">
                                	<input name="rdoDepositCategory" id="rdoDepositCategory2" type="radio" class="radio-btn">
                                    <label for="rdoDepositCategory2" class="radio-label">Current</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtAccountNumber" class="title">Account Number*</label>
                            <input name="txtAccountNumber" type="text" id="txtAccountNumber" placeholder="" class="width40">
                        </div>
                        <div class="form-element">
                        	<label for="txtAccountHolder" class="title">Account Holder*</label>
                            <input name="txtAccountHolder" type="text" id="txtAccountHolder" placeholder="" class="width40">
                        </div>
                        <div class="form-element button-container">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="right-container">
                                <input type="submit" value="Cancel" class="button black tinysize mid">
                                <input type="submit" value="Save Changes" class="button lightpink tinysize mid mLeft20">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@stop

