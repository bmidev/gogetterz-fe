@extends('desktop.layouts.master')
@section('content')

<section class="thanks-container">
	<div class="copy-container">
       	<h2 class="heading__h2">Thank you for signing up!</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip commodo consequat. </p>
    	<div class="button-container">
        	<a href="#" class="button grey-fill midsize full-width">Go to Home Page</a>
        </div>
    </div>        
</section> 

@stop

