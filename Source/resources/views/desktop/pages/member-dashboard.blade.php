@extends('desktop.layouts.master')
@section('content')
<script src="{{ asset('desktop/js/chart.js') }}"></script>
<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Dashboard</h3>
                </div>
                <div class="right">
                	<nav class="heading-nav">
                    	<ul>
                    	    <li><a href="#" class="active">Daily</a></li>
                            <li><a href="#">Weekly</a></li>
                            <li><a href="#">Monthly</a></li>
                	    </ul>
                    </nav>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="expert-vs-learner-button">
                	Expert
                	<label class="switch black">
                        <input type="checkbox" class="" checked>
                        <span class="slider"></span>
                    </label>
                    Learner
                </div>
                <div class="date-range daily" style="display:none">
                	<div class="form-container">
                    	<div class="form-element">
                        	<label for="txtFromDate" class="title">From :</label>
                            <input name="txtFromDate" type="date" id="txtFromDate">
                        </div>
                        <div class="form-element">
                        	<label for="txtToDate" class="title">To :</label>
                            <input name="txtToDate" type="date" id="txtToDate">
                        </div>
                    </div>
                </div>
                <div class="date-range weekly" style="display:block">
                	<div class="form-container">
                    	<div class="form-element">
                        	<label for="txtFromDate" class="title">Month :</label>
                            <select class="custom-select white">
                                <option>January</option>
                            </select>
                        </div>
                        <div class="form-element">
                        	<label for="txtToDate" class="title">Year :</label>
                            <select class="custom-select white">
                                <option> 2018</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="date-range monthly" style="display:none">
                	<div class="form-container">                    	
                        <div class="form-element">
                        	<label for="txtToDate" class="title">Year :</label>
                            <select class="custom-select white">
                                <option> 2018</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="chart-container">
                	<div class="chart-header">
                    	<h4 class="heading__h4">Hours Spent Per Day</h4>
                        <div class="chart-select-course">
                            <select class="select-multi-course" multiple="multiple">                                
                                <option value="1">All Categories</option>
                                <option value="2">Languages</option>
                                <option value="3">Health &amp; Wellbeing</option>
                                <option value="5">Technology &amp; Design</option>                                
                                <option value="6">Option 6</option>
                                <option value="7">Option 7</option>
                                <option value="8">Option 8</option>
                            </select>
                        </div>
                    </div>
                    <div class="chart-canvas">
                    	<canvas id="multi-area-chart"></canvas>
                    	<script>
							var ctx = document.getElementById("multi-area-chart").getContext("2d");

							const colors = {
							  red01: {
								fill: '#fbd3d3',
								stroke: '#fbd3d3',
							  },
							  red02: {
								fill: '#d53838',
								stroke: '#d53838',
							  },
							  red03: {
								fill: '#ff7373',
								stroke: '#ff7373',
							  },
							  red04: {
								fill: '#fc4c4c',
								stroke: '#fc4c4c',
							  },
							};
							
							const courseName01 = [0, 1, 0.2, 0.1, 0.5, 0.2, 2, 1.5];
							const courseName02 = [0, 0.7, 0.2, 0.3, 1, 0.3, 2, 1.5];
							const courseName03 = [0, 1, 2, 0.3, 1.5, 0.8, 2, 1.5];
							const unavailable = [0, 0.3, 0.2, 0.5, 1, 0.8, 2, 1.5];
							const xData = [10, 11, 12, 13, 14, 15, 16, 17];
							
							const myChart = new Chart(ctx, {
							  type: 'line',
							  data: {
								labels: xData,
								datasets: [{
								  label: "Unavailable",
								  fill: true,
								  backgroundColor: colors.red04.fill,
								  pointBackgroundColor: colors.red04.stroke,
								  borderColor: colors.red04.stroke,
								  pointHighlightStroke: colors.red04.stroke,
								  borderCapStyle: 'butt',
								  data: unavailable,
							
								}, {
								  label: "Course Name 03",
								  fill: true,
								  backgroundColor: colors.red03.fill,
								  pointBackgroundColor: colors.red03.stroke,
								  borderColor: colors.red03.stroke,
								  pointHighlightStroke: colors.red03.stroke,
								  borderCapStyle: 'butt',
								  data: courseName03,
								}, {
								  label: "Course Name 02",
								  fill: true,
								  backgroundColor: colors.red02.fill,
								  pointBackgroundColor: colors.red02.stroke,
								  borderColor: colors.red02.stroke,
								  pointHighlightStroke: colors.red02.stroke,
								  borderCapStyle: 'butt',
								  data: courseName02,
								}, {
								  label: "Course Name 01",
								  fill: true,
								  backgroundColor: colors.red01.fill,
								  pointBackgroundColor: colors.red01.stroke,
								  borderColor: colors.red01.stroke,
								  pointHighlightStroke: colors.red01.stroke,
								  data: courseName01,
								}]
							  },
							  options: {
								responsive: true,
								// Can't just just `stacked: true` like the docs say
								scales: {
								  xAxes: [{
									gridLines: {
										display: false
									},
								  }],
								  yAxes: [{
									stacked: true,
									gridLines: {
										display: false
									},
								  }]
								},
								animation: {
								  duration: 750,
								},
								legend: {
									display: true,
									position: 'bottom'
								},
								
							  }
							});
						</script>
                    </div>
                </div>
                <div class="chart-container">
                	<div class="chart-header">
                    	<h4 class="heading__h4">Percentage Completed by Enrolled</h4>
                        <div class="chart-select-course">
                            <select class="select-multi-course" multiple="multiple">                                
                                <option value="1">All Categories</option>
                                <option value="2">Languages</option>
                                <option value="3">Health &amp; Wellbeing</option>
                                <option value="5">Technology &amp; Design</option>                                
                                <option value="6">Option 6</option>
                                <option value="7">Option 7</option>
                                <option value="8">Option 8</option>
                            </select>
                        </div>
                    </div>
                    <div class="chart-details ">
                        <div class="chart-info">
                            <h4 class="heading__h4">COURSE NAME : XXXXXXXX</h4>
                            <p>Total Number of Employees Assigned : XX</p>
                            <div class="legend">
                                <ul>
                                    <li><span class="completed"></span> % of Course Completed</li>
                                    <li><span class="remaining"></span> % of Course Remaining</li>
                                </ul>
                            </div>
                        </div>
                        <div class="chart-canvas">
                        	<div class="value">60%</div>
                            <canvas id="courseCompleted" width="120" height="120"></canvas>
                            <script>
								// For a Outdoors doughnut chart
	
								var dataCourseCompleted = {
									datasets: [
										{
											labels: [ "Completed", "Remaining"],
											data: [60, 40],
											backgroundColor: [ "#ff4d4d", "#f1e5e5"],
											borderWidth: 3
										}]
								};
	
								var chartPercentage = document.getElementById("courseCompleted");
								var outdoorsChart = new Chart(chartPercentage, {
									type: 'doughnut',
									data: dataCourseCompleted,
									options: {
										legend: {
											display: false
										},
										rotation: 20,
										tooltips: {
											callbacks: {
												label: function(tooltipItem, data) {
													var dataset = data.datasets[tooltipItem.datasetIndex];
													var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
														return previousValue + currentValue;
													});
													var currentValue = dataset.data[tooltipItem.index];
													var precentage = Math.floor(((currentValue/total) * 100)+0.5);
													var current_label = dataset.labels[tooltipItem.index];
													return current_label + ':' + precentage + "%";
												}
											}
										}
									}
								});
							</script>
                    	</div>
                    </div>
                </div>
                <div class="chart-container">
                	<div class="chart-header">
                    	<h4 class="heading__h4">Percentage Completed by Enrolled</h4>
                        <div class="chart-select-course">
                            <select class="select-multi-course" multiple="multiple">                                
                                <option value="1">All Categories</option>
                                <option value="2">Languages</option>
                                <option value="3">Health &amp; Wellbeing</option>
                                <option value="5">Technology &amp; Design</option>                                
                                <option value="6">Option 6</option>
                                <option value="7">Option 7</option>
                                <option value="8">Option 8</option>
                            </select>
                        </div>
                    </div>
                    <div class="chart-details ">
                        <div class="chart-info">
                            <h4 class="heading__h4">COURSE NAME : XXXXXXXX</h4>
                            <p>Total Number of Employees Assigned : XX</p>
                            <div class="legend">
                                <ul>
                                    <li><span class="completed"></span> % of Course Completed</li>
                                    <li><span class="remaining"></span> % of Course Remaining</li>
                                </ul>
                            </div>
                        </div>
                        <div class="chart-canvas">
                        	<div class="value">40%</div>
                            <canvas id="courseCompleted2" width="120" height="120"></canvas>
                            <script>
								// For a Outdoors doughnut chart
								var data = {
									labels: [
										"Completed",
										"Remaining"
									],
									datasets: [
										{
											labels: [ "Completed", "Remaining"],
											data: [60, 40],
											backgroundColor: [ "#ff4d4d", "#f1e5e5"],
											borderWidth: 3
										}]
								};
								
								var ctx = document.getElementById("courseCompleted2");
								
								// And for a doughnut chart
								var myDoughnutChart = new Chart(ctx, {
									type: 'doughnut',
									data: data,
									options: {
										legend: {
											display: false
										},
										rotation: 1 * Math.PI,
									  circumference: 1 * Math.PI
									}
								});
							</script>
                    	</div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@stop

