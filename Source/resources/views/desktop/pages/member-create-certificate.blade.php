@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Certifications</h3>
                </div>
                <div class="right">
                	<a href="#" class="button tinysize small caps certificate-btn">
                    	<span class="icon-trophy"></span> Create new certificate
                    </a>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="member-breadcrumb">
               		<a href="{{ BASE_URL }}member-certifications">Certifications</a> 
                    <span class="divider"> > </span> 
                    Create New Certificate
                </div>
				<div class="content-white-box">
                	<h3 class="heading__h3">Create New Certificate</h3>
                    <div class="form-container">
                    	<div class="form-element">
                        	<label for="txtCourseName" class="title">Course Name</label>
                            <select name="" class="custom-select width60" id="txtCourseName">
                                <option>Type or search your course name here</option>
                            </select>
                        </div>
                        <div class="form-element">
                        	<label for="txtPromoVideos" class="title">Affiliation Logo<br>
								<span>(Optional)</span>
                            </label>
                            <div class="file-upload lightpink">
                                <input type="file" name="receipt" id="file-browse" class="inputfile" />
                                <label for="file-browse"><span class="input-text">Upload File</span> <strong>&nbsp;</strong></label>
                            </div>
                            <div class="left note">Note :  Supported format for uploading the file : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
                        </div>
                        <div class="form-element">
                        	<label for="rdoCertificateContent" class="title">Certificate Content<br>
								<span>(Choose Any One)</span>
                            </label>
                            <div class="radioGroup block">
                            	<div class="radio-item">
                                	<input name="rdoCertificateContent" id="rdoCertificateContent1" type="radio" class="radio-btn">
                                    <label for="rdoCertificateContent1" class="radio-label">GoGetterz proudly presents this Certificate of Completion to (name) for the dedication and hard work that resulted in the successful culmination of (course)</label>
                                </div>
                                <div class="radio-item">
                                	<input name="rdoCertificateContent" id="rdoCertificateContent2" type="radio" class="radio-btn">
                                    <label for="rdoCertificateContent2" class="radio-label">The faculty at GoGetterz are honored to present (name) with the official certification for completing (course). Your achievement will be remembered and your participation cherished.</label>
                                </div>
                                <div class="radio-item">
                                	<input name="rdoCertificateContent" id="rdoCertificateContent3" type="radio" class="radio-btn">
                                    <label for="rdoCertificateContent3" class="radio-label">This Certificate of Completion is awarded to (name) for the outstanding completion of (course) at GoGetterz</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="rdoCertificateType" class="title">Certificate Type</label>
                            <div class="radioGroup">
                            	<h6 class="heading__h6">Type 1</h6>
                            	<div class="radio-item">
                                	<input name="rdoCertificateType" id="rdoCertificateType11" type="radio" class="radio-btn">
                                    <label for="rdoCertificateType11" class="radio-label">Online Course</label>
                                </div>
                                <div class="radio-item">
                                	<input name="rdoCertificateType" id="rdoCertificateType12" type="radio" class="radio-btn">
                                    <label for="rdoCertificateType12" class="radio-label">Live Classroom</label>
                                </div>
                                <div class="radio-item">
                                	<input name="rdoCertificateType" id="rdoCertificateType13" type="radio" class="radio-btn">
                                    <label for="rdoCertificateType13" class="radio-label">Event</label>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-element">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="radioGroup right-container">
                            	<h6 class="heading__h6">Type 2</h6>
                            	<div class="radio-item">
                                	<input name="rdoCertificateType2" id="rdoCertificateType21" type="radio" class="radio-btn">
                                    <label for="rdoCertificateType21" class="radio-label">Completion</label>
                                </div>
                                <div class="radio-item">
                                	<input name="rdoCertificateType2" id="rdoCertificateType22" type="radio" class="radio-btn">
                                    <label for="rdoCertificateType22" class="radio-label">Test</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtExpertName" class="title">Expert Name</label>
                            <input name="" type="text" id="txtExpertName" placeholder="Type your name here" class="width40">
                        </div>
                        <div class="form-element">
                        	<label for="txtPromoVideos" class="title">Digital Signature<br>
								<span>(Optional)</span>
                            </label>
                            <div class="file-upload lightpink">
                                <input type="file" name="receipt" id="file-browse2" class="inputfile" />
                                <label for="file-browse2"><span class="input-text">Upload File</span> <strong>&nbsp;</strong></label>
                            </div>
                            <div class="left note">Note :  Supported format for uploading the file : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
                        </div>
                        <div class="form-element button-container">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="right-container">
                                <input type="submit" value="Submit" class="button lightpink tinysize mid">
                            </div>
                        </div>
                    </div>
                </div>                
            </section>
            
        </div>
    </div>
</div>
@stop

