@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Messages</h3>
                </div>
                <div class="right">
                	<section class="search-courses"><!--Css in _course.scss-->
                        <div class="form-container red">
                            <input type="text" placeholder="Search Messages">
                            <input type="submit" title="Submit" value="">
                        </div>
                    </section>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<h4 class="heading__h4">Subject : Name of the course query</h4>
                <div class="content-white-box margin-bot"> 
                	<div class="message-chat">
                    	<div class="row">
                        	<div class="col s2 label">From</div>
                            <div class="col s9 name">Student Username</div>
                            <div class="col s1 date">10 Jan</div>
                        </div>
                        <div class="row">
                        	<div class="col s2 label">Message</div>
                            <div class="col s9 text">Lobortis feugiat vivamus at augue eget arcu dictum. Fames ac turpis egestas sed tempus urna et. Facilisi cras fermentum odio eu. Quisque non tellus orci ac auctor augue mauris augue. Nullam eget felis eget nunc lobortis. Quis lectus nulla at volutpat diam ut venenatis tellus. At erat pellentesque adipiscing commodo elit at imperdiet. Consectetur adipiscing elit ut aliquam purus sit amet luctus. Sapien faucibus et molestie ac. Ac tortor dignissim convallis aenean et tortor. Malesuada pellentesque elit eget gravida cum. Amet commodo nulla facilisi nullam.</div>
                        </div>
                        <div class="delete">
                        	<a href="#" class="icon-delete"></a>
                        </div>
                    </div>
                    <div class="message-chat">
                    	<div class="row">
                        	<div class="col s2 label">From</div>
                            <div class="col s9 name">Student Username</div>
                            <div class="col s1 date">10 Jan</div>
                        </div>
                        <div class="row">
                        	<div class="col s2 label">Message</div>
                            <div class="col s9 text">Lobortis feugiat vivamus at augue eget arcu dictum. Fames ac turpis egestas sed tempus urna et. Facilisi cras fermentum odio eu. Quisque non tellus orci ac auctor augue mauris augue. Nullam eget felis eget nunc lobortis. Quis lectus nulla at volutpat diam ut venenatis tellus. At erat pellentesque adipiscing commodo elit at imperdiet. Consectetur adipiscing elit ut aliquam purus sit amet luctus. Sapien faucibus et molestie ac. Ac tortor dignissim convallis aenean et tortor. Malesuada pellentesque elit eget gravida cum. Amet commodo nulla facilisi nullam.</div>
                        </div>
                        <div class="delete">
                        	<a href="#" class="icon-delete"></a>
                        </div>
                    </div>
                    <div class="message-chat">
                    	<div class="row">
                        	<div class="col s2 label">From</div>
                            <div class="col s9 name">Student Username</div>
                            <div class="col s1 date">10 Jan</div>
                        </div>
                        <div class="row">
                        	<div class="col s2 label">Message</div>
                            <div class="col s9 text">Lobortis feugiat vivamus at augue eget arcu dictum. Fames ac turpis egestas sed tempus urna et. Facilisi cras fermentum odio eu. Quisque non tellus orci ac auctor augue mauris augue. Nullam eget felis eget nunc lobortis. Quis lectus nulla at volutpat diam ut venenatis tellus. At erat pellentesque adipiscing commodo elit at imperdiet. Consectetur adipiscing elit ut aliquam purus sit amet luctus. Sapien faucibus et molestie ac. Ac tortor dignissim convallis aenean et tortor. Malesuada pellentesque elit eget gravida cum. Amet commodo nulla facilisi nullam.</div>
                        </div>
                        <div class="delete">
                        	<a href="#" class="icon-delete"></a>
                        </div>
                    </div>
                </div>
				<div class="content-white-box">                	
                	<div class="form-container theme-grey">
                    	<div class="form-element">
                        	<label class="title">To</label>
                            <span class="name">Student Username</span>
                        </div>                        
                    	<div class="form-element">
                        	<label for="txtMessage" class="title">Message</label>
                            <textarea name="txtMessage" rows="5" id="txtMessage" class="width80"></textarea>
                        </div>
                        <div class="form-element button-container">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="right-container">
                                <input type="submit" value="Reply" class="button lightpink tinysize mid">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@stop

