@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Online course management</h3>
                </div>
                <div class="right">
                	<a href="#" class="button tinysize small caps certificate-btn">
                    	<span class="icon-delete white small"></span> Delete course
                    </a>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<h3 class="heading__h3 bg-grey">
                	<div class="steps-heading">
                    	<span>Step 01</span>
                        <span>Step 02</span>
                        <span>Step 03</span>
                        <span class="active">Step 04</span>
                    </div>
                </h3>
            	<div class="content-white-box no-top-border">
                	<div class="back-link">
                   		<a href="#">< Go Back</a> 
                    </div>
                	<h4 class="heading__h4">Complete Course Creation</h4>
                    <p class="steps-copy-text">Mauris cursus mattis molestie a iaculis at. Feugiat in ante metus dictum at. Nunc sed id semper risus in hendrerit. Facilisis gravida neque convallis a cras semper auctor neque. Mi proin sed libero enim sed faucibus turpis in. In hendrerit gravida rutrum quisque non tellus orci. Vitae suscipit tellus mauris a diam maecenas sed enim ut. Aenean pharetra magna ac placerat vestibulum lectus mauris. Sit amet aliquam id diam maecenas ultricies mi eget. Elit ullamcorper dignissim cras tincidunt lobortis. Fermentum odio eu feugiat pretium nibh. Lobortis scelerisque fermentum dui faucibus in ornare quam viverra. Elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus. Fringilla est ullamcorper eget nulla facilisi etiam dignissim diam. Sagittis eu volutpat odio facilisis mauris sit amet massa. Faucibus turpis in eu mi.Mauris cursus mattis molestie a iaculis at. </p>
                    <div class="form-container">
                        <div class="form-element button-container">
                        	<input type="submit" value="Save Changes" class="button lightpink tinysize small">
                        </div>
                    </div>
                </div>
                
            </section>            
        </div>
    </div>
</div>
@stop

