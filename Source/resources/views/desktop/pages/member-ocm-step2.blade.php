@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Online course management</h3>
                </div>
                <div class="right">
                	<a href="#" class="button tinysize small caps certificate-btn">
                    	<span class="icon-delete white small"></span> Delete course
                    </a>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<h3 class="heading__h3 bg-grey">
                	<div class="steps-heading">
                    	<span>Step 01</span>
                        <span class="active">Step 02</span>
                        <span>Step 03</span>
                        <span>Step 04</span>
                    </div>
                </h3>
            	<div class="content-white-box no-top-border">
                	<div class="back-link">
                   		<a href="#">< Go Back</a> 
                    </div>
                	<h4 class="heading__h4">Course Content Production</h4>
                    <div class="button-container">
                    	<a href="#" class="button grey-fill tinysize small">+ Add Chapter</a>
                        <a href="#" class="button grey tinysize small mLeft10">+ Add Lesson</a>
                    </div>
                    
                    <h4 class="heading__h4 bg-grey no-margin">Chapter 1</h4>
                    <div class="content-grey-box small-padding no-top-border">
                    	<div class="form-container theme-white">
                        	<div class="form-element">
                                <label for="txtCaption" class="title">Caption</label>
                                <input name="" type="text" class="width30" id="txtCaption" placeholder="">
                            </div>
                            <div class="form-element">
                                <label for="txtLesson1" class="title">Lesson 1</label>
                                <textarea name="" rows="4" id="txtLesson1">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</textarea>
                            </div>
                            <div class="form-element">
                                <div class="left-container">&nbsp;</div>
                                <div class="right-container">
                                    <a href="#add-teaching-material" class="button grey tinysize small modal-link80">+ Add Teaching Material</a>
                                </div>
                            </div>
                            <div class="form-element">
                                <div class="left-container">&nbsp;</div>
                                <div class="right-container">
                                    <a href="#" class="button grey-fill no-hover tinysize small mTop20">+ Add Lesson</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <h4 class="heading__h4 bg-grey no-margin">Chapter 2</h4>
                    <div class="content-grey-box small-padding no-top-border">
                    	<div class="form-container theme-white">
                        	<div class="form-element">
                                <label for="txtCaption2" class="title">Caption</label>
                                <input name="" type="text" class="width30" id="txtCaption2" placeholder="">
                            </div>
                            <div class="form-element">
                                <label for="txtLesson2" class="title">Lesson 2</label>
                                <textarea name="" rows="4" id="txtLesson2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</textarea>
                            </div>
                            <div class="form-element">
                                <div class="left-container">&nbsp;</div>
                                <div class="right-container">
                                    <a href="#add-teaching-material" class="button grey tinysize small modal-link80">+ Add Teaching Material</a>
                                </div>
                            </div>
                            <div class="form-element">
                                <div class="left-container">&nbsp;</div>
                                <div class="right-container">
                                    <a href="#" class="button grey-fill no-hover tinysize small mTop20">+ Add Lesson</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-container">
                        <div class="form-element button-container">
                            <div class="left-container">&nbsp;</div>
                            <div class="right-container">
                                <input type="submit" value="Continue to Step 3" class="button lightpink tinysize small">
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>            
        </div>
    </div>
</div>
<!--Modal code-->
<div class="hide">
	@include('desktop.modals.add-teaching-material')
</div>
@stop

