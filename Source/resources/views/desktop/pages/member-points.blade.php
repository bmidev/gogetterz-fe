@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<div class="left">
                    <h3 class="heading__h3">Points</h3>
                </div>
                <div class="right">
                	<section class="search-courses"><!--Css in _course.scss-->
                        <div class="form-container red">
                            <input type="text" placeholder="Search Messages">
                            <input type="submit" title="Submit" value="">
                        </div>
                    </section>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<h4 class="heading__h4">Total Points Remaining : 1200</h4>
				<div class="content-white-box">
                	<div class="notification-grid">
                    	<div class="title row">                        	
                            <div class="col m2 text-center">
                                <div class="bg-container">Date</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">Course Name</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">Earned</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">Spent</div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">1000</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">-</div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">-</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">500</div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">-</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">500</div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">200</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">-</div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">300</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">-</div>
                            </div>
                        </div>                        
                        <div class="data row">                        	
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">1000</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">-</div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">-</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">500</div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">-</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">500</div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">200</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">-</div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m2 text-center">
                                <div class="bg-container">10/02/2017</div>
                            </div>
                            <div class="col m6">
                                <div class="bg-container">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                </div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">300</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">-</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="note add-padding">
                    Points can be used to purchase courses or receive discounts
                </div>
            </section>
            
        </div>
    </div>
</div>
@stop

