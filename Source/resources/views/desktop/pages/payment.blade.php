@extends('desktop.layouts.master')
@section('content')

<section class="login-registration">
    <div class="m-page-container">
    	<div class="copy-container">
        	<h2 class="heading__h2">Choose Your<br> Mode of Payment</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        </div>
        <div class="process-container">
        	<div class="form-container payment-container">
            	<form>
                	<div class="payment-price">
                    	<div class="currency">
                        	<select class="custom-select">
                                <option>$ US DOLLAR</option>
                                <option>Indian Rupees</option>
                                <option>Japanese Yen</option>
                            </select>
                        </div>
                        <div class="cost">$999</div>
                    </div>
                	<div class="payment-method">
                    	<div class="accordion-group theme-grey">
                            <section class="accordion-group__accordion">
                                <div class="accordion-group__accordion-head">
                                    <h3 class="accordion-group__accordion-heading">
                                        <button type="button" class="accordion-group__accordion-btn">Credit card</button>
                                    </h3>
                                </div>
                                <div class="accordion-group__accordion-panel">
                                    <div class="accordion-group__accordion-content">                        
                                        <div class="form-element">
                                        	<input name="txtCardholderName" type="text" id="txtCardholderName" placeholder="Cardholder Name">
                                        </div>
                                        <div class="form-element">
                                        	<input name="txtCardNumber" type="text" id="txtCardNumber" placeholder="Card Number">
                                        </div>
                                        <div class="clear">
                                            <div class="form-element form-expire-date">
                                                <label for="" class="title">Expires On :</label>
                                                <input name="txtMM" type="text" id="txtMM" placeholder="MM" maxlength="2">
                                                <input name="txtYY" type="text" id="txtYY" placeholder="YY" maxlength="2">
                                            </div>
                                            <div class="form-element form-cvv">
                                                <label for="" class="title">CVV :</label>
                                                <input name="txtCVV" type="text" id="txtCVV" maxlength="3">
                                                <a href="#" class="icon-info"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="accordion-group__accordion">
                                <div class="accordion-group__accordion-head">
                                    <h3 class="accordion-group__accordion-heading">
                                        <button type="button" class="accordion-group__accordion-btn">Debit card</button>
                                    </h3>
                                </div>
                                <div class="accordion-group__accordion-panel">
                                    <div class="accordion-group__accordion-content">                        
                                        <select class="custom-select">
                                            <option>Select Debit card</option>
                                        </select>
                                    </div>
                                </div>
                            </section>
                            <section class="accordion-group__accordion">
                                <div class="accordion-group__accordion-head">
                                    <h3 class="accordion-group__accordion-heading">
                                        <button type="button" class="accordion-group__accordion-btn">Netbanking</button>
                                    </h3>
                                </div>
                                <div class="accordion-group__accordion-panel">
                                    <div class="accordion-group__accordion-content">                        
                                        <select class="custom-select">
                                            <option>Select Netbanking</option>
                                        </select>
                                    </div>
                                </div>
                            </section>                            
                        </div>
                    </div>
                    <div class="or">OR Pay Via</div>
                    <div class="payment-via-others center">
                    	<a href="#" class="icon-amazon-pay"></a>
                        <a href="#" class="icon-line"></a>
                    </div>
                    
                    <div class="form-element">
                        <input name="btnNext" type="submit" id="btnNext" value="Complete Registration" class="button lightpink midsize full-width">
                    </div>                    
                </form>
            </div>
            
        </div>
    </div>
</section> 

@stop

