@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<h3 class="heading__h3">Online course management</h3>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="member-breadcrumb">
               		<a href="{{ BASE_URL }}member-online-course-management">Online Course Management</a> 
                    <span class="divider"> > </span> 
                    Create New Coupon
                </div>
            	<div class="content-white-box">
                	<h3 class="heading__h3">Create a New Coupon</h3>
                    <div class="form-container">
                    	<div class="form-element">
                        	<label for="txtCouponTitle" class="title">Title</label>
                            <input name="" type="text" class="width40" id="txtCouponTitle" placeholder="Coupon Title">
                        </div>
                        <div class="form-element">
                        	<label for="txtCouponCode" class="title">Coupon Code</label>
                            <input type="text" class="width40" id="txtCouponCode" placeholder="Type a unique code here">
                            <div class="clear">
                            	<div class="left-container">&nbsp;</div>
                                <div class="right-container width40">Coupon Code Format : Supported format for uploading the file : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</div>
                            </div>
                            
                        </div>
                        <div class="form-element">
                        	<label for="txtDiscount" class="title">Discount</label>
                            <input type="text" class="width30" id="txtDiscount" value="10%">
                        </div>
                        <div class="form-element">
                        	<label for="txtExpiryDate" class="title">Expiry Date</label>
                            <input name="" type="date" id="txtExpiryDate" class="width30">
                        </div>
                        
                        <div class="form-element">
                        	<label for="lstApplicableOn" class="title">Applicable on</label>
                            <select name="" class="custom-select" id="lstApplicableOn">
                                <option>Select Courses</option>
                            </select>
                        </div>
                        <div class="form-element">
                        	<label for="txtConditions" class="title">Conditions</label>
                            <textarea name="" rows="4" id="txtConditions">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</textarea>
                        </div>
                        <div class="form-element button-container">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="right-container">
                                <input type="submit" value="Cancel" class="button black tinysize mid">
                                <input type="submit" value="Upload Coupon" class="button lightpink tinysize mid mLeft20">
                            </div>
                        </div>
                    </div>
                </div>
            </section>            
        </div>
    </div>
</div>
@stop

