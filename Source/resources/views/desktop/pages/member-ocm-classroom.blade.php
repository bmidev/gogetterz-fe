@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<h3 class="heading__h3">Online course management</h3>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="member-breadcrumb">
               		<a href="{{ BASE_URL }}member-online-course-management">Online Course Management</a> 
                    <span class="divider"> > </span> 
                    Classroom
                </div>
				<div class="content-white-box">
                	<h4 class="heading__h4"><span class="icon-classroom"></span> Classroom | Name of the Course</h4>
                	<div class="notification-grid">
                        <div class="title small-size row">   
                            <div class="col m1 text-center">
                                <div class="bg-container">S.No</div>
                            </div>                     	
                            <div class="col m5 text-center">
                                <div class="bg-container">Username</div>
                            </div>
                            <div class="col m4 text-center">
                                <div class="bg-container">Progress</div>
                            </div>
                            <div class="col m2 text-center">
                                <div class="bg-container">Connect</div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">01</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container white">Janedoe12234 <span class="icon-query right-icon"></span></div>
                            </div>
                            <div class="col m4 text-center">
                                <div class="bg-container">
                                	<div class="status-calculator no-padding">
                                        <div class="status-bar">
                                            <span class="status-bar__value" style="width:50%"></span>                                        
                                        </div>
                                        <div class="status-percentage">50</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                            	<div class="bg-container">
                                	<a href="#" class="icon-connect"></a>
                                </div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">02</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container white">Janedoe12234 <span class="icon-query right-icon"></span></div>
                            </div>
                            <div class="col m4 text-center">
                                <div class="bg-container">
                                	<div class="status-calculator no-padding">
                                        <div class="status-bar">
                                            <span class="status-bar__value" style="width:50%"></span>                                        
                                        </div>
                                        <div class="status-percentage">50</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                            	<div class="bg-container">
                                	<a href="#" class="icon-connect"></a>
                                </div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">03</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container white">Janedoe12234 <span class="icon-query right-icon"></span></div>
                            </div>
                            <div class="col m4 text-center">
                                <div class="bg-container">
                                	<div class="status-calculator no-padding">
                                        <div class="status-bar">
                                            <span class="status-bar__value" style="width:50%"></span>                                        
                                        </div>
                                        <div class="status-percentage">50</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                            	<div class="bg-container">
                                	<a href="#" class="icon-connect"></a>
                                </div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">04</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container white">Janedoe12234 <span class="icon-query right-icon"></span></div>
                            </div>
                            <div class="col m4 text-center">
                                <div class="bg-container">
                                	<div class="status-calculator no-padding">
                                        <div class="status-bar">
                                            <span class="status-bar__value" style="width:50%"></span>                                        
                                        </div>
                                        <div class="status-percentage">50</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                            	<div class="bg-container">
                                	<a href="#" class="icon-connect"></a>
                                </div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">05</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container white">Janedoe12234 <span class="icon-query right-icon"></span></div>
                            </div>
                            <div class="col m4 text-center">
                                <div class="bg-container">
                                	<div class="status-calculator no-padding">
                                        <div class="status-bar">
                                            <span class="status-bar__value" style="width:50%"></span>                                        
                                        </div>
                                        <div class="status-percentage">50</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                            	<div class="bg-container">
                                	<a href="#" class="icon-connect"></a>
                                </div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">06</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container white">Janedoe12234 <span class="icon-query right-icon"></span></div>
                            </div>
                            <div class="col m4 text-center">
                                <div class="bg-container">
                                	<div class="status-calculator no-padding">
                                        <div class="status-bar">
                                            <span class="status-bar__value" style="width:50%"></span>                                        
                                        </div>
                                        <div class="status-percentage">50</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                            	<div class="bg-container">
                                	<a href="#" class="icon-connect"></a>
                                </div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">07</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container white">Janedoe12234 <span class="icon-query right-icon"></span></div>
                            </div>
                            <div class="col m4 text-center">
                                <div class="bg-container">
                                	<div class="status-calculator no-padding">
                                        <div class="status-bar">
                                            <span class="status-bar__value" style="width:50%"></span>                                        
                                        </div>
                                        <div class="status-percentage">50</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                            	<div class="bg-container">
                                	<a href="#" class="icon-connect"></a>
                                </div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">08</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container white">Janedoe12234 <span class="icon-query right-icon"></span></div>
                            </div>
                            <div class="col m4 text-center">
                                <div class="bg-container">
                                	<div class="status-calculator no-padding">
                                        <div class="status-bar">
                                            <span class="status-bar__value" style="width:50%"></span>                                        
                                        </div>
                                        <div class="status-percentage">50</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                            	<div class="bg-container">
                                	<a href="#" class="icon-connect"></a>
                                </div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">09</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container white">Janedoe12234 <span class="icon-query right-icon"></span></div>
                            </div>
                            <div class="col m4 text-center">
                                <div class="bg-container">
                                	<div class="status-calculator no-padding">
                                        <div class="status-bar">
                                            <span class="status-bar__value" style="width:50%"></span>                                        
                                        </div>
                                        <div class="status-percentage">50</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                            	<div class="bg-container">
                                	<a href="#" class="icon-connect"></a>
                                </div>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">10</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container white">Janedoe12234 <span class="icon-query right-icon"></span></div>
                            </div>
                            <div class="col m4 text-center">
                                <div class="bg-container">
                                	<div class="status-calculator no-padding">
                                        <div class="status-bar">
                                            <span class="status-bar__value" style="width:50%"></span>                                        
                                        </div>
                                        <div class="status-percentage">50</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m2 text-center">
                            	<div class="bg-container">
                                	<a href="#" class="icon-connect"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
        </div>
    </div>
</div>
@stop

