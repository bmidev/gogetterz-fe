@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.employee-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<h3 class="heading__h3">Profile settings</h3>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="content-white-box">
                	<h3 class="heading__h3">Personal Details</h3>
                    <div class="form-container">
                    	<div class="form-element">
                        	<label for="txtName" class="title">Name</label>
                            <input name="txtName" type="text" id="txtName" class="width40">
                        </div>
                        <div class="form-element">
                        	<label for="txtEmail" class="title">Email Id</label>
                            <input name="txtEmail" type="text" id="txtEmail" class="width40">
                        </div>
                        <div class="form-element">
                        	<label for="txtBirthday" class="title">Birthday</label>
                            <input name="txtBirthday" type="date" id="txtBirthday" class="width30">
                        </div>
                        <div class="form-element">
                        	<label for="txtCompanyName	" class="title">Gender*</label>
                            <div class="radioGroup">
                            	<div class="radio-item">
                                	<input name="rdoGender" id="rdoGender1" type="radio" class="radio-btn">
                                    <label for="rdoGender1" class="radio-label">Male</label>
                                </div>
                                <div class="radio-item">
                                	<input name="rdoGender" id="rdoGender2" type="radio" class="radio-btn">
                                    <label for="rdoGender2" class="radio-label">Female</label>
                                </div>
                                <div class="radio-item">
                                	<input name="rdoGender" id="rdoGender3" type="radio" class="radio-btn">
                                    <label for="rdoGender3" class="radio-label">Others</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtAddress1" class="title">Address</label>
                            <input name="txtAddress1" type="text" id="txtAddress1" placeholder="Address Line 1">
                        </div>
                        <div class="form-element">
                            <label for="txtAddress2" class="title"></label>
                            <input name="txtAddress2" type="text" id="txtAddress2" placeholder="Address Line 2">
                        </div>
                        <div class="form-element">
                        	<label for="lstCountry" class="title">Country</label>
                            <select name="lstCountry" class="custom-select" id="lstCountry">
                                <option>Japan</option>
                            </select>
                        </div>
                        <div class="form-element">
                        	<label for="txtIdentification" class="title">Identification</label>
                            <div class="file-upload lightpink">
                                <input type="file" name="receipt" id="file-browse" class="inputfile" />
                                <label for="file-browse"><span class="input-text">Upload File</span> <strong>&nbsp;</strong></label>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtCompanyName" class="title">Company</label>
                            <input name="txtCompanyName" type="text" id="txtCompanyName" placeholder="">
                        </div>                        
                        <div class="form-element button-container">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="right-container text-right">
                                <input type="submit" value="Save Changes" class="button lightpink tinysize mid mLeft20">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
        </div>
    </div>
</div>
@stop

