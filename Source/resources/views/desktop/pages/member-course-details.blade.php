@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading clear">
            	<div class="left">
            	<nav class="member-heading-nav"> 
                    <!-- Sample menu definition -->
                    <ul id="main-menu" class="sm sm-simple">
                        <li><a href="#">Courses &amp; Catalogue</a>
                            <ul>
                                <li><a href="#">Languages</a></li>
                                <li><a href="#">Health &amp; wellbeing</a></li>
                                <li><a href="#">Technology &amp; design</a></li>
                                <li><a href="#">Cool japan</a>
                                    <ul>
                                        <li><a href="#">Sub-Category of the Course</a></li>
                                        <li><a href="#">Sub-Category of the Course</a></li>
                                        <li><a href="#">Sub-Category of the Course</a></li>
                                        <li><a href="#">Sub-Category of the Course</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>                                                
                    </ul>
                </nav>
                </div>
                <div class="right">
                	<section class="search-courses"><!--Css in _course.scss-->
                        <div class="form-container red">
                            <input type="text" placeholder="Search Courses/Subjects">
                            <input type="submit" title="Submit" value="">
                        </div>
                    </section>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="back-link">
               		<a href="#"> < BACK</a> 
                </div>
                <div class="course-info">
					<div class="course-info-header">
                        <h2 class="heading__h2">English Email Tips
                            <span class="rating">
                                <span class="star"></span> 4.5
                            </span>
                        </h2>
                        <div class="other-links">
                            <a href="#" class="links favourites"><i class="icon"></i> Add to Favourites</a>
                            <a href="#" class="links share-courses"><i class="icon"></i> Share Course</a>
                            <span class="links views"><i class="icon"></i> 10000 Views</span>
                        </div>
                    </div>
                    <div class="course-content">
                        <div class="course-content-image"> 
                            <a href="#view-lesson" class="modal-link">
                                <span class="play"></span>
                                <img src="{{ asset('desktop/images/jp/img-course-details.jpg') }}" alt=""/>
                            </a>
                        </div>
                        <div class="course-text">                            
                            <p>Ornare quam viverra orci sagittis eu volutpat. Sed sed risus pretium quam vulputate dignissim suspendisse in est. Sed id semper risus in hendrerit gravida rutrum quisque non. Bibendum neque egestas congue quisque egestas diam in arcu cursus. </p>
                            <p>Dignissim suspendisse in est ante in nibh. Facilisi morbi tempus iaculis urna id volutpat lacus laoreet. Eget mauris pharetra et ultrices neque ornare aenean euismod. Arcu odio ut sem nulla pharetra diam. </p>
                            <div class="button-container">
                                <a href="#" class="button grey midsize small">Enroll Now</a> 
                                <a href="#" class="button lightpink midsize small mLeft10">Watch Video</a>
                            </div>
                        </div>
                    </div>        
                </div>
                <div class="course-summary">
                    <div class="content-white-box">
                        <h2 class="heading__h2">Course Summary</h2>
                        <div class="copy">
                            <p>uspendisse potenti nullam ac tortor vitae purus. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat. Varius sit amet mattis vulputate enim nulla aliquet porttitor lacus. Varius vel pharetra vel turpis nunc eget lorem dolor. Risus nec feugiat in fermentum posuere. Sed turpis tincidunt id aliquet risus feugiat in ante. Purus ut faucibus pulvinar elementum integer enim. Sed libero enim sed faucibus. Mauris augue neque gravida in fermentum et. Mattis nunc sed blandit libero volutpat sed cras ornare arcu. Augue neque gravida in fermentum et sollicitudin ac orci. Non curabitur gravida arcu ac tortor. Id nibh tortor id aliquet lectus proin nibh. Mi eget mauris pharetra et. Ac turpis egestas sed tempus urna et pharetra. Varius sit amet mattis vulputate.</p>
                            <p>Lectus proin nibh nisl condimentum id. Mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare massa. Adipiscing enim eu turpis egestas pretium aenean pharetra magna ac. Platea dictumst vestibulum rhoncus est pellentesque. Vitae ultricies leo integer malesuada nunc vel risus commodo. Suspendisse potenti nullam ac tortor. Sit amet massa vitae tortor condimentum lacinia quis vel. Tempor orci dapibus ultrices in iaculis. Amet luctus venenatis lectus magna fringilla urna. Pellentesque sit amet porttitor eget dolor morbi non. Sagittis purus sit amet volutpat consequat. Faucibus vitae aliquet nec ullamcorper sit amet </p>
                        </div>
                        <h3 class="heading__h3">Requirements</h3>
                        <div class="copy">            
                            <div class="listing">
                                <ul>
                                    <li> Suspendisse potenti nullam ac tortor vitae purus.</li>
                                    <li>Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat.</li>
                                    <li>Varius sit amet mattis vulputate enim nulla aliquet porttitor lacus.</li>
                                    <li>Varius vel pharetra vel turpis nunc eget lorem dolor.</li>
                                    <li>Risus nec feugiat in fermentum posuere. Sed turpis tincidunt id aliquet risus feugiat in ante.</li>
                                    </ul>
                                </div>
                        </div>
                        <h3 class="heading__h3">Target Audience</h3>
                        <div class="copy">
                            <p>Lectus proin nibh nisl condimentum id. Mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare massa. Adipiscing enim eu turpis egestas pretium aenean pharetra magna ac. Platea dictumst vestibulum rhoncus est pellentesque. Vitae ultricies leo integer malesuada nunc vel risus commodo. Suspendisse potenti nullam ac tortor. Sit amet massa vitae tortor condimentum lacinia quis vel. Tempor orci dapibus ultrices in iaculis. Amet luctus venenatis lectus magna fringilla urna. Pellentesque sit amet porttitor eget dolor morbi non. Sagittis purus sit amet volutpat consequat. Faucibus vitae aliquet nec ullamcorper sit amet risus. Vitae semper quis lectus nulla at volutpat. Pellentesque id nibh tortor id aliquet lectus. Orci nulla pellentesque dignissim enim sit. </p>
                        </div>
                    </div>
            	</div>
                <div class="what-i-learn">
                    <div class="info-copy">
                        <h2 class="heading__h2">What Will I Learn</h2>
                    </div>
                    <div class="info-image"> <img src="{{ asset('desktop/images/jp/img-what-will-i-learn.svg') }}" alt=""/> </div>
                    <div class="info-copy">
                        <div class="listing">
                            <ul>
                                <li>Lectus proin nibh nisl condimentum id. Mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare massa. Adipiscing enim eu turpis egestas pretium aenean pharetra magna ac. Platea dictumst vestibulum rhoncus est pellentesque. </li>
                                <li>Vitae ultricies leo integer malesuada nunc vel risus commodo. Suspendisse potenti nullam ac tortor. Sit amet massa vitae tortor condimentum lacinia quis vel. Tempor orci dapibus ultrices 		in iaculis. Amet luctus venenatis lectus magna fringilla urna.</li>
                                <li>Pellentesque sit amet porttitor eget dolor morbi non. Sagittis purus sit amet volutpat consequat. Faucibus vitae aliquet nec ullamcorper sit amet risus. Vitae semper quis lectus nulla at volutpat. Pellentesque id nibh tortor id aliquet lectus. </li>
                                <li>Orci nulla pellentesque dignissim enim sit. Enim ut sem viverra aliquet eget sit. Cursus eget nunc scelerisque viverra mauris in. Mattis nunc sed blandit libero volutpat sed cras. Morbi tincidunt ornare massa eget egestas purus viverra.</li>
                                <li>Lectus proin nibh nisl condimentum id. Mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare massa. Adipiscing enim eu turpis egestas pretium aenean pharetra magna ac. Platea dictumst vestibulum rhoncus est pellentesque. </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="course-curriculum-accordion">
                    <h2 class="heading__h2">Course Curriculum</h2>
                    <div class="accordion-group theme-white">
                        <section class="accordion-group__accordion">
                            <div class="accordion-group__accordion-head">
                                <h3 class="accordion-group__accordion-heading">
                                    <button type="button" class="accordion-group__accordion-btn">Chapter 1 </button>
                                </h3>
                            </div>
                            <div class="accordion-group__accordion-panel">
                                <div class="accordion-group__accordion-content">
                                    <div class="lessons">
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 1</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 2</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 3</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 4</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="accordion-group__accordion">
                            <div class="accordion-group__accordion-head">
                                <h3 class="accordion-group__accordion-heading">
                                    <button type="button" class="accordion-group__accordion-btn">Chapter 2 </button>
                                </h3>
                            </div>
                            <div class="accordion-group__accordion-panel">
                                <div class="accordion-group__accordion-content">
                                    <div class="lessons">
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 1</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 2</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 3</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 4</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="accordion-group__accordion">
                            <div class="accordion-group__accordion-head">
                                <h3 class="accordion-group__accordion-heading">
                                    <button type="button" class="accordion-group__accordion-btn">Chapter 3 </button>
                                </h3>
                            </div>
                            <div class="accordion-group__accordion-panel">
                                <div class="accordion-group__accordion-content">
                                    <div class="pay-chapter"> <a href="#" class="button lightpink tinysize small caps mRight10">Pay For the Chapter ($100)</a> <span>OR</span> <a href="#" class="button lightpink tinysize small caps mLeft10">Pay For the Course ($500)</a> </div>
                                    <div class="lessons disabled">
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 1</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 2</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 3</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 4</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="accordion-group__accordion">
                            <div class="accordion-group__accordion-head">
                                <h3 class="accordion-group__accordion-heading">
                                    <button type="button" class="accordion-group__accordion-btn">Chapter 4 </button>
                                </h3>
                            </div>
                            <div class="accordion-group__accordion-panel">
                                <div class="accordion-group__accordion-content">
                                    <div class="pay-chapter"> <a href="#" class="button lightpink tinysize small caps mRight10">Pay For the Chapter ($150)</a> <span>OR</span> <a href="#" class="button lightpink tinysize small caps mLeft10">Pay For the Course ($500)</a> </div>
                                    <div class="lessons disabled">
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 1</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 2</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 3</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 4</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="accordion-group__accordion">
                            <div class="accordion-group__accordion-head">
                                <h3 class="accordion-group__accordion-heading">
                                    <button type="button" class="accordion-group__accordion-btn">Chapter 5 </button>
                                </h3>
                            </div>
                            <div class="accordion-group__accordion-panel">
                                <div class="accordion-group__accordion-content">
                                    <div class="pay-chapter"> <a href="#" class="button lightpink tinysize small caps mRight10">Pay For the Chapter ($250)</a> <span>OR</span> <a href="#" class="button lightpink tinysize small caps mLeft10">Pay For the Course ($500)</a> </div>
                                    <div class="lessons disabled">
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 1</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 2</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 3</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                        <div class="lesson-info">
                                            <h4 class="heading__h4">Lesson 4</h4>
                                            <p>Lectus proin nibh nisl. Mattis ullam corper velit sed morbi tinciduw ornare massa. Adipiscing eu turpis egestas pretium aenean pharetra magna ac. </p>
                                            <div class="button-container"> <a href="#" class="button grey-fill full-width tinysize text-center">Get Started</a> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="expert-container">
                    <div class="content-white-box">
                        <div class="about-expert no-margin">
                            <div class="info-image">
                                <figure> <span class="expert-img" style="background-image:url('{{ asset('desktop/images/jp/img-about-expert.jpg') }}');"></span>
                                    <figcaption>
                                        <div class="social-links"> <a href="#" class="ig"></a> <a href="#" class="tw"></a> <a href="#" class="fb"></a> </div>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="info-copy">
                                <h2 class="heading__h2">About the Expert - (Expert Name)</h2>
                                <p>Suspendisse potenti nullam ac tortor vitae purus. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat. Varius sit amet mattis vulputate enim nulla aliquet porttitor lacus. Varius vel pharetra vel turpis nunc eget lorem dolor. Risus nec feugiat in fermentum posuere. Sed turpis tincidunt id aliquet risus feugiat in ante. Purus ut faucibus pulvinar elementum integer enim. Sed libero enim sed faucibus. Mauris augue neque gravida in fermentum et. Mattis nunc sed blandit libero volutpat sed cras ornare arcu. </p>
                                <div class="button-container"> <a href="#" class="button grey smallsize small">View Upcoming Events</a> </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="user-reviews">
                    <div class="content-white-box">
                        <h2 class="heading__h2 clear">User Reviews</h2>
                        <div class="review_container">
                            <div class="review lightgrey clear">
                                <div class="review-img" style="background-image: url({{ asset('desktop/images/client/review-user.jpg') }})"></div>
                                <div class="review-copy">
                                    <div class="star-container">
                                        <ul>
                                            <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                            <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                            <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                            <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                        </ul>
                                    </div>
                                    <h3 class="reviewer_name">Username</h3>
                                    <p class="review_comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                </div>
                            </div>
                            <div class="review lightgrey clear">
                                <div class="review-img" style="background-image: url({{ asset('desktop/images/client/review-user.jpg') }})"></div>
                                <div class="review-copy">
                                    <div class="star-container">
                                        <ul>
                                            <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                            <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                            <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                            <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                        </ul>
                                    </div>
                                    <h3 class="reviewer_name">Username</h3>
                                    <p class="review_comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                </div>
                            </div>
                            <div class="review lightgrey clear">
                                <div class="review-img" style="background-image: url({{ asset('desktop/images/client/review-user.jpg') }})"></div>
                                <div class="review-copy">
                                    <div class="star-container">
                                        <ul>
                                            <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                            <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                            <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                            <li><img src="{{ asset('desktop/images/client/review-star.svg') }}" ></li>
                                        </ul>
                                    </div>
                                    <h3 class="reviewer_name">Username</h3>
                                    <p class="review_comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                </div>
                            </div>
                            
                        </div>
                        <div class="review-load-more"> <a href="#">Load More <img src="{{ asset('desktop/images/client/down-arrow.svg') }}" /> </a> </div>
                    </div>
                </div>
            </section>
            
        </div>
    </div>
</div>

@stop

