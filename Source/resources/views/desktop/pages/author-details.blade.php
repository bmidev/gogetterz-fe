@extends('desktop.layouts.master')
@section('content')
    <div class="client-bg">
        <section class="expert-container author-details">
            <div class="m-page-container">
                <div class="about-expert">
                    <div class="info-image">
                        <figure>
                            <span class="expert-img" style="background-image:url('{{ asset('desktop/images/jp/img-about-expert.jpg') }}');"></span>
                            <figcaption>
                                <div class="social-links">
                                    <a href="#" class="ig"></a>
                                    <a href="#" class="tw"></a>
                                    <a href="#" class="fb"></a>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="info-copy">
                        <h2 class="heading__h2">About the Expert - (Expert Name)</h2>
                        <p>Suspendisse potenti nullam ac tortor vitae purus. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat. Varius sit amet mattis vulputate enim nulla aliquet porttitor lacus. Varius vel pharetra vel turpis nunc eget lorem dolor. Risus nec feugiat in fermentum posuere. Sed turpis tincidunt id aliquet risus feugiat in ante. Purus ut faucibus pulvinar elementum integer enim. Sed libero enim sed faucibus. Mauris augue neque gravida in fermentum et. Mattis nunc sed blandit libero volutpat sed cras ornare arcu. </p>
                        <div class="button-container">
                            <a href="#" class="button grey smallsize small">View Upcoming Events</a>
                            <a href="#expert-video-modal" class="button grey-fill smallsize small mLeft10 modal-link">View Video</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="course-detais-view author-detais-view">
            <div class="m-page-container">
                <div class="course-header-links">
                    <div class="sort-by">
                        <select class="custom-select">
                            <option>SORT BY</option>
                            <option>Alphabetically A-Z</option>
                            <option>Free Courses</option>
                            <option>Most Popular</option>
                            <option>Most Relevant</option>
                        </select>
                    </div>
                    <div class="select-lang">
                        <select class="custom-select select-author-content">
                            <option value="author-courses">Courses</option>
                            <option value="author-quick-lessons">Quick lessons</option>
                        </select>
                    </div>
                    {{--<div class="filter">
                        <a href="#" class="button grey-fill midsize mid caps">filter</a>
                    </div>--}}
                </div>
                <div id="author-courses" class="course-articles author-article" >
                    <div class="article-wrapper">
                        <article class="course">
                            <figure>
                                <span class="overlay">Free</span>
                                <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                            </figure>
                            <div class="article-info">
                                <h4><a href="#">English For Beginners</a></h4>
                                <div class="author"><a href="#">Arighna Sarkar</a></div>
                                <div class="footer-container">
                                    <button class="favorite"></button>
                                    <div class="rating">
                                        New
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <figure>
                                <span class="overlay">Free</span>
                                <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                            </figure>
                            <div class="article-info">
                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                <div class="author"><a href="#">Arighna Sarkar</a></div>
                                <div class="footer-container">
                                    <button class="favorite"></button>
                                    <div class="rating">
                                        <span class="star"></span> 4.5
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <figure>
                                <span class="overlay">Free</span>
                                <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                            </figure>
                            <div class="article-info">
                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                <div class="author"><a href="#">Arighna Sarkar</a></div>
                                <div class="footer-container">
                                    <button class="favorite"></button>
                                    <div class="rating">
                                        New
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <figure>
                                <span class="overlay">¥500</span>
                                <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                            </figure>
                            <div class="article-info">
                                <h4><a href="#">English For Beginners</a></h4>
                                <div class="author"><a href="#">Arighna Sarkar</a></div>
                                <div class="footer-container">
                                    <button class="favorite"></button>
                                    <div class="rating">
                                        New
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <figure>
                                <span class="overlay">¥500</span>
                                <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                            </figure>
                            <div class="article-info">
                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                <div class="author"><a href="#">Arighna Sarkar</a></div>
                                <div class="footer-container">
                                    <button class="favorite"></button>
                                    <div class="rating">
                                        <span class="star"></span> 4.5
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <figure>
                                <span class="overlay">¥500</span>
                                <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                            </figure>
                            <div class="article-info">
                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                <div class="author"><a href="#">Arighna Sarkar</a></div>
                                <div class="footer-container">
                                    <button class="favorite"></button>
                                    <div class="rating">
                                        New
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <figure>
                                <span class="overlay">¥500</span>
                                <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                            </figure>
                            <div class="article-info">
                                <h4><a href="#">English For Beginners</a></h4>
                                <div class="author"><a href="#">Arighna Sarkar</a></div>
                                <div class="footer-container">
                                    <button class="favorite"></button>
                                    <div class="rating">
                                        New
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <figure>
                                <span class="overlay">¥500</span>
                                <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                            </figure>
                            <div class="article-info">
                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                <div class="author"><a href="#">Arighna Sarkar</a></div>
                                <div class="footer-container">
                                    <button class="favorite"></button>
                                    <div class="rating">
                                        <span class="star"></span> 4.5
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <figure>
                                <span class="overlay">¥500</span>
                                <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                            </figure>
                            <div class="article-info">
                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                                <div class="author"><a href="#">Arighna Sarkar</a></div>
                                <div class="footer-container">
                                    <button class="favorite"></button>
                                    <div class="rating">
                                        New
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
                <div id="author-quick-lessons" class="course-articles author-article" style="display: none">
                    <div class="article-wrapper">
                        <article class="course">
                            <a href="#quick-lesson-modal" class="quick-lesson-video">
                                <figure>
                                    <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-01.jpg') }})"></span>
                                </figure>
                            </a>
                            <div class="article-info">
                                <h4><a href="#">English For Beginners</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <a href="#quick-lesson-modal" class="quick-lesson-video">
                                <figure>
                                    <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-02.jpg') }})"></span>
                                </figure>
                            </a>
                            <div class="article-info">
                                <h4><a href="#">English Email Tips</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <a href="#quick-lesson-modal" class="quick-lesson-video">
                                <figure>
                                    <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-03.jpg') }})"></span>
                                </figure>
                            </a>
                            <div class="article-info">
                                <h4><a href="#">Spoken English</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <a href="#quick-lesson-modal" class="quick-lesson-video">
                                <figure>
                                    <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-01.jpg') }})"></span>
                                </figure>
                            </a>
                            <div class="article-info">
                                <h4><a href="#">English For Beginners</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <a href="#quick-lesson-modal" class="quick-lesson-video">
                                <figure>
                                    <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-02.jpg') }})"></span>
                                </figure>
                            </a>
                            <div class="article-info">
                                <h4><a href="#">English Email Tips</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <a href="#quick-lesson-modal" class="quick-lesson-video">
                                <figure>
                                    <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-03.jpg') }})"></span>
                                </figure>
                            </a>
                            <div class="article-info">
                                <h4><a href="#">Spoken English</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <a href="#quick-lesson-modal" class="quick-lesson-video">
                                <figure>
                                    <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-01.jpg') }})"></span>
                                </figure>
                            </a>
                            <div class="article-info">
                                <h4><a href="#">English For Beginners</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <a href="#quick-lesson-modal" class="quick-lesson-video">
                                <figure>
                                    <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-02.jpg') }})"></span>
                                </figure>
                            </a>
                            <div class="article-info">
                                <h4><a href="#">English Email Tips</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="article-wrapper">
                        <article class="course">
                            <a href="#quick-lesson-modal" class="quick-lesson-video">
                                <figure>
                                    <span class="img-name" style="background-image:url({{ asset('desktop/images/quick-lesson/quick-lesson-03.jpg') }})"></span>
                                </figure>
                            </a>
                            <div class="article-info">
                                <h4><a href="#">Spoken English</a></h4>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
            <!--Modal code-->
            <div class="hide">
                @include('desktop.modals.quick-lesson-video')
                @include('desktop.modals.expert-video')
            </div>
        </section>
    </div>
@stop