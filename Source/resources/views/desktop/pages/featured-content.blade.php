@extends('desktop.layouts.master')
@section('content')
    <div class="featured-bg">
        <section class="featured-banner">
            <div class="m-page-container">
                <div class="featured-banner-container">
                    <div class="featured-banner-image" style="background-image:url({{ asset('desktop/images/feautred-content/banner.jpg') }})">
                        <div class="total-fees">Total Fees : ¥800</div>
                        <div class="banner-copy">
                            <h1 class="heading__h1">Trust Yourself</h1>
                            <img src="{{ asset('desktop/images/feautred-content/views-eye.svg') }}" />
                            <span>1000 Views</span>
                            <p>Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod tempor incididunt ut labore </p>
                        </div>
                    </div>
                    <div class="featured-copy">
                        <h5 class="heading__h5">Brief Introduction</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eleifend donec pretium vulputate sapien. Dignissim cras tincidunt lobortis feugiat vivamus at. Amet consectetur adipiscing elit ut aliquam. Volutpat ac tincidunt vitae semper quis lectus nulla. Sed euismod nisi lorem mollis aliquam ut porttitor. Sollicitudin tempor id eu nisl nunc. Sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque eu. Diam quam nulla porttitor massa. Egestas integer eget aliquet nibh praesent tristique. Accumsan lacus vel facilisis volutpat est velit egestas.</p>
                        <p>Eleifend quam adipiscing vitae proin sagittis. Nunc aliquet bibendum enim facilisis. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. At tempor commodo ullamcorper a lacus vestibulum. Leo in vitae turpis massa sed elementum tempus egestas. Ac auctor augue mauris augue neque. Sed egestas egestas fringilla phasellus faucibus. Placerat vestibulum lectus mauris ultrices eros in cursus turpis massa.Enim diam vulputate ut pharetra sit amet aliquam id. Id velit ut tortor pretium viverra suspendisse potenti nullam ac. Mauris cursus mattis molestie.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="featured-data">
            <div class="m-page-container">
                <div class="data-container">
                    <article class="course">
                        <a href="#featured-episodes" class="modal-link-episode">
                            <figure>
                                <span class="overlay">Free</span>
                                <span class="img-name" style="background-image:url({{ asset('desktop/images/feautred-content/video-placeholder.jpg') }})"></span>
                                <span class="play"></span>
                            </figure>
                        </a>
                        <div class="article-info">
                            <h4 class="video-heading"><a href="#" >Episode 1 : Name of the Episode</a></h4>
                            <div class="video-copy"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eleifend donec pretium vulputate sapien. Dignissim cras tincidunt lobortis feugiat vivamus at. Amet consectetur adipiscing elit ut aliquam. Volutpat ac tincidunt vitae semper quis lectus nulla. Sed euismod nisi porta lorem mollis aliquam ut porttitor. Sollicitudin tempor id eu nisl nunc</a></div>
                        </div>
                    </article>
                    <article class="course">
                        <a href="#featured-episodes" class="modal-link-episode">
                            <figure>
                                <span class="overlay">Free</span>
                                <span class="img-name" style="background-image:url({{ asset('desktop/images/feautred-content/video-placeholder.jpg') }})"></span>
                                <span class="play"></span>
                            </figure>
                        </a>
                        <div class="article-info">
                            <h4 class="video-heading"><a href="#" >Episode 1 : Name of the Episode</a></h4>
                            <div class="video-copy"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eleifend donec pretium vulputate sapien. Dignissim cras tincidunt lobortis feugiat vivamus at. Amet consectetur adipiscing elit ut aliquam. Volutpat ac tincidunt vitae semper quis lectus nulla. Sed euismod nisi porta lorem mollis aliquam ut porttitor. Sollicitudin tempor id eu nisl nunc</a></div>
                        </div>
                    </article>
                    <article class="course">
                        <a href="#featured-episodes" class="modal-link-episode">
                            <figure>
                                <span class="overlay">Free</span>
                                <span class="img-name" style="background-image:url({{ asset('desktop/images/feautred-content/video-placeholder.jpg') }})"></span>
                                <span class="play"></span>
                            </figure>
                        </a>
                        <div class="article-info">
                            <h4 class="video-heading"><a href="#" >Episode 1 : Name of the Episode</a></h4>
                            <div class="video-copy"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eleifend donec pretium vulputate sapien. Dignissim cras tincidunt lobortis feugiat vivamus at. Amet consectetur adipiscing elit ut aliquam. Volutpat ac tincidunt vitae semper quis lectus nulla. Sed euismod nisi porta lorem mollis aliquam ut porttitor. Sollicitudin tempor id eu nisl nunc</a></div>
                        </div>
                    </article>
                    <article class="course">
                        <a href="#featured-episodes" class="modal-link-episode">
                            <figure>
                                <span class="overlay">Free</span>
                                <span class="img-name" style="background-image:url({{ asset('desktop/images/feautred-content/video-placeholder.jpg') }})"></span>
                                <span class="play"></span>
                            </figure>
                        </a>
                        <div class="article-info">
                            <h4 class="video-heading"><a href="#" >Episode 1 : Name of the Episode</a></h4>
                            <div class="video-copy"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eleifend donec pretium vulputate sapien. Dignissim cras tincidunt lobortis feugiat vivamus at. Amet consectetur adipiscing elit ut aliquam. Volutpat ac tincidunt vitae semper quis lectus nulla. Sed euismod nisi porta lorem mollis aliquam ut porttitor. Sollicitudin tempor id eu nisl nunc</a></div>
                        </div>
                    </article>
                    <article class="course">
                        <a href="#featured-episodes" class="modal-link-episode">
                            <figure>
                                <span class="overlay">Free</span>
                                <span class="img-name" style="background-image:url({{ asset('desktop/images/feautred-content/video-placeholder.jpg') }})"></span>
                                <span class="play"></span>
                            </figure>
                        </a>
                        <div class="article-info">
                            <h4 class="video-heading"><a href="#" >Episode 1 : Name of the Episode</a></h4>
                            <div class="video-copy"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eleifend donec pretium vulputate sapien. Dignissim cras tincidunt lobortis feugiat vivamus at. Amet consectetur adipiscing elit ut aliquam. Volutpat ac tincidunt vitae semper quis lectus nulla. Sed euismod nisi porta lorem mollis aliquam ut porttitor. Sollicitudin tempor id eu nisl nunc</a></div>
                        </div>
                    </article>
                    <article class="course">
                        <a href="#featured-episodes" class="modal-link-episode">
                            <figure>
                                <span class="overlay">Free</span>
                                <span class="img-name" style="background-image:url({{ asset('desktop/images/feautred-content/video-placeholder.jpg') }})"></span>
                                <span class="play"></span>
                            </figure>
                        </a>
                        <div class="article-info">
                            <h4 class="video-heading"><a href="#" >Episode 1 : Name of the Episode</a></h4>
                            <div class="video-copy"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eleifend donec pretium vulputate sapien. Dignissim cras tincidunt lobortis feugiat vivamus at. Amet consectetur adipiscing elit ut aliquam. Volutpat ac tincidunt vitae semper quis lectus nulla. Sed euismod nisi porta lorem mollis aliquam ut porttitor. Sollicitudin tempor id eu nisl nunc</a></div>
                        </div>
                    </article>
                </div>
            </div>
            <div class="button-container text-center">
                <a href="#" class="button grey midsize small">Enroll Now for ¥800</a>
            </div>
        </section>
        <!--Modal code-->
        <div class="hide">
            @include('desktop.modals.featured-episodes')
        </div>
    </div>
@stop
