@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Certifications</h3>
                </div>
                <div class="right">
                	<a href="#" class="button tinysize small caps certificate-btn">
                    	<span class="icon-trophy"></span> Create new certificate
                    </a>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="member-breadcrumb">
               		<a href="{{ BASE_URL }}member-certifications">Certifications</a> 
                    <span class="divider"> > </span> 
                    Create New Certificate
                </div>
				<div class="content-white-box">
                    <div class="tranks-certificate">
                        <h3 class="heading__h3">Thank you for submitting your certificate.<br>
                        Our team will review it and get back to you shortly.</h3>
                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                        <h3 class="heading__h3">Certificate ID : XXXXXXXX</h3>
                        <div class="button-container">
                            <a href="#" class="button tinysize small lightpink">Preview Certificate</a>
                        </div>
                    </div>
                </div>                
            </section>
            
        </div>
    </div>
</div>
@stop

