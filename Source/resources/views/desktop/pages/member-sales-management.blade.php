@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<h3 class="heading__h3">Sales management</h3>
            </div>
            <section class="content-grey-box no-top-border">
				<div class="content-white-box">
                    <div class="bg-grey clear">
                        <div class="left">
                            <h3 class="heading__h3  no-margin">Total Revenue Generated : $20000</h3>
                        </div>
                        <div class="right">
                        	<select class="custom-select full-width white">
                                <option>$ US Dollar</option>
                                <option>$ INR Rupees</option>
                            </select>
                        </div>
                    </div>
                    <div class="content-grey-box no-top-border small-padding">
                    	<h6 class="heading__h6">INDIVIDUAL COURSE EARNINGS</h6>
                        <div class="clear">
                        	<select class="custom-select white margin-bot">
                                <option>Select Course(s)</option>
                            </select>
                        </div>
                    	<div class="notification-grid theme-white">
                            <div class="title small-size row">   
                                <div class="col m1 text-center">
                                    <div class="bg-container">S.No</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">Course ID</div>
                                </div>
                                <div class="col m5">
                                    <div class="bg-container">Course Name</div>
                                </div>
                                <div class="col m2 text-center">
                                    <div class="bg-container">Amount (in $)</div>
                                </div>
                                <div class="col m2 text-center">
                                    
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">01</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">QQQ666</div>
                                </div>
                                <div class="col m5">
                                    <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                                </div>
                                <div class="col m2 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2 text-center">
                                    <a href="#" class="button grey-fill tinysize">Download</a>
                                </div>
                            </div>
                        	<div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">01</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">QQQ666</div>
                                </div>
                                <div class="col m5">
                                    <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                                </div>
                                <div class="col m2 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2 text-center">
                                    <a href="#" class="button grey-fill tinysize">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">01</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">QQQ666</div>
                                </div>
                                <div class="col m5">
                                    <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                                </div>
                                <div class="col m2 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2 text-center">
                                    <a href="#" class="button grey-fill tinysize">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">01</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">QQQ666</div>
                                </div>
                                <div class="col m5">
                                    <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                                </div>
                                <div class="col m2 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2 text-center">
                                    <a href="#" class="button grey-fill tinysize">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">01</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">QQQ666</div>
                                </div>
                                <div class="col m5">
                                    <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                                </div>
                                <div class="col m2 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2 text-center">
                                    <a href="#" class="button grey-fill tinysize">Download</a>
                                </div>
                            </div>
                    	</div>
                    </div>
                    <div class="date-range margin-bot ">
                        <div class="form-container">
                            <div class="form-element">
                                <label for="txtFromDate" class="title">From </label>
                                <input name="txtFromDate" type="date" id="txtFromDate">
                            </div>
                            <div class="form-element">
                                <label for="txtToDate" class="title">To </label>
                                <input name="txtToDate" type="date" id="txtToDate">
                            </div>
                        </div>
                    </div>
                	<div class="notification-grid">
                            <div class="title small-size row">   
                                <div class="col m1 text-center">
                                    <div class="bg-container">S.No</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">Course ID</div>
                                </div>
                                <div class="col m5">
                                    <div class="bg-container">Course Name</div>
                                </div>
                                <div class="col m2 text-center">
                                    <div class="bg-container">Amount (in $)</div>
                                </div>
                                <div class="col m2 text-center">
                                    
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">01</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">QQQ666</div>
                                </div>
                                <div class="col m5">
                                    <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                                </div>
                                <div class="col m2 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2 text-center">
                                    <a href="#" class="button grey-fill tinysize">Download</a>
                                </div>
                            </div>
                        	<div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">01</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">QQQ666</div>
                                </div>
                                <div class="col m5">
                                    <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                                </div>
                                <div class="col m2 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2 text-center">
                                    <a href="#" class="button grey-fill tinysize">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">01</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">QQQ666</div>
                                </div>
                                <div class="col m5">
                                    <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                                </div>
                                <div class="col m2 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2 text-center">
                                    <a href="#" class="button grey-fill tinysize">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">01</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">QQQ666</div>
                                </div>
                                <div class="col m5">
                                    <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                                </div>
                                <div class="col m2 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2 text-center">
                                    <a href="#" class="button grey-fill tinysize">Download</a>
                                </div>
                            </div>
                            <div class="data row">                        	
                                <div class="col m1 text-center">
                                    <div class="bg-container">01</div>
                                </div>                     	
                                <div class="col m2 text-center">
                                    <div class="bg-container">QQQ666</div>
                                </div>
                                <div class="col m5">
                                    <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                                </div>
                                <div class="col m2 text-center">
                                    <div class="bg-container">100,000</div>
                                </div>
                                <div class="col m2 text-center">
                                    <a href="#" class="button grey-fill tinysize">Download</a>
                                </div>
                            </div>
                    	</div>
                </div>
            </section>
            
        </div>
    </div>
</div>
@stop

