@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
                <div class="left">
                    <h3 class="heading__h3">Online course management</h3>
                </div>
                <div class="right">
                	<a href="#" class="button tinysize small caps certificate-btn">
                    	<span class="icon-delete white small"></span> Delete course
                    </a>
                </div>
            </div>
            <section class="content-grey-box no-top-border">
            	<h3 class="heading__h3 bg-grey">
                	<div class="steps-heading">
                    	<span class="active">Step 01</span>
                        <span>Step 02</span>
                        <span>Step 03</span>
                        <span>Step 04</span>
                    </div>
                </h3>
            	<div class="content-white-box no-top-border">
                	<h4 class="heading__h4">Online Course Basic Settings</h4>
                    <div class="form-container">
                    	<div class="form-element">
                        	<label for="txtCourseTitle" class="title">Course Title*</label>
                            <input name="" type="text" class="width40" id="txtCourseTitle" placeholder="Course Title here">
                        </div>
                        <div class="form-element">
                        	<label for="txtCourseSubtitle" class="title">Course Subtitle</label>
                            <input name="" type="text" class="width40" id="txtCourseSubtitle" placeholder="Course Subtitle here">
                        </div>                        
                        <div class="form-element">
                        	<label for="txtCourseThumbnail" class="title">Course Thumbnail</label>
                            <div class="file-upload upload-img">
                                <input type="file" name="receipt" id="file-browse" class="inputfile" />
                                <label for="file-browse"><span class="input-text">&nbsp;</span> <strong>+ Upload Image</strong></label>
                            </div>
                            <div class="left note">Course Image Format : Supported format for uploading the file : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtCourseThumbnailVideo" class="title">Course Trailer Video</label>
                            <div class="file-upload upload-video">
                                <input type="file" name="receipt" id="file-browse" class="inputfile" />
                                <label for="file-browse"><span class="input-text">&nbsp;</span> <strong>+ Upload Image</strong></label>
                            </div>
                            <div class="left note">Course Video Format : Supported format for uploading the file : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
                        </div>
                        <div class="form-element">
                        	<label for="rdoCertification" class="title">Certification</label>
                            <div class="radioGroup">
                            	<div class="radio-item">
                                	<input name="rdoCertification" id="rdoCertification1" type="radio" class="radio-btn">
                                    <label for="rdoCertification1" class="radio-label">Yes</label>
                                </div>
                                <div class="radio-item">
                                	<input name="rdoCertification" id="rdoCertification2" type="radio" class="radio-btn">
                                    <label for="rdoCertification2" class="radio-label">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="lstCategory" class="title">Category</label>
                            <select name="" class="custom-select width30" id="lstCategory">
                                <optgroup label="category 01">
                                    <option value="">Sub category 01</option>
                                    <option value="">Sub category 02</option>
                                </optgroup>
                                <optgroup label="category 01">
                                    <option value="">Sub category 11</option>
                                    <option value="">Sub category 12</option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="form-element">
                        	<label for="lstCourseLanguage" class="title">Course Language</label>
                            <select name="" class="custom-select width30" id="lstCourseLanguage">
                                <option>English</option>
                            </select>
                        </div>
                        <div class="form-element">
                        	<label for="lstRegion" class="title">Region</label>
                            <div class="right-container width30">
                                <select name="" multiple="MULTIPLE" class="custom-select width30 multiselect-listbox" id="lstRegion">
                                    <option>Select Countries</option>
                                    <option>India</option>
                                    <option>Japan</option>
                                    <option>US</option>
                                    <option>UK</option>
                                    <option>France</option>
                                    <option>Nepal</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtTargetAudience" class="title">Target Audience</label>
                            <div class="right-container">
                            	<input type="text" class="width80" id="txtTargetAudience" value="Lorem ipsum dolor sit amet, consectetur ">
                            </div>
                        </div>
                        <div class="form-element">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="right-container">
                                <input type="submit" value="+ Add a Target" class="button grey-fill tinysize mid">
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtRequirements" class="title">Requirements</label>
                            <textarea name="" rows="4" id="txtRequirements">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</textarea>
                        </div>
                        <div class="form-element">
                        	<label for="txtKeywords" class="title">Keywords</label>
                            <div class="right-container">
                            	<input type="text" class="width80" id="txtKeywords" value="Lorem ipsum dolor sit amet, consectetur ">
                            </div>
                        </div>
                        <div class="form-element">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="right-container">
                                <input type="submit" value="+ Add More Keywords" class="button grey-fill tinysize mid left">
                                <span class="note left">Notet : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </span>
                            </div>
                        </div>
                        <div class="form-element">
                        	<label for="txtCourseOutline" class="title">Course Outline</label>
                            <div class="right-container">
                                <textarea name="" rows="4" id="txtCourseOutline" class="full-width">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</textarea>
                                <span class="note full-width clear">Notet : Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </span>
                        	</div>
                        </div>
                        <div class="form-element">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="right-container custom-checkbox">
                                <input id="chk1" type="checkbox" name="" class="styled-checkbox pink" >
                                <label for="chk1" class="">I allow GoGetterz Instructional Design team to edit, improve and/or optimize my course</label>                                
                            </div>
                        </div>
                        <div class="form-element button-container">
                        	<div class="left-container">&nbsp;</div>
                        	<div class="right-container">
                                <input type="submit" value="Continue to Step 2" class="button lightpink tinysize mid ">
                            </div>
                        </div>
                    </div>
                </div>
            </section>            
        </div>
    </div>
</div>
@stop

