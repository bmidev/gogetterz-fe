@extends('desktop.layouts.master')
@section('content')

<section class="category-landing">
    <div class="m-page-container">
        <h2 class="heading__h2">Featured content</h2>
        <p class="copy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
    	
    	<div class="category-titles">
        	<div class="category-tiles width1 height1">
            	<a href="#" class="black">Languages</a>
            </div>
            <div class="category-tiles width1 height1">
            	<a href="#" class="white">Creative</a>
            </div>
            <div class="category-tiles width1 height1">
            	<a href="#" class="pink">Business</a>
            </div>
            <div class="category-tiles width2 height1">
            	<a href="#" class="black">Health &amp; beauty</a>
            </div>
            <div class="category-tiles width3 height2 img-container">
            	{{--<figure><img src="{{ asset('desktop/images/jp/category-landing.jpg') }}" alt=""/></figure>--}}
                <div class="featured-content-promo-slider">
                	<div class="slide-data" style="background-image:url({{ asset('desktop/images/jp/category-landing.jpg') }})"></div>
                    <div class="slide-data" style="background-image:url({{ asset('desktop/images/jp/category-landing.jpg') }})"></div>
                    <div class="slide-data" style="background-image:url({{ asset('desktop/images/jp/category-landing.jpg') }})"></div>
                </div>
            </div>
            <div class="category-tiles width2 height2">
            	<div class="category-tiles width5 height1">
            		<a href="#" class="white">It &amp; technology</a>
                </div>
                <div class="category-tiles width5 height1">
                    <a href="#" class="white">Hobbies &amp; culture</a>
                </div>
            </div>
            
            <div class="category-tiles width1 height1">
            	<a href="#" class="black">Money</a>
            </div>
            <div class="category-tiles width2 height1">
            	<a href="#" class="white">Certifications</a>
            </div>
            <div class="category-tiles width2 height1">
            	<a href="#" class="pink">Japanese culture &amp; tourism</a>
            </div>
            <div class="category-tiles width1 height1">
            	<a href="#" class="white">Cooking</a>
            </div>
            <div class="category-tiles width2 height1">
            	<a href="#" class="black">All courses</a>
            </div>
        </div>
    </div>
</section>    

@stop

