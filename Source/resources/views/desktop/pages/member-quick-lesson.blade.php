@extends('desktop.layouts.master')
@section('content')

<div class="page-container member-page-container">
    <div class="members-container">
        @include('desktop.partials.member-left-links')
        <div class="member-content-area">
        	<div class="member-content-heading">
            	<h3 class="heading__h3">Quick lesson</h3>
            </div>
            <section class="content-grey-box no-top-border">
            	<div class="course-top-buttons">
                	<a href="{{ BASE_URL }}member-create-quick-lesson" class="button grey-fill no-hover tinysize small"><span class="icon-quick-lesson"></span> Create Quick lesson</a>                    
                </div>
            	<div class="content-white-box">
                	<h4 class="heading__h4">Lesson Created by You</h4>
                	<div class="notification-grid">
                        <div class="title small-size row">   
                            <div class="col m1 text-center">
                                <div class="bg-container">S.No</div>
                            </div>                     	
                            <div class="col m2 text-center">
                                <div class="bg-container">Course ID</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container">Title</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Status</div>
                            </div>
                            <div class="col m1 text-center">
                                
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">01</div>
                            </div>                     	
                            <div class="col m2 text-center">
                                <div class="bg-container">QQQ666</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Approved</div>
                            </div>
                            <div class="col m1 text-center">
                                <a href="{{ BASE_URL }}member-ocm-classroom" class="icon-classroom"></a>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">02</div>
                            </div>                     	
                            <div class="col m2 text-center">
                                <div class="bg-container">QQQ666</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Approved</div>
                            </div>
                            <div class="col m1 text-center">
                                <a href="{{ BASE_URL }}member-ocm-classroom" class="icon-classroom"></a>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">03</div>
                            </div>                     	
                            <div class="col m2 text-center">
                                <div class="bg-container">QQQ666</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Approved</div>
                            </div>
                            <div class="col m1 text-center">
                                <a href="{{ BASE_URL }}member-ocm-classroom" class="icon-classroom"></a>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">04</div>
                            </div>                     	
                            <div class="col m2 text-center">
                                <div class="bg-container">QQQ666</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Approved</div>
                            </div>
                            <div class="col m1 text-center">
                                <a href="{{ BASE_URL }}member-ocm-classroom" class="icon-classroom"></a>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">05</div>
                            </div>                     	
                            <div class="col m2 text-center">
                                <div class="bg-container">QQQ666</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Pending Approval</div>
                            </div>
                            <div class="col m1 text-center">
                                <a href="{{ BASE_URL }}member-ocm-classroom" class="icon-classroom"></a>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">06</div>
                            </div>                     	
                            <div class="col m2 text-center">
                                <div class="bg-container">QQQ666</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Approved</div>
                            </div>
                            <div class="col m1 text-center">
                                <a href="{{ BASE_URL }}member-ocm-classroom" class="icon-classroom"></a>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">07</div>
                            </div>                     	
                            <div class="col m2 text-center">
                                <div class="bg-container">QQQ666</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Approved</div>
                            </div>
                            <div class="col m1 text-center">
                                <a href="{{ BASE_URL }}member-ocm-classroom" class="icon-classroom"></a>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">08</div>
                            </div>                     	
                            <div class="col m2 text-center">
                                <div class="bg-container">QQQ666</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Approved</div>
                            </div>
                            <div class="col m1 text-center">
                                <a href="{{ BASE_URL }}member-ocm-classroom" class="icon-classroom"></a>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">09</div>
                            </div>                     	
                            <div class="col m2 text-center">
                                <div class="bg-container">QQQ666</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Approved</div>
                            </div>
                            <div class="col m1 text-center">
                                <a href="{{ BASE_URL }}member-ocm-classroom" class="icon-classroom"></a>
                            </div>
                        </div>
                        <div class="data row">                        	
                            <div class="col m1 text-center">
                                <div class="bg-container">10</div>
                            </div>                     	
                            <div class="col m2 text-center">
                                <div class="bg-container">QQQ666</div>
                            </div>
                            <div class="col m5">
                                <div class="bg-container">Excepteur sint occaecat cupidatat</div>
                            </div>
                            <div class="col m3 text-center">
                                <div class="bg-container">Pending Approval</div>
                            </div>
                            <div class="col m1 text-center">
                                <a href="{{ BASE_URL }}member-ocm-classroom" class="icon-classroom"></a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </section>            
        </div>
    </div>
</div>
@stop

