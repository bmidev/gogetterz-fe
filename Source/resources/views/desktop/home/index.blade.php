@extends('desktop.layouts.master')
@section('content')

<section class="home-welcome">
    <div class="page-container">
        <div class="home-welcome-image">
        	<div class="welcome-slider">
                <div>
                    <img src="{{ asset('desktop/images/jp/welcome01.jpg') }}" alt=""/>
                </div>
                <div>
                    <img src="{{ asset('desktop/images/jp/welcome02.jpg') }}" alt=""/>
                </div>
                <div>
                    <img src="{{ asset('desktop/images/jp/welcome01.jpg') }}" alt=""/>
                </div>
                <div>
                    <img src="{{ asset('desktop/images/jp/welcome02.jpg') }}" alt=""/>
                </div>
            </div>
        </div>
        <div class="welcome-copy">
            <h1>Welcome!</h1>
            <p>
                GoGetterz is a video-based skill sharing platform that enables people to teach and learn skills, 
                set goals and achieve them. We provide you with a learning experience that gives you freedom in matching your 
                lifestyle and goals. It is the place for those with burning passion to proactively pursue their 
                learning interests as well as the place for those with a deep 
                passion to share their expertise and skills. We help you design your future.
            </p>
            <div class="button-container">
                <a href="{!! BASE_URL !!}register" class="button black large small">Join Now</a> 
            </div>
        </div>
    </div>
</section>
<section class="categories-courses-users">
    <div class="page-container">
        <div class="top-categories">
            <div class="section-label">Pick from our top categories</div>
            <div class="category-info">
                <figure>
                    <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/icon-business.svg') }})"></span></a>
                    <figcaption><a href="#">Business</a></figcaption>
                </figure>
            </div>
            <div class="category-info">
                <figure>
                    <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/icon-health-beauty.svg') }})"></span></a>
                    <figcaption><a href="#">Health &amp; beauty</a></figcaption>
                </figure>
            </div>
            <div class="category-info">
                <figure>
                    <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/icon-languages.svg') }})"></span></a>
                    <figcaption><a href="#">Languages</a></figcaption>
                </figure>
            </div>
            <div class="category-info">
                <figure>
                    <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/icon-creative.svg') }})"></span></a>
                    <figcaption><a href="#">Creative</a></figcaption>
                </figure>
            </div>
        </div>
        <div class="quick-info">
            <div class="courses-info">
            	<span class="icon courses"></span>
                <span class="info"><strong>50,000</strong> Courses</span>
            </div>
            <div class="users-info">
            	<span class="icon users"></span>
                <span class="info"><strong>100,000</strong> Users</span>
            </div>
        </div>
    </div>
</section>
<section class="quick-article-container grey-bg">
    <div class="page-container bg-black">
    	<div class="heading">
        	<h3 class="heading__h3">
            	<a href="#"><span>Recommended for you</span></a>
            </h3>
        </div>
        <div class="courses-slider">
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	New
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course04.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course04.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>
<section class="quick-article-container grey-bg2">
    <div class="page-container">
    	<div class="heading">
        	<h3 class="heading__h3">
            	<a href="#"><span>Quick lessons</span></a>
            </h3>
        </div>
        <div class="quick-lessons-slider">
            <div class="slide">
                <figure>
                	<a href="#quick-lesson-modal" class="quick-lesson-video"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-quick-lesson01.jpg') }})"></span></a>
                	<figcaption><a href="#">Trust Yourself</a></figcaption>
                </figure>
            </div>
            <div class="slide">
                <figure>
                	<a href="#quick-lesson-modal" class="quick-lesson-video"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-quick-lesson02.jpg') }})"></span></a>
                	<figcaption><a href="#">Trust Yourself</a></figcaption>
                </figure>
            </div>
            <div class="slide">
                <figure>
                	<a href="#quick-lesson-modal" class="quick-lesson-video"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-quick-lesson03.jpg') }})"></span></a>
                	<figcaption><a href="#">Trust Yourself</a></figcaption>
                </figure>
            </div>
        </div>
    </div>
</section>

<section class="quick-article-container grey-bg">
    <div class="page-container">
    	<div class="heading">
        	<h3 class="heading__h3">
            	<a href="#"><span>Most Popular Courses</span></a>
            </h3>
        </div>
        <div class="courses-slider">
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	New
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course04.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course04.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>

<section class="quick-article-container grey-bg2">
    <div class="page-container">
    	<div class="heading">
        	<h3 class="heading__h3">
            	<a href="#"><span>Latest Courses</span></a>
            </h3>
        </div>
        <div class="courses-slider">
            <div>
                <article class="course">
                    <figure>
                    	<span class="overlay">Free</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	New
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                    	<span class="overlay">Free</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course04.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                    	<span class="overlay">Free</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                    	<span class="overlay">Free</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course04.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>
<section class="quick-article-container grey-bg">
    <div class="page-container">
    	<div class="heading">
        	<h3 class="heading__h3">
            	<a href="#"><span>Top Certification Courses</span></a>
            </h3>
        </div>
        <div class="courses-slider">
            <div>
                <article class="course">
                    <figure>
                    	<span class="overlay">Free</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	New
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                    	<span class="overlay">Free</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course04.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                    	<span class="overlay">Free</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                    	<span class="overlay">Free</span>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course04.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        <div class="author"><a href="#">Arighna Sarkar</a></div>
                        <div class="footer-container">
                        	<button class="favorite"></button>
                            <div class="rating">
                            	<span class="star"></span> 4.5
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>
<section class="home-featured-content">
    <div class="page-container">
        <h2 class="heading__h2"><a href="#">Featured content</a></h2>
        <p class="copy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
    </div>
    <div class="featured-content-slider">
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>                        
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course04.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course01.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        

                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course02.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course03.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        
                    </div>
                </article>
            </div>
            <div>
                <article class="course">
                    <figure>
                        <a href="#"><span class="img-name" style="background-image:url({{ asset('desktop/images/jp/img-course04.jpg') }})"></span></a>
                    </figure>
                    <div class="article-info">
                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h4>
                        
                    </div>
                </article>
            </div>
        </div>
    <div class="view-all">
    	<a href="#">View all</a>
    </div>
</section>
<section class="home-info-container">
    <div class="page-container">
    	<div class="info-copy">
            <h2 class="heading__h2">How it works</h2>
        </div>
        <div class="home-info-image">
        	<img src="{{ asset('desktop/images/jp/img-home-how-it-works.svg') }}" alt=""/>
        </div>
        <div class="info-copy">
            <div class="steps">
                <ol>
                    <li>
                        <h3>Membership</h3>
                        <p>You have to sign up on GoGetterz website to become a member. Once you become a member, you can freely search the courses and the content that experts have uploaded to the site.</p>
                    </li>
                    <li>
                        <h3>Course License Contract</h3>
                        <p>Members can access a course material i.e. license the contract of a course upon payment of the tuition fee set by an expert. </p>
                    </li>
                    <li>
                        <h3>Types of courses</h3>
                        <p>Members are entitled for various services when a course license contract becomes valid. They are limited, unlimited, monthly contract distribution courses and Event seminars.</p>
                    </li>
                </ol>
            </div>            
        </div>        
    </div>
    <div class="button-container text-center clear">
        <a href="#" class="button white large small">Join Now</a> 
    </div>
    <!--Modal code-->
    <div class="hide">
        @include('desktop.modals.quick-lesson-video')
    </div>
</section>
@stop

