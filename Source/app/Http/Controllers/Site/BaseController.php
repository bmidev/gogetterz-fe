<?php namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Libs\Platform\Page\PageManager;

class BaseController extends Controller {
    
    public function __construct() {
        parent::__construct();
        
        /* Application Variables */
        if (!defined('BASE_URL')) define('BASE_URL', url('/') . '/');
        if (!defined('FACEBOOK_APP_ID')) define('FACEBOOK_APP_ID', 261593247716096); // TODO: Need to create.
        /* Application Variables */
    
        /* [START] Initialize Locale */
        if(!session()->exists('locale')) {
            session(['locale' => 'en']);
            app()->setLocale(session('locale'));
        } else {
            app()->setLocale(session('locale'));
        }
        /* [END] Initialize Locale */
    
        /* [START] Page Feature */
        $this->page = new PageManager();
        $this->page->setActivePage(str_replace('controller', '', strtolower(substr(get_class(), strrpos(get_class(), '\\') + 1))));
        $this->page->getBody()->addBreadcrumb('Home', '#');
    
        $this->viewBase = str_replace('controller', '', str_replace('app\\http\\controllers\\site\\', '', strtolower(get_called_class())));
        $this->viewBase = str_replace('\\', '.', $this->viewBase);
        /* [END] Page Feature */
    }
}
