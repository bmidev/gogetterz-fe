<?php namespace App\Http\Controllers\Site;

class PageController extends BaseController {

    public function __construct() {
        parent::__construct();
    }
    
    public function getHome() {
        return view('desktop.pages.home');
    }
    public function getExpert() {
        return view('desktop.pages.expert');
    }
	public function getCatalogue() {
        return view('desktop.pages.catalogue');
	}
	public function getCatalogueDetails() {
        return view('desktop.pages.catalogue-details');
	}	
    public function getClient() {
        return view('desktop.pages.client');
    }
	public function getCourseDetails() {
        return view('desktop.pages.course-details');
	}
    public function getFeaturedContent() {
        return view('desktop.pages.featured-content');
    }
	
	public function getThanks() {
        return view('desktop.pages.thanks');
    }
	public function getPayment() {
        return view('desktop.pages.payment');
    }
    public function getUpcomingEvents() {
        return view('desktop.pages.upcoming-events');
    }
    public function getQuickLesson() {
        return view('desktop.pages.quick-lesson');
    }
    public function getAuthorDetails() {
        return view('desktop.pages.author-details');
    }
	public function getCourseLearner() {
        return view('desktop.pages.course-learner');
    }
	public function getCourseLearnerTest() {
        return view('desktop.pages.course-learner-test');
    }
	
	//Members pages
	public function getMembersProfile() {
        return view('desktop.pages.member-profile');
    }
	public function getMemberCategories() {
        return view('desktop.pages.member-categories');
    }
	public function getMemberCourseDetails() {
        return view('desktop.pages.member-course-details');
    }
	public function getMemberCourses() {
        return view('desktop.pages.member-courses');
    }
	public function getMemberCoursesResult() {
        return view('desktop.pages.member-courses-result');
    }
	public function getMemberDashboard() {
        return view('desktop.pages.member-dashboard');
    }
	public function getMemberNotifications() {
        return view('desktop.pages.member-notifications');
    }
	public function getMemberMessages() {
        return view('desktop.pages.member-messages');
    }
	public function getMemberComposeMessage() {
        return view('desktop.pages.member-compose-message');
    }
	public function getMemberMessageChat() {
        return view('desktop.pages.member-message-chat');
    }
	public function getMemberCertifications() {
        return view('desktop.pages.member-certifications');
    }
	public function getMemberCreateCertificate() {
        return view('desktop.pages.member-create-certificate');
    }
	public function getMemberCreateCertificateThanks() {
        return view('desktop.pages.member-create-certificate-thanks');
    }
	public function getMemberPoints() {
        return view('desktop.pages.member-points');
    }
	public function getMemberInfoHelp() {
        return view('desktop.pages.member-info-help');
    }
	public function getMemberSalesManagement() {
        return view('desktop.pages.member-sales-management');
    }
	public function getMemberEventManagement() {
        return view('desktop.pages.member-event-management');
    }
	public function getMemberQuickLesson() {
        return view('desktop.pages.member-quick-lesson');
    }
	public function getMemberCreateQuickLesson() {
        return view('desktop.pages.member-create-quick-lesson');
    }
	public function getMemberCreateQuickLessonThanks() {
        return view('desktop.pages.member-create-quick-lesson-thanks');
    }
	
	public function getMemberCourseManagement() {
        return view('desktop.pages.member-online-course-management');
    }
	public function getMemberOCMClassroom() {
        return view('desktop.pages.member-ocm-classroom');
    }
	public function getMemberOCMCreateCoupon() {
        return view('desktop.pages.member-ocm-create-coupon');
    }
	public function getMemberOCMStep1() {
        return view('desktop.pages.member-ocm-step1');
    }
	public function getMemberOCMStep2() {
        return view('desktop.pages.member-ocm-step2');
    }
	public function getMemberOCMStep3() {
        return view('desktop.pages.member-ocm-step3');
    }
	public function getMemberOCMStep4() {
        return view('desktop.pages.member-ocm-step4');
    }
	public function getMemberOCMStepThanks() {
        return view('desktop.pages.member-ocm-step-thanks');
    }
	
	public function getEmployeeBillings() {
        return view('desktop.pages.employee-billings');
    }
	public function getEmployeeCourses() {
        return view('desktop.pages.employee-courses');
    }
	public function getEmployeeDatabase() {
        return view('desktop.pages.employee-database');
    }
	public function getEmployeeDetails() {
        return view('desktop.pages.employee-details');
    }
	public function getEmployeeSettings() {
        return view('desktop.pages.employee-settings');
    }
}

