<?php namespace App\Http\Controllers\Site;

class HomeController extends BaseController {
    
    /**
     * HomeController constructor.
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Method to display homepage
     * 
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view($this->viewPath . '.' . $this->viewBase . '.' . __FUNCTION__);
    }
}
