<?php namespace App\Http\Controllers\Site;

use App\Http\Requests\Account\LoginValidationRequest;

class UserController extends BaseController {
    
    protected $errorMessage = 'These credentials do not match our records.';
    
    public function __construct() {
        parent::__construct();
    }
    
    public function login(LoginValidationRequest $request) {
        $identity = $request->input('identity');
        $field = filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $request->merge([$field => $identity]);
        
        if (Auth::attempt($request->only($field, 'password'))) {
            if (auth()->user()->is_active == 1) {
                return redirect()->route('home');
            } else {
                auth()->logout();
                $this->errorMessage = 'You\'ve been marked InActive.';
            }
        }
        
        return redirect()->route('login')->withInput()->withErrors([
            'password' => $this->errorMessage,
        ]);
    }
    
    public function register() {
        
    }
}
