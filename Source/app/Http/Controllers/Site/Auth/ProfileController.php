<?php namespace App\Http\Controllers\Site\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\C2C\UserUpdateValidationRequest;
use App\Libs\Platform\Storage\User\UserRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class ProfileController extends Controller {
    private $user;
    
    /**
     * UserController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user) {
        parent::__construct();
        
        $this->user = $user;
        $this->page->setActivePage('profile');
        $this->page->setActiveSection(['user']);
    }
    
    /**
     * Method to display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        /* Default Variables */
        $active = false;
        $fields = [];
        $metaDesc = 'Edit details of user; '; // meta description
        $metaKeywords = 'edit, update, user edit, edit user information '; // meta keywords
        $title = 'My Profile';
        $userID = auth()->id();
        $with = [];
        /* Default Variables */
        
        /* Get User */
        $response = $this->user->view($userID, $active, $fields, $with);
        /* Get User */
        
        /* Enhance Meta Information and Title */
        $metaDesc .= addslashes($response['full_name']);
        $metaKeywords .= 'manage, edit, manage user, edit ' . addslashes($response['full_name']) . ', manage '. addslashes($response['full_name']);
        /* Enhance Meta Information and Title */
        
        /* Breadcrumbs */
        $this->page->getBody()->addBreadcrumb('My Profile', 'profile');
        $this->page->getBody()->addBreadcrumb($response['full_name']);
        /* Breadcrumbs */
        
        /* Page Maker */
        $this->page->getHead()->setDescription($metaDesc);
        $this->page->getHead()->setKeywords($metaKeywords);
        $this->page->setTitle($title);
        /* Page Maker */
        
        /* Set Data To View */
        $this->page->getBody()->addToData('user', $response);
        /* Set Data To View */
        
        /* HTML View Response */
        return view($this->viewBase . '.' . __FUNCTION__, ['page' => $this->page]);
        /* HTML View Response */
    }

    /**
     * Method to update the specified resource in storage.
     * 
     * @param UserUpdateValidationRequest $userValidator
     * @param $userID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserUpdateValidationRequest $userValidator) {
        /* Default Variables */
        $userID = auth()->id();
        /* Default Variables */
        
        /* Separation & Limitations of Data By Models */
        $data['user'] = Input::only('username', 'full_name', 'email', 'is_active', 'is_superuser');
        if (Input::has('password')) {$data['user']['password'] = Hash::make(Input::get('password'));}
        /* Separation & Limitations of Data By Models */
        
        /* Query Creation & Fire */
        $modelResponse = $this->user->update($userID, $data);
        /* Query Creation & Fire */
        
        /* Redirect Based on Model Response */
        if ($modelResponse->exists) { // If Success
            return redirect()->route('profile.show')->with(['alert' => ['type' => 'success', 'message' => __('messages.crud.update.success', ['module' => 'Profile'])]]);
        } else { // If Error
            return redirect()->route('profile.show')->with(['alert' => ['type' => 'danger', 'message' => __('messages.crud.update.failure', ['module' => 'Profile'])]]);
        }
        /* Redirect Based on Model Response */
    }
}
