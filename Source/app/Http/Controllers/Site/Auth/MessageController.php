<?php namespace App\Http\Controllers\Site\Auth;

use App\Http\Controllers\Controller;
use App\Libs\Platform\Storage\User\UserRepository;

class MessageController extends Controller {
    private $user;
    
    /**
     * UserController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user) {
        parent::__construct();
        
        $this->user = $user;
        $this->page->setActivePage('message');
        $this->page->setActiveSection(['message']);
    }
    
    /**
     * Method to display listing page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        /* Default Variables */
        $metaDesc = 'List of messages'; // meta description
        $metaKeywords = 'list, messages, messages, list of messages, messages list'; // meta keywords
        $title = 'Message Management';
        /* Default Variables */
        
        /* Breadcrumbs */
        $this->page->getBody()->addBreadcrumb('Message', 'message', 'message.index');
        $this->page->getBody()->addBreadcrumb('Listing');
        /* Breadcrumbs */
        
        /* Page Maker */
        $this->page->getHead()->setDescription($metaDesc);
        $this->page->getHead()->setKeywords($metaKeywords);
        $this->page->setTitle($title);
        /* Page Maker */
        
        /* HTML View Response */
        return view($this->viewBase . '.' . __FUNCTION__, ['page' => $this->page]);
        /* HTML View Response */
    }
}
