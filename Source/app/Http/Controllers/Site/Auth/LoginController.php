<?php namespace App\Http\Controllers\Site\Auth;

use App\Http\Controllers\Site\BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\Account\LoginValidationRequest;
use Illuminate\Support\Facades\Auth;

class LoginController extends BaseController {
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    
    use AuthenticatesUsers;
    
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    
    /**
     * Error message
     *
     * @var string
     */
    protected $errorMessage = 'These credentials do not match our records.';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        
        $this->middleware('guest')->except('logout');
    }
    
    
    public function showLoginForm() {
        return view($this->viewPath . '.' . 'auth.login');
    }
    
    /**
     * Method to override default login feature.
     * 
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login(LoginValidationRequest $request) {
        $identity = $request->input('identity');
        $field = filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $request->merge([$field => $identity]);
        
        if (Auth::attempt($request->only($field, 'password'))) {
            if (auth()->user()->is_active == 1) {
                return redirect()->route('home');
            } else {
                auth()->logout();
                $this->errorMessage = 'You\'ve been marked InActive.';
            }
        }
        
        return redirect()->route('login')->withInput()->withErrors([
            'password' => $this->errorMessage,
        ]);
    }
}
