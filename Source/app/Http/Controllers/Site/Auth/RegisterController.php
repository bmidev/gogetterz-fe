<?php namespace App\Http\Controllers\Site\Auth;

use App\Http\Controllers\Site\BaseController;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Input;

class RegisterController extends BaseController {
    private $loginWithGoGetterz = 1;
    
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    
    use RegistersUsers;
    
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        
        $this->middleware('guest');
    }
    
    /**
     * Method shows login form instead of register form.
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function showRegistrationForm() {
        return view($this->viewPath . '.' . 'auth.register');
    }
    
    public function register() {
        $requestData = Input::only('email', 'userName', 'password', 'password_confirmation');
        $requestData['function'] = 'add_user';
        $requestData['loginWithID'] = $this->loginWithGoGetterz;
        
        try {
            $client = new Client();
            $result = $client->request('POST', config('app.api_url'), [
                'form_params' => $requestData
            ]);
        } catch (\Exception $ex) {
            echo "ERR: " . $ex->getTraceAsString(); exit;
        }
        
        echo "Status Code: " . $result->getStatusCode(); exit;
        
        echo "<pre>";
        print_r($result->getBody());
        echo "</pre>";
        exit;
    }
}
