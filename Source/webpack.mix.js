let mix = require('laravel-mix');

mix
	.sass('resources/assets/desktop/sass/app.scss', 'public/desktop/css')
    .styles([
        'resources/assets/desktop/css/vendor/colorbox.css',
        'resources/assets/desktop/css/vendor/slick.css',
        'resources/assets/desktop/css/vendor/animate.css',
		'resources/assets/desktop/css/vendor/aria-accordion.css',
		'resources/assets/desktop/css/vendor/sm-core-css.css',
		'resources/assets/desktop/css/vendor/fSelect.css',
		'resources/assets/desktop/css/vendor/css-menu.css',
		'resources/assets/desktop/css/vendor/easy-responsive-tabs.css'
    ], 'public/desktop/css/vendor.css')
	
	.scripts([
        'resources/assets/desktop/js/vendor/slick.min.js',
		'resources/assets/desktop/js/vendor/colorbox.js',
		'resources/assets/desktop/js/vendor/jquery.dlmenu.js',
		'resources/assets/desktop/js/vendor/aria-accordion.js',
		'resources/assets/desktop/js/vendor/jquery.custom-file-input.js',
		'resources/assets/desktop/js/vendor/jquery.smartmenus.js',
		'resources/assets/desktop/js/vendor/fSelect.js',
		'resources/assets/desktop/js/vendor/easyResponsiveTabs.js',
		'resources/assets/desktop/js/vendor/jquery.multi-select.js'
    ], 'public/desktop/js/vendor.js')
	
	.scripts([
		'resources/assets/desktop/js/vendor/Chart.bundle.js'
    ], 'public/desktop/js/chart.js')
	
	.scripts([
        'resources/assets/desktop/js/vendor/modernizr.custom.js'
    ], 'public/desktop/js/modernizr.js')
	
	.scripts([
        'resources/assets/desktop/js/vendor/jquery.js',
        'resources/assets/desktop/js/vendor/angular.js',
        'resources/assets/desktop/js/vendor/angular-resource.js',
        'resources/assets/desktop/js/site/app/main.js',
        'resources/assets/desktop/js/site/app/controllers/*.js',
        'resources/assets/desktop/js/site/app/services/*.js',
        'resources/assets/desktop/js/site/app/filters/*.js'
    ], 'public/desktop/js/app.js')
    
    .js('resources/assets/desktop/js/site/site.js', 'public/desktop/js/site.js')

    .options({
        processCssUrls : false
    });
